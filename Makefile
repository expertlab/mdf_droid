start: up-server
stop: down-server down-deps

up-deps:
	cd docker && docker compose up -d redis

down-deps:
	cd docker && docker compose down redis

up-server: down-server up-deps
	python "mdf_droid/service.py" &

down-server:
	-pkill -f "mdf_droid/service.py"
