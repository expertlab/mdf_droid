import pathlib
from setuptools import setup, find_packages
# The directory containing this file
HERE = pathlib.Path(__file__).parent
# The text of the README file
README = (HERE / "README.md").read_text()

setup(
    name='mdf_droid',
    version='6.0',
    packages=find_packages(include=['mdf_droid','mdf_droid.*']),
    install_requires=['twisted','zope-interface','Pillow','numpy','pycryptodome','pyserial','pyOpenSSL',
                      'cryptography','service_identity',
                      'mdf_canon @ git+ssh://git@gitlab.com/expertlab/mdf_canon.git',
                      #'mdf_canon @ git+ssh://git@gitlab.com/expertlab/mdf_canon.git@newconf',
                      ],
    #package_data={'mdf_droid':['*.sh','glaze/*.js']},
    
    description="MDF Droid",
    long_description=README,
    author='Daniele Paganelli',
    author_email='dp@mythsmith.it',
    url='https://www.expertlabservice.it/en/software/measurement-development-framework',
    license="LGPL",
    classifiers=[
        "License :: LGPL",
        "Programming Language :: Python :: 3",
    ],
    
)
