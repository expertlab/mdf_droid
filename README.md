# MDF Droid (Measurement Development Platform)

Welcome to the repository for the MDF Droid component development. 
``mdf_droid`` contains backend and server-side code, mainly written in Python3 and released with a MIT license.
Depends on ``mdf_canon`` for data access, and is further extendable with closed-source plugins supporting specific instrumentation and analysis needs.

MDF is an open source software platform divided in a client package (``mdf_client``), a server package (``mdf_droid``) and a bridge package (``mdf_canon``).

``mdf_droid`` was forked in 2019 from the github project ``misura.droid``, and since substantially diverged.