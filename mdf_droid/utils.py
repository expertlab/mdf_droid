#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Utilities and constants common to all mdf server-side modules."""
import types
from glob import glob
from mdf_canon.csutil import go as getstatusoutput

from numpy import array

from twisted.web import xmlrpc
from twisted.internet import threads

from . import parameters as params
from mdf_canon import csutil
from mdf_canon.csutil import *

csutil.binfunc = xmlrpc.Binary

psutil = False
if isWindows:
    import psutil


def defer(func):
    """deferToThread decorator"""

    # FIXME
    @functools.wraps(func)
    def defer_wrapper(self, *args, **kwargs):
        # Skip deferring if unittesting
        if params.ut:
            return func(self, *args, **kwargs)
        d = threads.deferToThread(func, self, *args, **kwargs)
        return d

    return defer_wrapper


def listDirExt(dir, ext=params.conf_ext, create=False):
    """Elenca tutti i file con estensione `ext` contenuti nella cartella `dir`"""
    if create:
        if not os.path.exists(dir):
            os.makedirs(dir)
            return []
    l = os.listdir(dir)
    n = len(ext)
    r = []
    for e in l:
        if n > 0:
            if e[-n:] != ext:
                continue
            r.append(e[:-n])
        else:
            r.append(e)
    f = lambda x, y: (x.lower() > y.lower())
    return sorted(r, key=functools.cmp_to_key(f))


def latestFile(path):
    ls = os.listdir(path)
    ls = [path + f for f in ls]
    if not len(ls):
        return None
    latest_file = max(ls, key=os.path.getmtime)
    return latest_file


def newestFileModified(lst):
    mod = [os.stat(f).st_mtime for f in lst]
    i = mod.index(np.max(mod))
    return lst[i]


def oldestFileModified(lst):
    mod = [os.stat(f).st_mtime for f in lst]
    i = mod.index(np.min(mod))
    return lst[i]


def crc(s):
    """Calculate CRC16 MSB+LSB 4 bytes to append each message in a ModBus transaction"""
    v = 0xffff
    is_bytes = True
    if not isinstance(s[0], int):
        s = [ord(b) for b in s]
        is_bytes = False
        
    for b in s:
        v = v ^ b
        for j in range(8):
            if v % 2:
                v = ((v >> 1) ^ 0xA001)
            else:
                v = v >> 1
    lsb, msb = divmod(v, 256)
    if is_bytes:
        r = bytes([msb, lsb])
    else:
        r = chr(msb) + chr(lsb)
    return r


def check_crc(msg):
    c = crc(msg[:-2])
    return c == msg[-2:]


from mdf_canon.csutil import go as gopt


def go(cmd, **kw):
    """Failsafe bash execution"""
    err, out = gopt(cmd, **kw)
    if err != 0:
        print('Error %i executing %s:' % (err, cmd))
        print(out)
    return err, out


# http://code.activestate.com/recipes/219300/
# Convert positive integer to bit string
int2bitstring = lambda n: n > 0 and int2bitstring(n >> 1) + str(n & 1) or ''


def get_history(buf, start_time=False):
    t = buf[-1]
    if t == []:
        return time(), []
    if len(t) > 1:
        t = t[0]
    else:
        t = 0
    # Se start_time non è specificato, restituisce le ultime 10 righe
    if not start_time:
        fromn = -10
    # Altrimenti ricerco nel log la posizione di start_time
    else:
        fromn = buf.get_time(start_time)
    return t, buf[fromn:]


def apply_time_delta(delta):
    """Change hardware clock by `delta` seconds"""
    if delta < 1:
        return 'no time delta'
    print('APPLY TIME DELTA', delta)
    ago = ''
    if delta < 0:
        ago = 'ago'
        delta = abs(delta)
    cmd = "sudo date -s '{} seconds {}'".format(delta, ago)
    r = go(cmd)[1]
    # Sync to hardware clock for next reboot
    r1 = go("sudo hwclock --systohc")[1]
    print('apply_time_delta', cmd, r, r1)
    return 'apply_time_delta {} {} {}'.format(cmd, r, r1)


avahi = False
dbus = False
try:
    import avahi
    import dbus
except:
    pass


class ZeroconfService:

    """A simple class to publish a network service with zeroconf using avahi.
    Credits: http://stackp.online.fr/?p=35
    """

    def __init__(self, name, port, stype="_https._tcp",
                 domain="", host="", text=""):
        self.name = name
        self.stype = stype
        self.domain = domain
        self.host = host
        self.port = port
        self.text = text

    def publish(self):
        bus = dbus.SystemBus()
        server = dbus.Interface(
            bus.get_object(
                avahi.DBUS_NAME,
                avahi.DBUS_PATH_SERVER),
            avahi.DBUS_INTERFACE_SERVER)

        g = dbus.Interface(
            bus.get_object(avahi.DBUS_NAME,
                           server.EntryGroupNew()),
            avahi.DBUS_INTERFACE_ENTRY_GROUP)

        g.AddService(avahi.IF_UNSPEC, avahi.PROTO_UNSPEC, dbus.UInt32(0),
                     self.name, self.stype, self.domain, self.host,
                     dbus.UInt16(self.port), self.text)

        g.Commit()
        self.group = g

    def unpublish(self):
        self.group.Reset()


def testZCS():
    service = ZeroconfService(name="TestService", port=3000)
    service.publish()
    raw_input("Press any key to unpublish the service ")
    service.unpublish()


class void(object):
    pass

# ##
# UDEV UTILITIES
# To uniquely identify a device based on its hw connection
# ##


def query_udev_tree(node):
    """Parses udevinfo about device `node`. Returns a dictionary of values."""
    env = dict(os.environ, LD_LIBRARY_PATH="")
    s, out = go("udevadm info -a -p $(udevadm info -q path -n %s)" % node, env=env)
    if s != 0:
        return False
    return _query_udev_tree(out)


def _query_udev_tree(out):
    tree = []
    begin = False
    prop = False
    for line in out.splitlines():
        if 'looking at device ' in line or 'looking at parent device ' in line:
            if begin and prop:
                tree.append(prop)
            prop = {'ATTRS': {}}
            begin = True
            ip = line.find("'") + 1
            op = line.find("'", ip)
            path = line[ip:op]
            prop['path'] = path
            continue
        elif not begin or '==' not in line:
            continue
        line = line.replace(' ', '')
        key, val = line.split('==')[:2]
        val = val[1:-1]
        if 'ATTRS{' in key:
            key = key[6:-1]
            prop['ATTRS'][key] = val
            continue
        prop[key] = val
    print("query_udev_tree", tree)
    return tree


valid_idProduct = ['6001', 'b3a8', '0483', 'ea60', '081b']
valid_idVendor = ['0403', '046d', '16c0', '10c4']
valid_manufacturer = ['Bison', 'e-consystems', 'Teensyduino']


def query_known_serial(node):
    tree = query_udev_tree(node)
    return _query_known_serial(tree)

    
def _query_known_serial(tree):
    if not tree:
        return False, tree
    for dev in tree:
        # skip non-parent devices
        if 'ATTRS' not in dev:
            continue
        attrs = dev['ATTRS']
        if 'serial' in attrs:
            serial = attrs['serial']
        else:
            continue
        manu, vendor, product = attrs.get('manufacturer', None), \
                                attrs.get('idVendor', None), \
                                attrs.get('idProduct', None)
        # print('found serial', serial, manu, vendor, product)
        if manu in valid_manufacturer:
            return serial, tree
        if vendor in valid_idVendor and product in valid_idProduct:
            return serial, tree
    return False, tree


import string


def validate_obj_name(name, valid=string.ascii_letters + string.digits, replace='_'):
    """Replace all non valid Python object characters with `replace` string"""
    new = ''
    for s in name:
        if s not in valid:
            new += replace
        else:
            new += s
    return new


def query_udev(node):
    # Intercept pure serial nodes
    if node.startswith('/dev/ttyS'):
        return node.split('/')[-1], [{}]
    serial, tree = query_known_serial(node)
    if serial:
        return 's' + serial, tree
    if not tree:
        return False, False
    vbus = []
    for dev in tree:
        bus = ''
        dri = dev.get('DRIVERS')
        if not dri or dri != 'usb':
            continue
        kn = dev.get('KERNELS')
        if kn:
            bus += kn + ':'
        attrs = dev['ATTRS']
        # Check if vid:pid are forbidden
        pid = attrs.get('idProduct', '____')
        vid = attrs.get('idVendor', '____')
        if '%s:%s' % (vid, pid) in params.forbiddenID:
            return False, False
        for a in ['idProduct', 'idVendor', 'serial']:
            if attrs.get(a, False):
                bus += attrs.get(a) + 'I'
        vbus.append(bus[:-1])
    dp = '_'.join(vbus)
    dp = validate_obj_name('p' + dp)
    return dp, tree


def count_usb_devices(supported):
    """Count number of devices in sysfs with matching idVendor and idProduct.
    ` supported`={idVend0:[idProd0,idProd1,...],
                            idVend1:[idProd10,idProd11,...], ... }"""
    n = 0
    r = []
    lst = glob('/sys/bus/usb/devices/*')
    for d in lst:
        fv = d + '/idVendor'
        if not os.path.exists(fv):
            continue
        vid = open(fv, 'r').read()[:-1].upper()
        if vid not in supported:
            continue
        fp = d + '/idProduct'
        if not os.path.exists(fp):
            continue
        pid = open(fp, 'r').read()[:-1].upper()
        if pid not in supported[vid]:
            continue
        r.append('{}/{}:{}'.format(d, vid, pid))
        n += 1
    print('devices', n, r)
    return n


def beep():
    getstatusoutput('beep')


def uname(flags):
    s, o = getstatusoutput('uname ' + flags)
    return o


def validateRow(row):
    """Valida una lista di righe per l'aggiunta ad una tabella"""
    lt = [types.ListType, type(array([]))]
    if not type(row) in lt:
        return False
    if len(row) < 1:
        return False
    if type(row[0]) not in lt:
        row = [row]
    for el in row:
        if type(el) not in lt:
            row.remove(el)
        if len(el) < 1:
            row.remove(el)
    return row

