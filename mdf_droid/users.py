#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
from copy import deepcopy
from mdf_canon.option import mkheader
from . import parameters as params
from . import device
from mdf_canon.csutil import unicode_func

# TODO: translate params variables into Users object options.


class Users(device.Device):

    """Gestione utenti e livelli di autorizzazione."""
    naturalName = 'users'
    conf_def = deepcopy(device.Device.conf_def)
    conf_def.append({"handle": u'users', "name": u'Users List', "type": 'Table',
                     "readLevel":5, "writeLevel":5,
                     "header": mkheader('Name',
                                        ('Read Level', 'Integer'),
                                        ('Write Level', 'Integer'),
                                        'Hash'),
                     "current": [
                         [u'admin', 5, 5, u'admin'],
                         [u'maint', 4, 4, u'maint'],
                         [u'tech', 3, 3, u'tech'],
                         [u'user', 2, 2, u'user'],
                         [u'analyst', 1, 1, u'analyst'],
                         [u'guest', 0, 0, u'guest']
                     ]})

    def __init__(self, parent=None, node='users'):
        device.Device.__init__(self, parent=parent, node=node)
        self.name = 'users'
        self['name'] = 'Users'
        self['comment'] = 'Users Access Control'
        self['devpath'] = 'users'
        self.dic = {}
        self.set_users(self['users'])
        self.logged = [False, 0]
        self.xmlrpc_auth = self.auth
        self.post_connection()

    def set_users(self, val):
        self.dic = {ent[0]: [ent[1], ent[2], ent[3]] for ent in val}
        return val

    def auth(self, credentials):
        """Verify credentials"""
        user = unicode_func(credentials.username)
        if user not in self.dic:
            msg = 'No such user: {}'.format(user)
            self.log.critical(msg)
            return False, msg
        rlev, wlev, pw = self.dic[user]
        if isinstance(pw, str):
            pw = pw.encode('utf-8')
        if isinstance(user, str):
            user = user.encode('utf-8')
        if hasattr(credentials, 'password') and isinstance(credentials.password, str):
            credentials.password = credentials.password.encode('utf-8')
        if credentials.checkPassword(pw):
            return True, user
        msg = 'Wrong password for user: {}, {}, {}'.format(user, type(credentials.username), type(pw), repr(credentials.password))
        self.log.critical(msg)
        return False, msg

    def levels(self, user):
        user = unicode_func(user)
        if user not in self.dic:
            self.log.error('Unknown user', user)
            return 0, 0
        r = self.dic[user][:2]
        return r

    def logout(self, userName=''):
        if self.logged[0] != userName:
            self.log.warning(
                'User asked to logout without being logged in: ', userName)
            return 'You were not logged in'
        self.logged = [False, 0]
        if params.exclusiveLogin:
            self.log.info('Successfull logout for user: ', userName)
            return 'Logout successfull.'
        else:
            msg = 'Exclusive login is not active. Logout is useless.'
            self.log.info(msg)
            return msg

    xmlrpc_logout = logout
