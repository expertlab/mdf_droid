#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from copy import deepcopy
from shutil import copytree, rmtree
import datetime
from time import time
from mdf_canon.csutil import go
from . import parameters as params
from . import device
from mdf_droid.version import __version__
from mdf_canon.linux import get_lib_info, get_process_stats, parse_vmstat, get_total_processes
tar_log_limit = 1000000


def get_today_string():
    return datetime.datetime.now().strftime("%Y_%m_%d_%H-%M-%S")


class Support(device.Device):

    """Support functionalities. Backup/restore, upgrade, remote assistance."""
    naturalName = 'support'
    original_firmware = False
    conf_def = deepcopy(device.Device.conf_def)
    conf_def += [
        # Conf Backups
        {"handle": 'stopUI', "name": 'Stop embedded UI', "current": False, "type": 'Boolean', "writeLevel":5},
        {"handle": u'logs', "name": u'Log archive',
            "type": 'FileList', 'attr': ['Runtime']},
        {"handle": u'doLogs', "name": u'Refresh log archive',
            "type": 'Button', "parent": 'logs'},
        {"handle": u'backups', "name": u'Available backups',
            "type": 'FileList', 'attr': ['Runtime']},
        {"handle": u'doBackup', "name": u'New configuration backup',
            "type": 'Button', "parent": 'backups'},
        {"handle": u'doRestore', "name": u'Restore configuration backup',
            "type": 'Button', "parent": 'backups'},
        {"handle": u'backupPackage', "name": u'Last applied backup name',
            "type": 'String', 'attr': ['ReadOnly'], "parent":'backups'},
        {"handle": u'backupDate', "name": u'Last applied backup date',
            "type": 'String', 'attr': ['ReadOnly'], "parent":'backups'},
        # Exe backups
        {"handle": u'exeBackups', "name": u'Available software backups',
            "type": 'FileList', 'attr': ['Runtime']},
        {"handle": u'doExeBackup', "name": u'New software backup',
            "type": 'Button', "parent": 'exeBackups'},
        {"handle": u'doExeRestore', "name": u'Restore software backup',
            "type": 'Button', "parent": 'exeBackups'},
        # Progress
        {"handle": u'backupProgress', "name": u'Backup/restore progress',
            "type": 'Progress', "attr": ['Runtime']},

        {"handle": u'packages', "name": u'Available software versions',
            "type": 'FileList', 'attr': ['Runtime']},
        {"handle": u'applyExe', "name": u'Apply selected software version',
            "type": 'Button', "parent": 'packages'},
        {"handle": u'upgradeProgress', "name": u'Upgrade/restore progress',
            "type": 'Progress', "attr": ['Runtime']},
        # System info
        {"handle": u'version', "name": u'Mdf version',
            "type": 'String', 'attr': ['ReadOnly']},
        {"handle": u'versionPackage', "name": u'Last applied package name',
            "type": 'String', 'attr': ['ReadOnly'], "parent":'version'},
        {"handle": u'versionDate', "name": u'Last applied package date',
            "type": 'String', 'attr': ['ReadOnly'], "parent":'version'},
        {"handle": u'versionString', "name": u'Extended version string',
            "type": 'TextArea', 'attr': ['ReadOnly'], "parent":'version'},
        {"handle": u'versionPath', "name": u'Executable path',
            "type": 'String', 'attr': ['ReadOnly'], "parent":'version'},
        {"handle": u'libs', "name": u'Loaded libraries info',
            "type": 'Button'},
        {"handle": u'env', "name": u'Environment variables', "type": 'Button'},
        {"handle": u'dmesg', "name": u'System logs', "type": 'TextArea'},

        # Network
        {"handle": u'network',
            "name": u'Apply network configuration', "type": 'Button'},
        {"handle": 'dhcp', "name": 'Autoconfigure with DHCP',
         "current": True, "type": 'Boolean', "parent": 'network'},
        {"handle": 'staticip', "name": 'Static IP',
            "type": 'String', "parent": 'network'},
        {"handle": 'netmask', "name": 'Static Netmask',
            "type": 'String', "parent": 'network'},
        {"handle": 'gateway', "name": 'Static Gateway',
            "type": 'String', "parent": 'network'},
        {"handle": u'reboot', "name": u'Reboot machine OS', "type": 'Button'},
        {"handle": u'halt', "name": u'Shutdown machine OS', "type": 'Button'},
        
        {"handle": u'sys', "name": u'System Info', "type": 'Section'},
        {"handle": u'sys_usedRam', "name": u'Used RAM', "unit": "percent",
            "attr":['ReadOnly', 'History'], "type": 'Float'},
        {"handle": u'sys_ram', "name": u'Total RAM', "unit": "megabyte",
            "attr":['ReadOnly'], "type": 'Float'},
        {"handle": u'sys_usedSwap', "name": u'Used swap', "unit": "percent",
            "attr":['ReadOnly', 'History'], "type": 'Float'},
        {"handle": u'sys_swap', "name": u'Total swap', "unit": "megabyte",
            "attr":['ReadOnly', 'History'], "type": 'Float'},
        {"handle": u'sys_cpu', "name": u'CPU load', "unit": "percent",
            "attr":['ReadOnly', 'History'], "type": 'Float'},
        {"handle": u'sys_cpuTicks', "name": u'Active CPU ticks',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Float'},
        {"handle": u'sys_cpuTicksIdle', "name": u'Idle CPU ticks',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Float'},
        {"handle": u'sys_temp', "name": u'CPU Temperature', "unit": 'celsius',
            "attr":['ReadOnly', 'History'], "type": 'Float'},
        {"handle": u'sys_time', "name": u'Last system read',
            "attr":['ReadOnly', 'Runtime'], "type": 'Time'},
        
        {"handle": u'proc', "name": u'Server Process', "type": 'Section'},
        {"handle": u'proc_n', "name": u'Active server processes',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Integer'},
        {"handle": u'proc_tot', "name": u'Total processes',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Integer'},
        {"handle": u'proc_cpu', "name": u'Highest CPU Usage', "unit": 'percent',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Float'},
        {"handle": u'proc_cpuTot', "name": u'Total CPU Usage', "unit": 'percent',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Float'},
        {"handle": u'proc_mem', "name": u'Highest Memory', "unit": 'percent',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Float'},
        {"handle": u'proc_memTot', "name": u'Total Memory', "unit": 'percent',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Float'},
         {"handle": u'proc_resident', "name": u'Highest Resident', "unit": 'megabyte',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Float'},
        {"handle": u'proc_virtual', "name": u'Highest Virtual', "unit": 'megabyte',
            "attr":['ReadOnly', 'Runtime', 'History'], "type": 'Float'},
        
    ]

    def __init__(self, parent=None, node='support'):
        device.Device.__init__(self, parent=parent, node=node)
        self.name = 'support'
        self['name'] = 'Support'
        self['comment'] = 'Support, upgrade, backup, restore, get assistance'
        self['devpath'] = 'support'
        self.post_connection()
        self.set_stopUI(self['stopUI'])
        self.vmstat()
        if os.path.exists(params.version_file):
            self['versionString'] = open(params.version_file, 'r').read()
            
    def set_stopUI(self, val):
        if val:
            self.log.debug('Stopping LightDM')
            r = go('sudo service lightdm stop')
            self.log.debug(r[1])
        return val

    excl_conf = '--exclude "sessile_betas.h5" --exclude "*/data/*" --exclude "*/support/*/*" '
    """Exclude files from configuration backup."""

    # excl_exe='--exclude "*.py"'
    excl_exe = '--exclude "*.h5" --exclude "*/tests/storage/*" --exclude "*/.svn/*" --exclude "*/.git/*" \
--exclude "*/opencv/*" --exclude "*/mdf_client/*" --exclude "*/veusz/*" --exclude "*/mdf.doc/*" \
--exclude "*/dil/*" --exclude "*/glaze/*" --exclude "*/thegram/*" --exclude "*/libftdi/*" --exclude "*/build/*" \
--exclude "*/install/*" --exclude "*/pkg/*" --exclude "*/wiki/*" --exclude "*/tests/*" --exclude "*/.cache/*"'
    """Exclude files from exe backups"""

    def do_backup(self, source, odir, excl='', outfile=False, overwrite=False):
        """Generalized backup."""
        if params.isWindows:
            return 'unsupported', 'Unsupported'
        if not os.path.exists(odir):
            os.makedirs(odir)
        # TODO: migrate to tarfile implementation providing progress updates
        if not outfile:
            outfile = get_today_string()
        outfile = os.path.join(odir, outfile)
        n = 1
        outfile1 = outfile
        while os.path.exists(outfile1 + '.tar.bz2'):
            if overwrite:
                os.remove(outfile1 + '.tar.bz2')
                break
            outfile1 = '{}_{}'.format(outfile, n)
            n += 1
        cmd = 'tar {} -cvhf {}.tar.bz2 -C "{}" .'.format(
            excl, outfile1, source, source)
        self.log.info('Starting backup:', cmd)
        r = go(cmd)
        msg = r[1]
        if len(msg) > tar_log_limit:
            self.log.debug('Truncating backup output', len(msg))
            msg = msg[:tar_log_limit] + '\n...[truncated]...'
        self.log.info('New backup was successful. These files were archived:', r[0], msg)
        return outfile1 + '.tar.bz2', msg
    
    def check_package(self, source):
        if source.endswith('.run'):
            st, msg = go('bash "{}" --check'.format(source))
            if st != 0:
                self.log.critical(msg)
                return False, msg
            return True, 'Self-installing package is ok'
        # Check integrity of the data
        cmd = 'tar -tf "{}"'.format(source)
        r = go(cmd)
        if r[0] != 0:
            msg = r[1]
            if len(msg) > 300:
                msg = '...[truncated]...\n' + msg[-300:]
            self.log.critical('Package is defective. Restore aborted!\n', msg)
            return False, 'INVALID ARCHIVE!!! \n' + msg 
        self.log.debug('Valid archive', source)
        return True, 'Valid archive'

    def do_restore(self, source, dest):
        """Generalized restore. Returns status and message."""
        if params.isWindows:
            return False, 'Unsupported'
        if not os.path.exists(source):
            self.log.error('Selected backup file does not exist:', source)
            return False, 'Selected backup does not exist: ' + source
        r, msg = self.check_package(source)
        if not r:
            self.log.critical('do_restore: Invalid archive. Aborted.')
            return False, msg
        if source.endswith('.run'):
            # Execute a self-installing script
            cmd = 'bash "{}"'.format(source)
        else:
            # As config is stored with absolute paths, a simple untar should
            # restore everything
            cmd = 'tar -C "{}" -xvf "{}"'.format(dest, source)
        self.log.info('Restoring backup:', cmd)
        r = go(cmd)
        msg = r[1]
        msg = msg.replace('\x08     ', '').replace('\x08', '')
        if len(msg) > tar_log_limit:
            self.log.debug('Truncating restore output', len(msg))
            msg = msg[:tar_log_limit] + '\n...[truncated]...'
        
        log = 'archive {} to {} [exit:{}]:'.format(source, dest, r[0])
        if r[0] != 0:
            self.log.error('Failed restoring ' + log, msg)
            r = False
        else:
            self.log.info('Successfully restored ' + log, msg)
            r = True
        return r, msg
    
    def save_last_version_info(self, prefix, package_name):
        self[prefix + 'Package'] = package_name
        self[prefix + 'Date'] = get_today_string()
        self.save('default')
        
    def get_versionPath(self):
        return params.mdfServerExe     

    def get_doBackup(self):
        """Perform configuration backup."""
        odir = self.desc.getConf_dir() + 'backups/'
        outfile, msg = self.do_backup(params.confdir, odir, self.excl_conf)
        self['backups'] = os.path.basename(outfile)
        return msg
    
    def get_doLogs(self):
        """Perform logs backup."""
        odir = self.desc.getConf_dir() + 'logs/'
        outfile, msg = self.do_backup(params.confdir + 'data/log/', odir, outfile='logs', overwrite=True)
        self['logs'] = os.path.basename(outfile)
        return msg

    def get_doRestore(self):
        """Perform configuration restore"""
        source = self.desc.getConf_dir() + 'backups/' + self['backups']
        status, msg = self.do_restore(source, params.confdir)
        if status:
            self.save_last_version_info('backup', 'backups://' + self['backups'])
        return msg
    
    def project_root(self):
        return params.sep.join(params.mdir.split(params.sep)[:-4])
    
    def bin_path(self):
        if params.mdfServerExe.endswith('server.bin'):
            return params.mdfServerExe
        return False

    def get_doExeBackup(self):
        """Perform executable backup."""
        odir = self.desc.getConf_dir() + 'exeBackups/'
        # Try to backup only server.bin, containing the code
        source = self.bin_path()
        if not source:
            source = self.project_root()
        if source == '/opt':
            msg = 'Cannot backup from source'
            self.log.debug(msg)
            return msg
        outfile, msg = self.do_backup(source, odir, self.excl_exe)
        self['exeBackups'] = os.path.basename(outfile)
        return msg
    
    def firmware(self):
        r = os.path.join(self.desc.getConf_dir(), '..', 'firmware/')
        r = os.path.abspath(r)
        if not os.path.exists(r):
            os.makedirs(r)
        return r

    def get_doExeRestore(self):
        """Perform executable restore"""
        source = self.desc.getConf_dir() + 'exeBackups/' + self['exeBackups']
        odir = self.firmware()
        status, msg = self.do_restore(source, odir)
        if status:
            self.save_last_version_info('version', 'exeBackups://' + self['exeBackups'])
        return msg

    def get_applyExe(self):
        """Apply software version."""
        if params.isWindows:
            return 'Unsupported'
        source = self.desc.getConf_dir() + 'packages/' + self['packages']
        if not os.path.exists(source) or not self['packages']:
            msg = 'Software version does not exist: impossible to apply.', source
            self.log.error(msg)
            return msg
        sh = False
        if source.endswith('.run'):
            self.log.info('Provided a self-installing package', source)
            sh = True
        elif not (source.endswith('.tar') or source.endswith('.tar.gz')):
            msg = 'Selected upgrade package is invalid. Removing.\n' + source
            self.log.error(msg)
            os.remove(source)
            return msg
        
        r, msg = self.check_package(source)
        if not r:
            self.log.critical('get_applyExe: aborted!')
            return msg
        # Prepare number of steps
        self.setattr('upgradeProgress', 'max', 4)
        # Tell the main server which operation is in progress
        self.root.set('progress', self['fullpath'] + 'upgradeProgress')
        self['upgradeProgress'] = 1
        # First do a software backup:
        self.get_doExeBackup()
        self['upgradeProgress'] = 2
        # Then do a configuration backup:
        self.get_doBackup()
        self['upgradeProgress'] = 3
        # Clean the firmware dir
        dest_dir = self.firmware()
        if self.original_firmware:
            bin_path = self.original_firmware
        else:
            bin_path = self.bin_path()
        # Reset firmware folder to the original firmware
        rmtree(dest_dir)  # Delete entirely
        
        if not sh:
            self.log.debug('Restoring original firmware environment', os.path.dirname(bin_path), dest_dir)
            copytree(os.path.dirname(bin_path), dest_dir)  # Copy in block
            self.log.debug('Copied original firmware environment to upgrade folder', dest_dir)
        # Lastly, restore to the selected exe version
        status, r = self.do_restore(source, dest_dir)
        self['upgradeProgress'] = 0
        if status:
            self.save_last_version_info('version', 'packages://' + self['packages'])
            msg = 'Upgrade to {} finished successfully. \nPlease restart Mdf to apply it!'.format(self['packages'])
        else:
            msg = 'Failed to upgrade to {}!'.format(self['packages'])
        self.log.critical(msg)
        return msg + '\n' + r

    def get_version(self):
        """Get current mdf version"""
        return __version__

    def get_libs(self):
        """Get information about loaded libraries"""
        return get_lib_info()

    def get_env(self):
        """Get environment variables"""
        if params.isWindows:
            return 'unsupported'
        r = go('env')
        return r[1]
    
    def get_dmesg(self):
        if os.name == 'nt':
            return 'NotImplemented'
        s, out = go('dmesg')
        return out

    def get_network(self):
        """Apply network configuration"""
        # Open /etc/network/interfaces and write current config
        return 'NotImplemented'

    def get_reboot(self):
        if params.isWindows:
            return 'unsupported'
        r = go('sudo reboot')
        self.log.warning('Reboot requested. Result:', r)
        return r

    def get_halt(self):
        if params.isWindows:
            return 'unsupported'
        r = go('sudo halt -p')
        self.log.warning('Shutdown requested. Result:', r)
        return r
    
    def vmstat(self):
        if params.isWindows:
            return {}
        vm = parse_vmstat()
        t = time()
        d = get_process_stats("MdfServer")
        d['proc_tot'] = get_total_processes()
        d.pop('proc_pid')
        d['sys_ram'] = vm['M total memory']
        d['sys_swap'] = vm['M total swap']
        d['sys_usedRam'] = 100.*vm['M used memory'] / vm['M total memory']
        if vm['M total swap']:
            d['sys_usedSwap'] = 100.*vm['M used swap'] / vm['M total swap']
        cpu = [vm[k] for k in ['non-nice user cpu ticks',
                               'nice user cpu ticks',
                               'IO-wait cpu ticks',
                               'IRQ cpu ticks',
                               'softirq cpu ticks',
                               'stolen cpu ticks']]
        dt = t - self['sys_time']
        if dt < 0:
            d['sys_time'] = t
            self.updateCurrent(d, t=t)
            return vm, d
        
        cpu = sum(cpu)
        dcpu = cpu - self['sys_cpuTicks']
        didle = vm['idle cpu ticks'] - self['sys_cpuTicksIdle']
        d['sys_cpu'] = 100.*dcpu / (dcpu + didle)
        d['sys_time'] = t
        d['sys_cpuTicks'] = cpu
        d['sys_cpuTicksIdle'] = vm['idle cpu ticks']
        
        self.updateCurrent(d, t=t)
        return vm, d
    
    def get_sys_usedRam(self):
        vm, d = self.vmstat()
        return d['sys_usedRam']
    
    def get_sys_usedSwap(self):
        self.vmstat()
        return self.desc['sys_usedSwap']
    
    def get_sys_cpu(self):
        self.vmstat()
        return self.desc['sys_cpu']
    
    def get_sys_temp(self):
        """Returns the maximum temperature found in all thermal zones"""
        if params.isWindows:
            return 0
        st, msg = go('cat /sys/class/thermal/thermal_zone*/temp')
        if st != 0:
            return 0
        temps = list(map(float, msg.splitlines()))
        return max(temps) / 1000.0
    
    def check(self):
        vm, d = self.vmstat()
        T = self.get_sys_temp()
        self['sys_temp'] = T
        a = [d.get('proc_' + k, -1) for k in ('mem', 'cpu', 'resident', 'virtual')]
        a.append(T)
        self.log.debug('%MEM={}, %CPU={}, Resident={:.1f}MB, Virtual={:.1f}MB Temp={:.1f}'.format(*a))
        return super(Support, self).check()

