#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
from unittest import mock
import os
from mdf_droid import tests
from mdf_droid.data import refupdater
from mdf_canon.mem import  dirshelf
from mdf_canon.option import Option
from mdf_canon.indexer import SharedFile
from time import time, sleep
from mdf_canon.csutil import dumps
print('Importing', __name__)


def setUpModule():
    print('Starting', __name__)
    tests.reset_test_memory()

dspath = '/dev/shm/mdf_test_droid/test'
if os.name=='nt':
    dspath='C:\\dev\\shm\\mdf_test_droid\\test'

class TestRefUpdater(unittest.TestCase):

    def setUp(self):
        self.zerotime = time()
        self.k = os.path.join(dspath,'refup', 'h', 'self')
        if os.path.exists(self.k):
            os.remove(self.k)
        self.sh = SharedFile('reftest.h5', mode='w')
        self.ru = refupdater.ReferenceUpdater(dspath, self.sh, self.zerotime)
        self.ds = dirshelf.DirShelf(dspath, 'refup', {'h': Option(
            handle='h', type='Integer', attr=['History'], kid='/h')})

    def tearDown(self):
        self.ds.close()
        self.ru.close()
        self.sh.close()
        
    @unittest.skip('Maybe not needed')
    def test_dump(self):
        dumps(self.ru)

    def changeval(self, nval):
        print('CHANGEVAL', nval)
        t0 = time()
        sleep(0.1)
        t1 = time()
        self.ds.set_current('h', nval, t1)
        sleep(0.1)
        t2 = time()
        self.assertTrue(self.ru.sync())
        rt = self.ru.cache[self.k].mtime
        print('{:f} < {:f} < {:f}'.format(t0, rt, t2))
        self.assertAlmostEqual(rt, t1, delta=1e-3)
        self.assertGreater(rt, t0)
        self.assertLess(rt, t2)

    def test_refupdater(self):
        # FIXME: The reference is created but the first point is missed
        self.ru.nthreads = 0
        self.ru.sync()
        self.assertTrue(self.k in self.ru.cache)
        rt = self.ru.cache[self.k].mtime
        self.assertAlmostEqual(rt, self.zerotime, delta=0.1)

        self.changeval(1)

        # Check double appending
        for i in range(10):
            print('Changing values', i)
            self.changeval(i + 2)
        sleep(5)
        for i in range(10):
            print('Sync', i)
            self.ru.sync()
        # Check summary
        for i in range(10):
            print('Changing values', i)
            self.changeval(i + 2)
            sleep(1)
            
    def test_collect(self):
        buf = mock.MagicMock()
        buf.sequence_from_time = lambda *a: [] #TODO
        ru = self.ru
        ref = unittest.mock.MagicMock()
        ref.mtime = 0
        ref.encode = lambda e: e
        ref.opt.get = lambda *a: [] #TODO
        ru.cache['a'] = ref
        ru.collect('a', buf)


if __name__ == "__main__":
    unittest.main(verbosity=2)
