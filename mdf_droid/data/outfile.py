# -*- coding: utf-8 -*-
"""Server-side SharedFile implementation for runtime data collection."""
from traceback import print_exc, format_exc
from Crypto import Random
from Crypto.PublicKey import RSA
import Crypto.Hash.SHA as SHA
import Crypto.Signature.PKCS1_v1_5 as PKCS1_v1_5

from mdf_canon import indexer
from mdf_canon.csutil import lockme
from .. import parameters as params

from .refupdater import ReferenceUpdater


def sign(f, cacert=False, privkey=False):
    """Digitally sign a file using server's private key."""
    # TODO: use config options?
    # Save public certificate
    Random.atfork()
    if not cacert:
        cacert = params.ssl_cacert
    f.root.conf._v_attrs.cert = open(cacert, 'r').read()
    # Read the certificate
    if not privkey:
        privkey = params.ssl_private_key
    private_key = open(privkey, 'r').read()
    # Create the key
    key = RSA.importKey(private_key)
    verifier = PKCS1_v1_5.new(key)
    # Public key
    public_key = key.publickey()
    f.root.conf._v_attrs.public_key = public_key.exportKey()
    print('PubKey', public_key, f.root.conf._v_attrs.public_key)

    data = indexer.calc_hash(f)

    # Create message digest
    h = SHA.new(data.encode('utf8'))
    # Create the signature
    signature = verifier.sign(h)
    f.root.conf._v_attrs.signature = signature
    f.flush()
    return True


class OutputFile(indexer.SharedFile):

    """Server-side SharedFile implementation for runtime data collection."""
    updater = False
    stopped = False
    last_error = ""

    def __init__(self, *a, **kw0):
        print('OutputFile', kw0)
        kw = kw0.copy()
        dbpath = kw.pop('shm_path', None)
        zerotime = kw.pop('zerotime', None)
        indexer.SharedFile.__init__(self, *a, **kw)
        if None not in [dbpath, zerotime]:
            self.set_updater(dbpath, zerotime)
        
    def set_updater(self, dbpath, zerotime=-1):
        if self.updater:
            self.log.debug('OutputFile.set_updater: Closing existing updater')
            self.updater.close()
        self.log.debug('OutputFile.set_updater', dbpath, zerotime)
        self.updater = ReferenceUpdater(outfile=self,
                                        zerotime=zerotime)
        self.stopped = False
    
    def xmlrpc_monitored(self):
        if not self.updater:
            return []
        return self.updater.paths
    
    def xmlrpc_excluded(self):
        if not self.updater:
            return []
        return list(self.updater.exclude)

    def sync(self, zerotime=-1, only_logs=False):
        try:
            return self.updater.sync(zerotime=zerotime, only_logs=only_logs)
        except:
            self.last_error = format_exc()
            self.log.error('OutputFile.sync() ERROR', self.last_error)
            return False
        
    def get_last_error(self):
        return self.last_error
        
    def stop(self):
        """Close the updater threadpool."""
        if self.updater and not self.stopped:
            self.updater.close()
        self.stopped = True
        return True

    def close(self):
        """Implicitly call self.stop()"""
        self.stop()
        self.header(refresh=True)
        return indexer.SharedFile.close(self, all_handlers=True)

    @lockme()
    def sign(self, cacert=False, privkey=False):
        return sign(self.test)
