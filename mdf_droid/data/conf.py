# -*- coding: utf-8 -*-
"""Data management and formats"""
from .persistent import PersistentConf
from .. import parameters as params
from .. import utils
from mdf_canon import option
from mdf_canon import logger
from mdf_canon.csutil import py
from mdf_canon.mem import dirshelf


class ConfigurationDictionary(PersistentConf):

    """Share-able configuration class for mdf objects."""

    def __init__(self, desc={}, conf_dir='/tmp/',
                 buffer_length=params.buffer_length, empty=False):
        PersistentConf.__init__(self, conf_dir=conf_dir)
        option.Conf.__init__(self, desc=desc, empty=empty)
        self.buffer_length = buffer_length
        from mdf_droid import share
        self.desc = dirshelf.DirShelf(desc=desc)
        self.kid_base = str(id(self))
        self.log = logger.SubLogger(self)
        self.db = share.database
    
    def h_get(self, *a, **k):
        return self.desc.h_get(*a, **k)
    
    def h_time_at(self, *a, **k):
        return self.desc.h_time_at(*a, **k)
    
    def h_get_time(self, *a, **k):
        return self.desc.h_get_time(*a, **k)

    def h_get_history(self, *a, **k):
        return self.desc.h_get_history(*a, **k)

    def h_clear(self, *a, **k):
        return self.desc.h_clear(*a, **k)

    def h_get_time_value(self, *a, **k):
        return self.desc.h_get_time_value(*a, **k)
    
    def read_current(self, *a, **k):
        return self.desc.read_current(*a, **k)
    
    def close(self):
        self.desc.close()

    @property
    def dir(self):
        return self.desc.dir

    def fp(self, name):
        return self.desc.fp(name)

    def set_current(self, name, nval, t=-1):
        if name == 'log':
            level, message = nval
            if py == 2:
                message = message.decode('utf-8', errors='replace')
            if self.db:
                self.db.put_log(
                    message, o=self.get_current('fullpath', 'Unknown'), p=level)
            nval = [level, message]
        self.desc.set_current(name, nval, t=t)
        return nval

    def get_current(self, name, *a):
        try:
            return self.desc.get_current(name)
        except:
            if len(a) == 1:
                return a[0]
            raise
    
    def set(self, name, nval, t=-1):
        """Sets a key, triggering history recording if set on the attributes. 
        `t` forces a different time."""
        if t < 0 or t is None:
            t = utils.time()
        nval = super().set(name, nval, t=t)
        return nval
    
    def set_meta(self, name, val):
        return self.desc.set_meta(name, val)
        
    def get_meta(self, name):
        return self.desc.get_meta(name)

    def describe(self, *excl):
        """Returns a dictionary of configuration options, each one rendered as a dictionary. 
        Runtime options are not included (attr `Runtime`).
        """
        desc = {}
        if len(excl) == 0:
            excl = ['Runtime', 'Binary', 'Object', 'Profile', 'Image']
        elif isinstance(excl, list) or isinstance(excl, tuple):
            excl = excl[0]
        excl = set(excl)
        for k, e in self.desc.items():
            try:
                par = set(e['attr'] + [e['type']])
            except:
                print('describe:', k, e)
                raise
            entry = e.entry
            # Update to current values
            if self.desc[k]['current'] is None:
                print('Found None value', k)
                entry['current'] = 'None'
            # Replace with factory default if type/attr is not requested
            if len(par - excl) < len(par):
                entry['current'] = entry['factory_default']
            desc[k] = entry
        return desc

    def mb(self, name):
        """Returns modbus parameter, if set."""
        if 'mb' in self.desc[name]:
            return int(self.desc[name]['mb'])
        return None

    def update(self, newdict, current=True):
        """Updates conf definition by merging dictionary `newdict`.
        Avoid keepnames."""
        opts = {}
        for k, v in newdict.items():
            if k in self.keepnames:
                continue
            if isinstance(v, dict):
                v = option.Option(**v)
            opts[k] = v
        self.desc.update(opts)
        self.validate()
        
    _keyset = None

    def keyset(self):
        if not self._keyset:
            self._keyset = set(self.desc.keys())
        return self._keyset

    def updateCurrent(self, currentDict, t=-1):
        """Updates current values to `currentDict`. 
        History options are recorded, optionally at time `t`.
        Returns the number of errors encountered."""
        if t < 0:
            t = utils.time()
        e = 0
        keys = self.keyset()
        for key, val in currentDict.items():
            if val is None:
                self.log.warning('Asked to set a property to None', key)
                e += 1
                continue
            if (key not in keys):
                e += 1
                self.log.debug('Asked to update non-existent property:', key)
                continue
            self.set_current(key, val, t)
        return e
    
