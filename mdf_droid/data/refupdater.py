# -*- coding: utf-8 -*-
"""Recursive Reference updater from DirShelf"""
from time import sleep
from traceback import print_exc, format_exc
import threading
import concurrent.futures
import numpy as np
from mdf_canon import reference
from mdf_canon.csutil import lockme, sharedProcessResources, executor
from mdf_canon.mem import MemBuffer

# TODO: Evaluate inotifyx for more efficient scan!

log_marker = '/log/self'

            
class ReferenceUpdater(object):
    outfile = False
    zerotime = 0
    nthreads = 0  # 0 for no-threads
    scanning = 0
    paths = None
    _lockme_error = False
    only_logs = False
    
    def __init__(self, base='', outfile=False, zerotime=0):
        self.base = base
        self._lock = threading.Lock()
        sharedProcessResources.register(self.restore_lock, self._lock)
        self.pool = {}  # thread_index: job
        self.callback_result = []
        # Direct initialization
        if outfile:
            self.reset(outfile, zerotime)
            
    def restore_lock(self, lk):
        print('ReferenceUpdater.restore_lock')
        self._lock = lk
        
    def __getstate__(self):
        r = self.__dict__.copy()
        r.pop('_lock')
        return r
    
    def __setstate__(self, s):
        list(map(lambda a: setattr(self, *a), s.items()))
        self._lock = threading.Lock()

    def close(self):
        """Stop threaded operations and commit latest results"""
        self.running = False
        concurrent.futures.wait(self.pool.values())
        n = self.commit_results(self.callback_result)
        for ref in self.cache.values():
            ref.update_last_edit_time()
        print('ReferenceUpdater.close: committed results', n)
        MemBuffer.remove_base('summary/')
        return True

    def __del__(self):
        self.close()

    @lockme()
    def reset(self, outfile, zerotime=0):
        """Prepare for a new acquisition"""
        self.zerotime = zerotime
        self.outfile = outfile
        self.callback_result = []
        self.pool = {}
        self.idx = 0
        self.running = False
        self.paths = {}  # dict of path:kid where new data may be found
        self.link = {}  # dict of kid:[RoleIO paths] for linked RoleIO paths
        self.cache = {}  # path: Reference
        self.linked = set([])
        self.exclude = set()
        # Initialize file buffers
        self.buffers = []
        for i in range(self.nthreads or 1):
            fb = MemBuffer()
            self.buffers.append(fb)
        self.main_buffer = MemBuffer()
        self.log = self.outfile.log
        MemBuffer.remove_base('summary/')
        print('Refupdater.reset DONE')
        self.log.debug('Refupdater.reset DONE')

    def recursive_link(self, data_source, ref):
        """Recursively create all defined links"""
        # FIXME: this is not safe if process died while creating a link and is re-created
        linked = self.link.get(data_source, [])
        for p in linked:
            if (p, ref.path) in self.linked:
                continue
            if not self.outfile.has_node(p):
                self.outfile.link(p, ref.path)
            self.linked.add((p, ref.path))
            link_name = '/summary' + p
            if ref.__class__.__name__ == 'Array' and not self.outfile.has_node(link_name):
                self.outfile.link(link_name, ref.summary.path)
            # Recursively create nested links
            self.recursive_link(p, ref)

    def get_cache(self, path, fbuffer):
        """If the path is not in cache, evaluate if eligible and add it."""
        if path in self.exclude:
            return False
        # Try to recover from cache
        ref = self.cache.get(path, False)
        if ref:
            return ref
        # Creating new reference
        t, opt = fbuffer.read_meta(path)
        Cls = reference.get_reference(opt)
        # Output folder is red from the option KID attribute
        ref = Cls(self.outfile, opt=opt)
        ref.mtime = self.zerotime
        # Save in cache
        self.cache[path] = ref
        self.recursive_link(opt['kid'], ref)
        self.outfile.flush()
        return ref
    
    def add_path(self, path):
        """Check if `path` should be added to self.path for monitoring.
        Only objects with 'History' attr are considered."""
        # path = os.path.join(path, 'self') # for FileBuffer instead of RedisBuffer
        # if History is not specified, exclude this path from further sync
        try:
            t, opt = self.main_buffer.read_meta(path)
        except:
            print('add_path', path)
            print_exc()
            self.exclude.add(path)
            return False
        attr = opt.get('attr', [])
        if opt.get('type', 0) == 'RoleIO':
            lk = opt['options']
            if lk[0] in (None, 'None'):
                self.exclude.add(path)
                return False
            # Remember kid of linked RoleIO
            kid = lk[0] + lk[2]
            if kid not in self.link:
                self.link[kid] = []
            self.link[kid].append(opt['kid'])
            # TODO: Should record anyway if defines History attr
            # but referred one does not...?
            self.exclude.add(path)
            return False
        if ('History' not in attr) and (opt.get('history', 0) == 0):
            self.exclude.add(path)
            return False
        # Monitor the path
        self.paths[path] = opt['kid']
        return True

    @lockme()
    def sync(self, zerotime=-1, only_logs=False):
        """Sync all entries.
        Returns number of monitored paths and updated ones."""
        self.only_logs = only_logs
        if self.paths is None:
            print('ReferenceUpdater.sync: no paths defined.', self.paths)
            self.paths = {}
            return False, 0
        N = len(self.paths)

        # if no path is being monitored, scan the entire memory
        if N == 0:
            print('Walking', self.base)
            MemBuffer.walk(self.base, self.add_path)
            N = len(self.paths)
            print('Walked', N, 'excluded', len(self.exclude))
            # print('Linked', self.link)
            if N == 0:
                print('No path found')
                return False, 0

        if zerotime >= 0:
            self.zerotime = zerotime

        # Call the scan function in 5 different threads
        self.batch_length = N // (self.nthreads or 1)
        if not self.nthreads:
            self.single_threaded_scan()
        else:
            self.multi_threaded_scan()
        sc = len(self.callback_result)
        sc = self.commit_results(self.callback_result)
        self.callback_result = []
        return N, sc

    def multi_threaded_scan(self):
        # WARNING: might cause silent failure in data recording!
        new_pool = {}
        stopped = 0
        for thread_index, job in self.pool.items():
            if job.running():
                new_pool[thread_index] = job
                continue
            print('multi_threaded_scan job ended', thread_index)
            self.log.warning('multi_threaded_scan job ended', thread_index)
            stopped += 1
        self.running = True
        started = 0
        keys = list(self.paths.keys())
        for thread_index in range(self.nthreads):
            if thread_index in new_pool:
                continue
            print('Starting scan thread', thread_index, self.idx)
            start = self.batch_length * thread_index
            end = len(keys)
            buffer = self.buffers[thread_index]
            if thread_index < self.nthreads - 1:
                end = self.batch_length * (thread_index + 1)
            job = executor().submit(self.scan_loop, keys[start:end], buffer, thread_index)
            new_pool[thread_index] = job
            started += 1
            self.idx += 1
        self.pool = new_pool
        return started
                
    def single_threaded_scan(self):
        self._lock.release()
        self.callback_result = self.scan(self.paths.keys(), self.buffers[0])
        self._lock.acquire()

    def commit_results(self, result=[]):
        """When a scan ends, write out the result"""
        c = 0
        for path, elems in result:
            if self.only_logs and not path.endswith(log_marker):
                continue
            if elems is False:
                # Just create an empty ref
                ref = self.get_cache(path, self.main_buffer)
                continue
            # Waiting for new points
            if len(elems) == 0:
                continue
            # Retrieve saved ref
            ref = self.cache.get(path, False)
            if ref is False:
                print('Failed finding reference!')
                continue
            self.commit(ref, elems)
            c += 1
        return c

    def commit(self, ref, elems):
        """Commits `elems` data to `ref` Reference object in thread-safe mode."""
        N = len(elems)
        if N == 0:
            return False
        e = False
        try:
            for e in elems:
                ref.append(np.array(e), uptime=False)
        except:
            print('ReferenceUpdater.commit', ref.folder, e)
            raise
        # Interpolation step
        try:
            elems = ref.interpolate()
            self.write_summary_mem(ref.path, elems)
        except:
            print('ReferenceUpdater.commit/interpolate', ref.folder, elems)
            raise
        return N
    
    def write_summary_mem(self, path, elems):
        if elems is None:
            return False
        path = 'summary' + path
        for elem in elems:
            self.main_buffer.write_current(path, float(elem[1]), t=float(elem[0]))
        return len(elems)

    @lockme()
    def from_cache(self, key):
        return self.cache.get(key, False)

    @lockme()
    def set_cache(self, key, val):
        self.cache[key] = val
    
    def collect(self, path, fbuffer):
        """Collects new data points from `path` using file buffer `fbuffer`.
        Returns a list of elements to be added, or False if the output Reference was not created."""
#       self._lock.acquire()
        if path in self.exclude:
            return False
        ref = self.from_cache(path)
#       self._lock.release()
        if ref is False:
            lastt = self.zerotime
        else:
            lastt = ref.mtime
        elems = []
        elems0 = fbuffer.sequence_from_time(path, lastt)
        if not elems0:
            return []
        ref = self.get_cache(path, fbuffer)
        h = ref.opt.get('history', 0)
        h = 0 if h == 0 else 1. / h
        for i, e in enumerate(elems0):
            if e is False:
                continue
            if e[0] <= ref.mtime + h:  # OK, e[0] has real timestamps as ref.mtime
                continue
            out = None
            ntime = e[0]  # newer time
            # Scale zerotime
            e[0] -= self.zerotime
            try:
                out = ref.encode(e)
            except:
                print_exc()
            if out is None:
                print('Encoding error', path, e)
                continue
            elems.append(out)
            # Remember newer time
            ref.mtime = ntime
        # Be sure to update reference with new mtime
        self.set_cache(path, ref)
        return elems

    def scan(self, paths, buffer):
        """Scan the whole self.paths list of eligible paths and collects data to be committed"""
        # Use a dedicated FileBuffer
        collected = []
        for p in paths:
            # Skip non-log entries
            if self.only_logs and not p.endswith(log_marker):
                continue
            elems = self.collect(p, buffer)
            if elems is False:
                collected.append((p, False))
                continue
            if len(elems) == 0:
                continue
            collected.append((p, elems))
        return collected
    
    def scan_loop(self, paths, buffer, index=0):
        """Continuously scan `paths`"""
        # t = time()
        while self.running:  # and time()-t<30:
            try:
                r = self.scan(paths, buffer)
            except:
                msg = 'scan_loop', index, format_exc()
                print(msg)
                self.log.error(msg)
                continue
            self._lock.acquire(True)
            self.callback_result += r
            self._lock.release()
            
            sleep(0.2)
        print('ReferenceUpdater.scan_loop: exiting', index)
        
