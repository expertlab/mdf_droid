# -*- coding: utf-8 -*-
"""Shared data management"""
from .conf import ConfigurationDictionary
from .database import Database
from .refupdater import ReferenceUpdater
from .outfile import OutputFile, sign
