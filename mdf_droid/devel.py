#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import sys

mdf = '/opt/mdf'
mis = '/opt/mdf4'


class OverrideImporter(object):

    def __init__(self, original):
        self.original = original
    
    def find_module(self, fullname, path=None):
        if 'mdf' in fullname or 'mdf' in fullname:
            return None
        return self.original.find_module(fullname, path=path)
    
    def __getattr__(self, name):
        if name not in ('original', 'find_module'):
            return getattr(self.original, name)
        return object.__getattribute__(self, name)

    
def override_devel():
    paths = [mis + '/mdf_canon', mis + '/mdf_droid',
              mdf + '/mdf.imago', mdf + '/mdf.server', mdf + '/mdf_server', mdf + '/mdf_imago']
    for p in paths[::-1]:
            # sys.path.insert(0, p)
            sys.path.append(p)
    print(sys.path)
    print(sys.meta_path)
    # Override    
    sys.meta_path[1] = OverrideImporter(sys.meta_path[1]) 
    
    
if __name__ == '__main__':
    if 'devel' in sys.argv:
        override_devel()
    from mdf_droid import service, parameters
    print(service.__file__)
    print(parameters.determine_path(service.__file__))
    service.run()
