# -*- coding: utf-8 -*-
"""
General Sample class
"""
from copy import deepcopy
from mdf_droid import device
from mdf_canon.option import mkheader
conf = [
    {"handle":'structuredName', "name":"Structured name fields", "attr":['Reset'],
     "header": mkheader("group", "code"), "type":'Table'},
    {"handle": 'T', "name": "Temperature", "type": 'RoleIO', "unit": 'celsius',
        "options": ['/kiln/', 'default', 'Ts']},
    {"handle": 'weight', "name": "Weight", "type": 'RoleIO'},
    {"handle": 'initialDimension', "name": 'Initial sample dimension', "unit": 'micron',
        "min": 0, "type": 'Float', "attr": ['NoSlider']},
    {"handle": 'ii', "name": 'Sample index on device', "min": 0, "attr": ['Hidden'],
     "max": 20, "current": 1, "type": 'Integer'},
    # idx (Index in parent device list)
    {"handle": 'idx', "name": 'Sample index in instrument',
        "current":-1, "type": 'Integer', "attr": ['Hidden']},
    {"handle": 'frame', "name": 'Last frame', "history": 0,
        "attr": ['Runtime'], "type": 'Image'},
    # x,y,w,h
    {"handle": 'roi', "name": 'Region Of Interest', "current": [0, 0, 640, 480],
     'type':'Rect', 'attr':['History']},
    {"handle": 'profile', "name": 'Last profile', "history": 0,
     "current": [(), [], []], "attr": ['Runtime'], "type": 'Profile'},
    {"handle": 'filteredProfile', "name": 'Last profile',
     "current": [(), [], []], "attr": ['Runtime', 'History', 'Hidden'], "type": 'Profile'},
    {"handle": 'recFrame', "name": 'Frame recording freq', "unit":'hertz',
        "current": 0, "attr": ['Hardware'], "type": 'Float', "min": 0},
    {"handle": 'recProfile', "name": 'Profile recording freq', "unit":'hertz',
        "current": 0, "attr": ['Hardware'], "type": 'Float', "min": 0},
    {"handle": 'moving', "name": 'Moving',
                  "attr": ['Runtime', 'History', 'ReadOnly', 'Event', 'Reset'], "type": 'Boolean'},

]


class Sample(device.Device):

    """A sample is the subject of a test run. Every @Instrument defines one or more samples by setting @Measure.nSamples, 
and usually defines its own sample type, inherited from this base class.

Samples can contain sub-samples which contribute to the calculation of a part of the partent sample (@SampleDilatometer, @SampleVertical).

Specific samples extend this base definition by adding image analysis outputs and related configuration options.
"""
    analyzer = False
    conf_def = deepcopy(device.Device.conf_def + conf)
    suffixes = []
    """List of sub-sample partial object names"""
    part_def = deepcopy(conf_def)
    """Definition for sub-sample partial objects"""
    samples = []
    """List of sub-samples"""

    def __init__(self, parent=None, node='sample', conf_def=False, suffixes=False):
        """For each sample-part suffix, a sub-sample is created being of the same class as self and part_def default configuration."""
        device.Device.__init__(
            self, parent=parent, node=node, conf_def=conf_def)
        if suffixes is not False:
            self.suffixes = suffixes
        self.samples = []
        for suffix in self.suffixes:
            # Create a sub-sample using the same class as "self",
            # so we get the correct analyzer
            part = self.__class__(parent=self,  # same class as itself
                                  node=suffix,
                                  conf_def=self.part_def,  # partial definition
                                  suffixes=[])  # no suffixes
            part['name'] = 'Part ' + suffix
            self.samples.append(part)
        
        self['recFrame'] = self.desc.get('recFrame')
        self['recProfile'] = self.desc.get('recProfile')
        
    def autoname(self, name=False):
        name = name or 'Sample n.'
        p = self.parent()
        m = getattr(p, 'measure', False)
        n = 1 if not m else m['nSamples']
        if n > 1:
            return f'{name} {self["idx"]}'
        return name
    
    def iter_samples(self):
        for s in self.suffixes:
            yield self.child(s)
        
    def reset_acquisition(self):
        ret = device.Device.reset_acquisition(self)
        for smp in self.samples:
            smp.reset_acquisition()
        return ret

    def set_roi(self, roi):
        # TODO: Full validation...?
        if roi[0] < 0:
            roi[0] = 0
        if roi[1] < 0:
            roi[1] = 0
        # Mirror any roi change to rrt
        # self['rrt'] = roi
        roi = list(map(int, roi))
        return roi

    def get_History_attr(self, opt):
        """Return true if History attr is set for option `opt`."""
        a = self.desc.getattr(opt, 'attr')
        if 'History' in a:
            return True
        return False

    def set_History_attr(self, opt, val):
        """Set History attr on option `opt`."""
        a = self.desc.getattr(opt, 'attr')
        old = 'History' in a
        if old == val:
            return val
        if val:
            a.append('History')
        else:
            a.remove('History')
        self.desc.setattr(opt, 'attr', a)
        # Recursively set the option to all sub-samples
        for smp in self.samples:
            smp.desc.setattr(opt, 'attr', a)
        return val

    def get_recFrame(self):
        """The value of recFrame depends on the presence of History attribute on frame option."""
        return self.getattr('frame', 'history')

    def set_recFrame(self, val):
        """By setting recFrame, History attribute on frame option is accordingly added or removed."""
        self.set_History_attr('frame', bool(val))
        self.setattr('frame', 'history', val)
        return val

    def get_recProfile(self):
        """The value of recProfile depends on the presence of History attribute on profile option."""
        return self.getattr('profile', 'history')

    def set_recProfile(self, val):
        """By setting recProfile, History attribute on profile/filteredProfile options is accordingly added or removed."""
        self.set_History_attr('profile', bool(val))
        self.set_History_attr('filteredProfile', bool(val))
        self.setattr('profile', 'history', val)
        self.setattr('filteredProfile', 'history', val)
        return val
