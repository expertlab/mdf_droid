#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Test for Sample object"""
import unittest
from mdf_droid import instrument

print('Importing ' + __name__)


def setUpModule():
    print('Starting ' + __name__)


def tearDownModule():
    print('Ending ' + __name__)


class DummySample(instrument.Sample):
    suffixes = ['a', 'b', 'c']


class Sample(unittest.TestCase):

    def test_init(self):
        s = instrument.Sample()
        self.assertTrue('anerr' in s)
        print(s.gete('initialDimension'))

    def test_sampleparts(self):
        d = DummySample()
        self.assertEqual(len(d.devices), 3)
        self.assertEqual(d.list(),
                         [('Part a', 'a'), ('Part b', 'b'), ('Part c', 'c')])


if __name__ == "__main__":
    unittest.main()
