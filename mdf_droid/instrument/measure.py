# -*- coding: utf-8 -*-
"""
General Measure class
"""
from copy import deepcopy
from mdf_canon.csutil import utime, validate_filename
from mdf_canon.milang import script_registry
from mdf_canon.option import mkheader
from mdf_droid import device

prefix = '^/\w/measure/'

scrEnd = 'mi.Point(idx=-1)'
scrMaxT = """
i,t,T=mi.Max('T')
mi.t(t)
mi.T(T)
"""
script_registry[prefix + 'scrMaxT$'] = scrMaxT

scrMaxHeatingRate = """
T1=kiln.TimeDerivative('T')
if len(T1)<10: mi.Exit()
rate=max(T1)
w=mi.Where(T1==rate)
if w<0: mi.Exit()
mi.Point(idx=w+1)
mi.Value(rate*60)

"""
script_registry[prefix + 'scrMaxHeatingRate$'] = scrMaxHeatingRate

scrCoolingDuration = """
ret = mi.GetCoolingTimeAndIndex()
if not ret:
    mi.Exit()
cooling_time =ret[0]
t, T = mi.AtTime('T', cooling_time)
end_t, end_T = mi.AtIndex('T', -1)
mi.T(T - end_T)
mi.t(end_t - t)
"""
script_registry[prefix + 'scrCoolingDuration$'] = scrCoolingDuration

scrMaxCoolingRate = """
ret = mi.GetCoolingTimeAndIndex()
if not ret:
    mi.Exit()
cooling_time, cooling_index = ret
T1=mi.TimeDerivative('/kiln/T', cooling_time)
if len(T1)<10: mi.Exit()
rate=min(T1)
w = mi.Where(T1 == rate) - 1
if w<0: mi.Exit()
mi.Value(rate/60)
mi.Point(idx=w)
"""
script_registry[prefix + 'scrMaxCoolingRate$'] = scrMaxCoolingRate

onKilnStopped = """if kiln.Opt('analysis'): mi.Exit()
belowTemp=script.Opt('coolingBelowTemp') # degrees
afterMinutes=script.Opt('coolingAfterTime')
stop=False
if belowTemp == 0 and afterMinutes <= 0:
    stop = True
if kiln.Opt('T')<belowTemp and belowTemp!=0:
    stop=True
if measure.Opt('elapsed')-kiln.Opt('coolingStart')>afterMinutes*60 and afterMinutes>0:
    stop=True

if stop:
    msg ='Stop acquisition after thermal cycle ended and cooling'
    mi.Log(msg)
    ins.stop_acquisition(message=msg)"""
script_registry[prefix + 'onKilnStopped$'] = onKilnStopped

conf = [
    {"handle": 'operator', "name": "Operator", "attr":['ReadOnly'],
     "current": 'unknown', "type": 'String'},
    {"handle": 'flavour', "name": 'Type',
        "current": 'Standard', "type": 'Chooser',
        "options": ['Standard', 'Calibration']
     },
    {"handle": 'structuredName', "name": "Structured name fields", "attr": ['Reset'],
     "header": mkheader("group", "code"), "type": 'Table'},
    {"handle": 'date', "name": 'Started at (h:m:s, dd/mm/yyyy)',
        "current": '',
        "attr": ['ReadOnly'], "type": 'Date',
     },
    {"handle": 'id', "name": 'Test ID name',
        "current": '', "type": 'ReadOnly',
     },
    {"handle": 'uid', "name": 'Test UID',
        "current": '', "type": 'String', "readLevel": 2, "parent": 'id',
        "attr": ['ReadOnly'],
     },
    {"handle": 'measureFile', "name": 'Measure File Path',
        "current": '', "readLevel": 2, "parent": 'id',
        "attr": ['Hidden'], "type": 'ReadOnly',
     },
    {"handle": 'nSamples', "name": 'Number of samples',
        "current": 1, "writeLevel": 2,
        "type": 'Chooser', "options": [1, 2, 3, 4, 5, 6, 7, 8]
     },
    {"handle": 'kilnBeforeStart',
     "name": 'Kiln position before acquisition', "type": 'Chooser',
        "current": 1,
        "options": ['Closed', 'Opened'], "writeLevel":2,
        "values":[1, 0]},
    {"handle": 'kilnAfterEnd',
        "name": 'Kiln position after acquisition', "type": 'Chooser',
        "options": ['Closed', 'Opened', 'Unchanged'], "writeLevel":2,
        "current":-1,
        "values":[1, 0, -1]},
    {"handle": 'tcmix',
     "name": 'Sample thermocouple weight',
        "current": 1,
        "type": 'RoleIO',
        "options": ['/kiln/', 'default', 'tcmix'],
     },
    {"handle": 'elapsed', "name": 'Elapsed time', "attr": ['ReadOnly'],
        "current": 0, "type": 'Float', "unit": 'second',
     },
        
    # {"handle": 'etaTC', "name": 'Estimated time to finish thermal cycle',
    #   "current": 0, "type": 'Float',  "unit": 'second',
    #   },
    # {"handle": 'etaAQ',   "name": 'Estimated time to finish acquisition',
    #   "current": 0, "type": 'Float', "unit": 'second',
    #   },

    # end (End of test)
    {"handle": 'end', "name": 'End of the test',
        "type": 'Meta',
     },
    {"handle": 'scrEnd', "name": 'End of test',
        "current": scrEnd, "parent": 'end',
        "flags": {'period':-1}, "type": 'Script', "readLevel": 3, "writeLevel": 3,
     },

    {"handle": 'endStatus', "name": "End status", "attr": ['ReadOnly', 'Result'],
        "current": '', "writeLevel": 5, "type": 'TextArea'},

    #------

    # maxT (Maximum Temperature)
    {"handle": 'maxT', "name": 'Maximum Temperature', "type": 'Meta'},
    {"handle": 'scrMaxT', "name": 'Maximum Temperature',
     "current": scrMaxT, "parent": 'maxT',
        "flags": {'period':-1}, "type": 'Script', "readLevel": 3, "writeLevel": 3,
     },

    # maxHeatingRate (Maximum Heating Rate)
    {"handle": 'maxHeatingRate',
        "name": 'Maximum Heating Rate', "type": 'Meta', "unit":{'value':u'celsius/min'}},
    {"handle": 'scrMaxHeatingRate', "name": 'Maximum Heating Rate',
        "current": scrMaxHeatingRate, "parent": 'maxHeatingRate', "readLevel": 3, "writeLevel": 3,
        "flags": {'period':-1}, "type": 'Script',
     },

    # coolingDuration (Total cooling duration)
    {"handle": 'coolingDuration',
        "name": 'Total cooling duration', "type": 'Meta'},
    {"handle": 'scrCoolingDuration', "name": 'Total cooling duration',
     "current": scrCoolingDuration, "parent": 'coolingDuration', "readLevel": 3, "writeLevel": 3,
        "flags": {'period':-1}, "type": 'Script',
     },

    # maxCoolingRate (Maximum Cooling Rate)
    {"handle": 'maxCoolingRate',
        "name": 'Maximum Cooling Rate', "type": 'Meta', "unit":{'value':u'celsius/min'}},
    {"handle": 'scrMaxCoolingRate', "name": 'Maximum Cooling Rate',
     "current": scrMaxCoolingRate, "parent": 'maxCoolingRate', "readLevel": 3, "writeLevel": 3,
        "flags": {'period':-1}, "type": 'Script',
     },

    # TERMINATION Conditions
    # errors (Stop on consecutive image analysis errors)
    #
    {"handle": 'errors', "name": 'Stop on consecutive image analysis errors',
        "max": 500, "current": 100, "step": 1, "min": 1,
        "flags": {'enabled': True}, "type": 'Integer', "readLevel": 4,
     },
    # duration (Maximum test duration (min))
    #
    {"handle": 'duration', "name": 'Maximum test duration', "max": 60 * 24 * 30,
        "current":-1, "type": 'Float', 'unit': 'minute', 'priority': 0
     },
    # cooling (Stop after cooling)
    #
    {"handle": 'onKilnStopped', "name": 'Stop after thermal cycle', 'priority': 0,
     "current": onKilnStopped,
        "flags": {'enabled': True, 'period': 0}, "type": 'Script',
     },
    # coolingBelowTemp (Wait T smaller than)
    {"handle": 'coolingBelowTemp', "name": 'Wait T smaller than',
        "parent": 'onKilnStopped', "unit": "celsius",
        "current": 0, "type": 'Float',
     },
    # coolingAfterTime (Wait minutes)
    {"handle": 'coolingAfterTime', "name": 'Wait minutes',
        "parent": 'onKilnStopped', "unit": "minute",
        "current": 0, "type": 'Float',
     },

    {"handle": 'thermalCycle',
     "name": 'Thermal cycle',
        "current": 'None',
        "type": 'RoleIO',
        "options": ['/kiln/', 'default', 'thermalCycle'],
     },
    {"handle": 'curve', "name": 'Heating curve',
        "options": ['/kiln/', 'default', 'curve'], "type": 'RoleIO',
        'attr':['Hidden']},
    {'name': 'measure'}
]


class Measure(device.Device):

    """Contains options applicable to a single test run, like the test name in @.name,
from which the output file name is also calculated.

It also defines generic :ref:`option_type_Meta` and :ref:option_type_Script` for summarizing overall 
metadata about the test run, like the test duration, the maximum temperature reached, etc. 
"""
    conf_def = deepcopy(device.Device.conf_def + conf)
    
    def __init__(self, *a, **kw):
        r = super(Measure, self).__init__(*a, **kw)
        self.setattr('curve', 'options', ['/kiln/', 'default', 'curve'])
        self.desc.set('thermalCycle', '')
        self.desc.set('name', 'measure')
        return r
    
    def autoname(self, name=False):
        return name or 'measure'
    
    def set_nSamples(self, n):
        self.parent()['nSamples'] = n
        n = self.parent()['nSamples']
        return  n
    
    def set_name(self, tn):
        """Option `name` must be a valid file name."""
        old = self.desc.get('name')
        tn = validate_filename(tn)
        smps = list(self.parent().iter_samples())
        for smp in smps:
            if not smp:
                self.log.error('Found invalid sample', smp, smps)
                continue
            if smp['name'] in (smp.autoname(), smp.autoname(old)):
                sn = smp.autoname(tn)
                print('Autoname', smp['idx'], tn, sn)
                smp['name'] = sn
        return tn
    
    def set_structuredName(self, table):
        # Set sample structuredName properties equal to measure properties if undefined
        for smp in list(self.parent().iter_samples()):
            smp['name'] = self['name']
            smp['structuredName'] = table
        return table

    def get_elapsed(self):
        """Returns elapsed time since the beginning of a test"""
        instrobj = self.parent()
        zt = instrobj['zerotime']
        # Zero, if zerotime is not initialized
        if zt <= 0.:
            return 0.
        r = float(utime() - zt)
        return r

    def get_coolingAfterTime(self):
        return self.get_if_on_kiln_stopped_enabled('coolingAfterTime')

    def get_coolingBelowTemp(self):
        return self.get_if_on_kiln_stopped_enabled('coolingBelowTemp')

    def get_if_on_kiln_stopped_enabled(self, option_name):
        if not self.gete('onKilnStopped')['flags']['enabled']:
            return 0.
        return self.desc.get(option_name)

    def set_preset(self, preset, *a, **k):
        """Force loading of the thermal cycle"""
        ret = device.Device.set_preset(self, preset, *a, **k)
        # Fix configuration definition error from past versions
        self['uid'] = ''
        if preset == 'factory_default':
            return ret
        cycle = self.get_from_preset('thermalCycle', preset)
        if cycle:
            self['thermalCycle'] = cycle
        opt = self.getattr('curve', 'options')
        curve = []
        self.log.debug('Fix curve RoleIO?', opt)
        if opt[-1] != 'curve':
            self.setattr('curve', 'options', ['/kiln/', 'default', 'curve'])
            # self.set_to_preset('curve','options',['/kiln/','default','curve'])
            self.map_role_dev('curve')
        else:
            curve = self.get_from_preset('curve', preset)
        self['curve'] = curve
        return ret
