#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
# !/usr/bin/twistd -noy
"""Mdf super server to start and manage other modules."""
# TODO: implement errors
import os
import sys
from time import sleep, time
import multiprocessing
import pkg_resources
from traceback import print_exc
import argparse
from twisted.internet import defer, ssl
from twisted.web import xmlrpc, server, static
from twisted.protocols.ftp import FTPFactory, BaseFTPRealm
from twisted.cred import credentials, checkers, error as credError
from twisted.cred.portal import Portal, IRealm
from twisted.web.guard import HTTPAuthSessionWrapper, DigestCredentialFactory, BasicCredentialFactory
from twisted.web.resource import IResource
from zope.interface import implementer
from subprocess import Popen
from mdf_canon.csutil import go
from mdf_droid import parameters as params
from mdf_droid import share
from mdf_droid.tests import import_all_tests
from mdf_droid.server import MainServer, MdfDirectory, LambdaRPC
from mdf_canon import logger

# Extract certificates from pkg_resources
# TODO: alternatively import from config dir
ssl_private_key = pkg_resources.resource_filename(
    'mdf_droid.server', 'privkey.pem')
ssl_cacert = pkg_resources.resource_filename(
    'mdf_droid.server', 'cacert.pem')
version_file = pkg_resources.resource_filename(
    'mdf_droid', 'VERSION')
params.ssl_enabled = params.ssl_enabled and os.path.exists(
    ssl_private_key) and os.path.exists(ssl_cacert)
params.ssl_private_key = ssl_private_key
params.ssl_cacert = ssl_cacert
params.version_file = version_file
realm_name = b"MDF"


@implementer(checkers.ICredentialsChecker)
class UsersChecker(object):

    """Dummy password checker relying on the MainServer.users object."""
    # FIXME: Re-organize this part!
    credentialInterfaces = (
        credentials.IUsernamePassword, credentials.IUsernameHashedPassword)

    def __init__(self, users):
        self.users = users

    def requestAvatarId(self, credentials):
        ok, msg = self.users.auth(credentials)

        if ok:
            return defer.succeed(msg)

        return defer.fail(
            credError.UnauthorizedLogin(msg))


@implementer(IRealm)
class MdfRealm(object):

    """Dummy realm returning the Mdf main resource."""
    resource = None

    def requestAvatar(self, avatarId, mind, *interfaces):
        if IResource in interfaces:
            return (IResource, self.resource, lambda: None)
        raise NotImplementedError()


class MdfFTPRealm(BaseFTPRealm):

    def getHomeDirectory(self, *a, **k):
        return self.anonymousRoot


def set_headers(request):
    request.setHeader('server', 'MdfServer/5')
    try:
        request.setHeader('Access-Control-Allow-Origin', request.getHeader('origin'))
    except:
        pass
    request.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
    request.setHeader("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization, X-Requested-With")
    request.setHeader("Access-Control-Allow-Credentials", "true")
    

class OPTIONS(object):

    @staticmethod
    def render(request):
        print('OPTIONS.render', request)
        request.setResponseCode(200)
        request.setHeader("X-Test", "ciao")
        return b"ciao"
    
    @staticmethod
    def isLeaf(*a, **k):
        return False


class SiteCORS(server.Site):
    
    def getResourceFor(self, request):
        set_headers(request)
        r = super(SiteCORS, self).getResourceFor(request)
        # print('SiteCORS.getResourceFor',request.method, request, r)
        if request.method == b"OPTIONS":
            return OPTIONS
        return r
    
    def getChildWithDefault(self, path, request):
        # print('SiteCORS.getChildWithDefault',path, request)
        set_headers(request)
        return super(SiteCORS, self).getChildWithDefault(path, request)
    
    def render(self, request):
        set_headers(request)
        r = super(SiteCORS, self).render(request)
        # print('SiteCORS.render',request)
        return r

    
class WrapperCORS(HTTPAuthSessionWrapper):

    def getResourceFor(self, request):
        # print('WrapperCORS.getResourceFor',request)
        set_headers(request)
        return super(WrapperCORS, self).getResourceFor(request)
    
    def getChildWithDefault(self, path, request):
        # print('WrapperCORS.getChildWithDefault',request.method, path, request)
        set_headers(request)
        if request.method == b"OPTIONS":
            return OPTIONS
        return super(WrapperCORS, self).getChildWithDefault(path, request)
    
    def render(self, request):
        set_headers(request)
        r = super(WrapperCORS, self).render(request)
        # print('WrapperCORS.render',request,r)
        return r

        
# from mdf_canon import csutil
# @csutil.profile
def setMain(opt):
    """Create a MainServer class based on `opt` options"""
    instanceName = opt['name']
    if not instanceName:
        instanceName = ''
    print('setMain', opt)
    # Determine logfile path into datadir
    log_filename = params.log_filename if 'data' not in opt else os.path.join(opt['data'], 'log', 'mdf.log')
    params.log_filename = log_filename
    # Start the object sharing process
    share.init(connect=False, log_filename=log_filename)
    # Instantiate mainserver class
    print('instantiate')
    main = MainServer(instanceName=instanceName,
                      port=opt['port'],
                      confdir=opt['conf'],
                      datadir=opt['data'],
                      plug=opt['enable'],
                      manager=share.manager)
    print('done instantiating')
    xmlrpc.addIntrospection(main)
    mimetypes = {'.h5': 'application/x-hdf;subtype=bag'}
    static.File.contentTypes.update(mimetypes)
    web = static.File(params.webdir)
    web.putChild(b'RPC', main)
    web.putChild(b'data', static.File(params.datadir))
    web.putChild(b'conf', static.File(params.confdir))
    # StreamServer
    mdir = MdfDirectory(main)
    web.putChild(b'stream', mdir)
    
    rpc = LambdaRPC(main)
    web.putChild(b'lambda', rpc)
    
    # Further reading about auth stuff:
    # http://www.tsheffler.com/blog/?p=502
    realm = MdfRealm()
    realm.resource = web
    # TODO: use also for FTP!
    checker = UsersChecker(main.users)
    portal = Portal(realm, [checker])
    cred_methods = (DigestCredentialFactory("SHA", realm_name),
                    BasicCredentialFactory(realm_name))
    wrapper = WrapperCORS(portal, cred_methods)
    site = SiteCORS(wrapper)
    return main, web, site


def addListeners(reactor, site, main, port=0, logf=False):
    """Add services"""
    # Simple logging
    if not logf:
        logf = logger.log.info
    if params.ssl_enabled:
        sslContext = ssl.DefaultOpenSSLContextFactory(
            ssl_private_key,
            ssl_cacert,)
        reactor.listenSSL(port, site, contextFactory=sslContext)
        logf('Mdf Server Listening on SSL port:', port)
        logf('Using CA Certificate on:', ssl_cacert)
        logf('using private key on:', ssl_private_key)
    else:
        print('SSL Disabled by parameters:', params.ssl_enabled)
        reactor.listenTCP(port, site)
        logf('Mdf Server Listening on TCP port:', port)
        logf('SSL DISABLED')
    reactor.addSystemEventTrigger("before", "shutdown", main.shutdown)
    # Add FTP server capability
    checker = UsersChecker(main.users)
    ftp = Portal(MdfFTPRealm(params.datadir), [checker])
    f = FTPFactory(ftp)
    ftp_port = 3821 + port - 3880
    reactor.listenTCP(ftp_port, f)
    logf('Mdf FTP Server Listening on TCP port:', ftp_port)


def startInstance(instanceName='', port=params.main_port):
    main, web, site = setMain(instanceName, port)
    addListeners(reactor, site, main, port=port, logf=main.log.info)
    main.log.critical(
        'MdfServer Ready To Serve, instance: ' + instanceName)
    main.connection()
    p = multiprocessing.Process(target=reactor.run)
    p.daemon = True
    p.start()
    return p


base_plugins = """mdf_droid.users.Users
mdf_droid.storage.Storage
mdf_droid.support.Support
#GROUP
"""


def getOpts():
    # TODO: complete migration to ArgumentParser
    ap = argparse.ArgumentParser(
                    prog='MdfServer',
                    description='MDF Server',
                    epilog='Example: server.bin -s')
    ap.add_argument('-n', '--name', default="",
                    required=False, help="Server instance name")
    ap.add_argument('-p', '--port', default=3880, type=int,
                    required=False, help="Base service port")
    ap.add_argument('-c', '--conf', default=params.confdir,
                    required=False, help="Configuration directory")
    ap.add_argument('-d', '--data', default=params.datadir,
                    required=False, help="Data directory")
    ap.add_argument('-m', '--mem', default=params.rundir,
                    required=False, help="Memory directory")
    ap.add_argument('-e', '--enable', default='',
                    required=False, help="Enable plugins")
    ap.add_argument('-s', '--start', action='store_true',
                    help="Kill any instance before starting a new one")
    ap.add_argument('-k', '--kill', action='store_true',
                    help="Kill any instance and exit")
    ap.add_argument('-r', '--init', default='',
                    required=False, help="Reinit instrument name")
    ap.add_argument('-l', '--preset', default='',
                    required=False, help="Load preset name (requires --init)")
    ap.add_argument('-w', '--wait', default=0, required=False,
                    help="Wait for resources to be freed after restart")
    ap.add_argument('-f', '--firm', action='store_true',
                    help="Reset to original firmware directory")
    ap.add_argument('-i', '--cmd', action='store_true',
                    help='Interactive console session (debug)')
    ap.add_argument('-t', '--test', action='store_true',
                    help='Import test. Import all modules and load cv2')
    ap.add_argument('-u', '--unit', action='store_true',
                    help='Run all unittests')
    
    args = vars(ap.parse_args())
    # Path cleanup
    for opt in ['conf', 'data', 'mem']:
        val = args[opt].strip('"')
        if not val.endswith(params.sep):
            val += params.sep
        if not os.path.exists(val):
            print("Non-existent path configured for %s: %s" % (opt, val))
    # Add plugins
    if args['enable']:
        val = args['enable'].strip('"').replace(';', '\n')
        val = base_plugins + val
    print('Parsed args', args)
    return args


def stop():
    """Kill other instances of MdfServer"""
    pids = go("pgrep -f MdfServer")[1].split('\n')
    pids += go("pgrep -f server.bin")[1].split('\n')
    pids += go("pgrep -f server.py")[1].split('\n')
    pid = str(os.getpid()), str(os.getppid())
    
    print('My pid', pid, pids)
    for p in pids:
        try:
            int(p)
        except:
            print('Skip pid', p)
            continue
        if p in pid:
            continue
        print('Killing pid:', p)
        print(go('kill -9 {}'.format(p)))
    print('Stopping tests:', go('pkill -9 -f test_'))
    share.stop()

    # share.close_sparse_objects()


def kill_myself():
    """Kill this instance to free up resources"""
    pid = str(os.getpid()), str(os.getppid())
    pid = (str(os.getpid()),)
    print('Killing myself:', pid)
    print(go('kill -9 {}'.format(*pid)))

    
def run_console(extend_locals={}):
    # Prepare environment
    import numpy as np
    import mdf_canon
    import mdf_droid
    import mdf_imago
    import mdf_server
    import code
    glo = globals()
    glo.update(locals())
    glo.update(extend_locals)
    msg = '################\n' * 3
    msg += """
    MDF Server Interactive Environment
    Important variables:
        main ->  Main server instance
        web -> Web server
        site ->  Web site with auth
    To start, run:
        reactor.run(installSignalHandlers=1)
    """
    code.interact(msg, local=glo)


def selftest(run=False):
    sys.argv = sys.argv[:1]
    import_all_tests(run=run)
    import cv2
    import gi
    gi.require_version('Gst', '1.0')
    gi.require_version('GstApp', '1.0')
    from gi.repository import Gst
    from gi.repository import GstApp  # NEEDED!
    print('CV2 version', cv2.__version__)
    return 0

    
def run(noname=False, mdfServerExe=params.mdfServerExe):
    global main, web, site
    # command-line start
    r = getOpts()
    if r['kill']:
        stop()
        print('All instances stopped. Exiting')
        return
    if r['start']:
        print('Stopping and restarting...')
        stop()
    print('Instance Name: %s; Port: %i\nConf: %s; Data: %s Mem: %s Ext: %s' % (r['name'],
                                r['port'], r['conf'], r['data'], r['mem'], r['enable']))
    if r['wait']:
        print('SLEEPING AFTER RESTART', r['wait'])
        sleep(int(r['wait']))
     
    if r['test'] or r['unit']:
        run = r['unit']
        try:
            r = selftest(run=run)
        except:
            print_exc()
            r = 2
        sys.exit(r)
        
    params.set_confdir(r['conf'])
    params.set_datadir(r['data'])
    params.set_rundir(r['mem'])
    print('Starting from ', mdfServerExe)
    firmware = False
    # Start the newer firmware instead:
    if os.path.basename(params.mdfServerExe) == 'server.bin':
        firmware = os.path.join(params.confdir, 'firmware', 'server.bin')
    print('Checking for newer firmware in ', firmware)
    original_firmware = os.path.abspath(mdfServerExe)
    if firmware and os.path.exists(firmware) and original_firmware != os.path.abspath(firmware):
        print("A newer firmware was found. Starting from:", firmware, mdfServerExe)
        go('chmod ugo+x "{}"'.format(firmware))
        # TODO: check if it immediately dies
        proc = Popen([firmware] + sys.argv[1:] + ['-f', original_firmware], close_fds=True)
        print('RESTARTED WITH PID', proc.pid)
        t0 = time()
        proc.wait()
        if time() - t0 > 120:
            print('SUBPROCESS ENDED, exiting')
            sys.exit(0)
        print('SUBPROCESS ENDED PREMATURELY, continuing with version', original_firmware)
    else:
        firmware = mdfServerExe
        print('Running from firmware in', firmware)
    share.set_dbpath()
    main, web, site = setMain(r)
    if r['firm']:
        print('Setting original_firmware to', r['firm'])
        main.support.original_firmware = r['firm']
    # Re-initialize last instrument
    init_instrument = r['init']
    if init_instrument:
        instrument = getattr(main, init_instrument, False)
        if not instrument:
            raise BaseException(
                'Asked to reinit an unexisting instrumnet! ' + init_instrument)
        print('Initialize instrument:', instrument)
        kw = {}
        if r['preset']:
            kw['preset'] = r['preset']
        instrument.init_instrument(**kw)
    from twisted.internet import reactor
    addListeners(
        reactor, site, main, port=r['port'], logf=main.log.info)
    if r['cmd']:
        run_console(locals())
        return
    main.connection()
    reactor.run(installSignalHandlers=1)
    # Stop remaining spares processes
    try:
        stop()
    except:
        print_exc()
    # Built server.bin must exit at every restart
    # DEVEL RESTART ONLY
    # TODO: shell command to apply a time delta!
    pyc = firmware.endswith('.pyc')
    if main.restart and pyc:
        print("WAITING FOR RESOURCES TO BE FREED")
        args = sys.argv[1:]
        if main.reinit_instrument:
            args += ['-r', main.reinit_instrument]
        if '-w' not in args:
            args += ['-w', '3']
        print('RESTARTING', firmware, args)
        # Make executable again, if upgraded in the meantime
        go('chmod ugo+x "{}"'.format(firmware))
        if pyc:
            cmd = ["python", firmware]
        else:
            cmd = [firmware]
        # cmd = ' '.join(cmd+args) 
        # print 'GO', cmd
        # os.system('nohup '+cmd+' &')
        # NOTICE: only works for 1st restart!
        pid = Popen(cmd + args, close_fds=True).pid
        print('RESTARTED WITH PID', pid)
        sleep(1)
        sys.exit(0)
        # kill_myself()


if __name__ == '__main__':
    run()
