import unittest
from mdf_droid import parameters, device
import shutil
from mdf_canon.mem import reset_redis
import sys
import gc
import os
from mdf_canon.csutil import iter_package


def reset_test_memory(path='droid', db=None):
    print('Garbage collect', gc.collect())
    db = 1 + int(sys.version.split('.')[1])
    shutil.rmtree('/tmp/mdf_test_' + path, ignore_errors=True)
    shutil.rmtree('/dev/shm/mdf_test_' + path, ignore_errors=True)
    reset_redis('/dev/shm/mdf_test_' + path + '/', db)
    parameters.regenerateDirs('/tmp/mdf_test_' + path + '/', '_test_' + path)
    device.delete_registry()


class HardwareConnectionException(BaseException):
    pass


def connection_error(msg):
    if os.environ.get('MDF_TEST_HARDWARE', False):
        raise HardwareConnectionException(msg)
    else:
        raise unittest.SkipTest(msg)


def connect_first_device(cls, *a, **k):
    avail = cls.list_available_devices()
    if not avail:
        connection_error("No connected devices of class " + cls.__name__)
    for node in list(avail.keys()):
        dev = cls(*a, node=node, **k)
        if dev.connection():
            return dev
    connection_error("Impossible to connect to any device of class " + cls.__name__)


def recurse_all_tests(packages, run=False):
    subs = []  # Plain list: avoid max recursion error
    tests = []
    for package in packages:
        for module, ispkg in iter_package(package):
            if ispkg:
                subs.append(module)
            elif '.tests.test_' in module.__name__:
                tests.append(module)
                if not run:
                    continue
                print("RUNNING", module)
                program = unittest.main(module, verbosity=2, exit=False)
                r = program.result
                if r.errors + r.failures:
                    print('TEST FAILURE', r)
                    sys.exit(1)
    return subs, tests


def import_all_tests(run=False):
    import mdf_canon
    import mdf_droid
    import mdf_imago
    import mdf_server
    subs = [mdf_canon, mdf_droid, mdf_imago, mdf_server]
    alltests = []
    while len(subs):
        subs, tests = recurse_all_tests(subs, run=run)
        print('Subpackages:', subs)
        alltests += tests
    return alltests
