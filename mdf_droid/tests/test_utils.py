#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import unittest
from mdf_droid import utils
from mdf_droid.data.tests import testdir
print('Importing ' + __name__)


def setUpModule():
    print('Starting ' + __name__)


udevadm_video0 = os.path.join(testdir, 'udevadm_video0.log')


class DroidUtils(unittest.TestCase):
    
    def query_udev(self, fpath):
        with open(fpath, 'r') as f:
            dat = f.read()
        return utils._query_udev_tree(dat)
    
    def test_query_udev_tree(self):
        tree = self.query_udev(udevadm_video0)
        print(tree)
        serial, tree = utils._query_known_serial(tree)
        self.assertEqual(serial, "437F2C60")
