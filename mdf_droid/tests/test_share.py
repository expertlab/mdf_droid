#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import unittest
import pickle
import multiprocessing
from mdf_canon import csutil, mem
from mdf_canon.csutil import exceptions
from mdf_droid import share
from mdf_droid import data
from mdf_droid import utils
from mdf_droid import utils_testing as ut

print('Importing ' + __name__)


def setUpModule():
    print('Starting ' + __name__)


class ShareModule(unittest.TestCase):

    def test_register(self):
        self.assertTrue('SharedFile' in share.registered)
        self.assertTrue('Database' in share.registered)


def parallel_dumps(obj, spr=False):
    """Dummy func which returns a pickled object"""
    print('PICKLING', obj, spr)
    if spr: spr()
    
    r = pickle.dumps(obj)
    print('PICKLED')
    return r


def parallel_func(obj):
    """Calling parallel_dumps in a separate process"""
    p = multiprocessing.Process(target=parallel_dumps, args=(obj, csutil.sharedProcessResources))
    p.daemon = True
    print('parallel_func start')
    p.start()
    p.join()
    print('parallel_func end')


def pool_func(mgr, *objs):
    """Calling parallel_dumps in a pool of worker processes"""
    print('Starting pool of workers')
    p = multiprocessing.Pool(2)
    print('Calling parallel_dumps')
    res = []
    if len(objs) == 0:
        objs = [mgr.OutputFile() for i in range(5)]
    for i, obj in enumerate(objs):
        r = p.apply_async(parallel_dumps, (obj,))
        res.append(r)
    r = [r.get(1) for r in res]
    print('results:', r)
    print('closing')
    p.close()
    print('joining')
    p.join()
    print('done pool_func')


class Pickling(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.mgr = share.dummy()

    @classmethod
    def tearDownClass(cls):
        cls.mgr.shutdown()

    def stop(self):
        print('stopping')
        self.mgr.shutdown()
        print('stopped')

    def start(self):
        self.mgr = share.SharingManager()
        self.mgr.start()

    def test_noshare(self):
        self.start()
        from mdf_droid import device
        obj = self.mgr.OutputFile()
        pickle.dumps(obj)
        obj = device.Device()
        pickle.dumps(obj.desc)
        self.stop()

    def test_noshare(self):
        obj = self.mgr.OutputFile()
        self.assertRaises(RuntimeError, lambda: pickle.dumps(obj))
    
    def test_parallel_noshare(self):
        obj = self.mgr.OutputFile()
        parallel_func(obj)
    
    def test_parallel_share(self):
        self.start()
        self.test_parallel_noshare()
        self.stop()

    def test_pool_noshare(self):
        self.assertRaises(RuntimeError, lambda: pool_func(self.mgr))

    def test_pool_share(self):
        print('test_pool_share')
        self.start()
        pool_func(self.mgr)
        self.stop()


@unittest.skip('FIXME')
class InitStop(unittest.TestCase):

    def test_initstop(self):
        share.init()
        share.stop()
        share.init()
        share.stop()


class ProcessCache(unittest.TestCase):

    def test(self):
        p = share.ProcessCache()
        p.start()
        m = p.get()
        obj = m.SharedFile()
        self.assertFalse(obj.get_uid())
        p.destroy(m)
        m = p.get()
        obj = m.SharedFile()
        pid = obj._get_pid()
        self.assertGreater(pid, 0)
        self.assertNotEqual(pid, os.getpid())
        self.assertTrue(utils.check_pid(pid))
        self.assertEqual(os.kill(pid, 9), None)
        utils.join_pid(pid, 10)
        self.assertFalse(utils.check_pid(pid))


if __name__ == "__main__":
    unittest.main(verbosity=2)
