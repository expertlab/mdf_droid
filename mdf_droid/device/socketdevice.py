#!/usr/bin/python
# -*- coding: utf-8 -*-
"""A device/peripheral available through the LAN."""
import socket
import os, threading
from copy import deepcopy
from traceback import format_exc

from .physicaldevice import Physical
from .enumerated import Enumerated
from contextlib import contextmanager

conf = [
    {"handle": 'addr', 	"name": 'Remote address',
     "current": '192.168.0.34', 	"type": 'String', "readLevel": 3	},
    {"handle": 'port', 	"name": 'Port', 	"current": 502,
        "type": 'Integer', 	"readLevel": 3},
    {"handle": 'nodelay', 	"name": 'TCP_NODELAY',
        "current": False, "type": 'Boolean', "readLevel": 3},
    {'timeout': 1000}
]


class Socket(Enumerated, Physical):

    """Generic interface for socket devices"""
    conf_def = deepcopy(Physical.conf_def)
    conf_def += conf
    available = {}
    _udev = {}
    enumerated_option = 'socket'
    protocol = socket.IPPROTO_TCP

    def __init__(self, parent=None, node='127.0.0.1:8000'):
        Physical.__init__(self, parent=parent, node=node)
        node = self['dev']
        self.timeout = self.get('timeout') / 1000.
        self['isConnected'] = False
        if ':' in node:
            self['addr'], port = node.split(':')
            self['port'] = int(port)
        else:
            self['addr'] = node

    @contextmanager
    def socket(self):
        """Re-create the socket connection"""
        t = self['timeout'] / 1000.
        addr = (self['addr'], self['port'])
        s = socket.create_connection(addr, timeout=t)
        s.setsockopt(self.protocol,
                       socket.TCP_NODELAY, int(self.desc.get('nodelay')))
        try:
            yield s
        finally:
            s.close()
            
    def connection(self, blacklist=[]):
        """Connect to a socket"""
        v = False
        try:
            v = self.validate_connection()
        except:
            self.log.error("Socket connect error ", format_exc())
        self['isConnected'] = v
        return v

    def validate_connection(self):
        """Funzione di validazione della connessione. Da reimplementare ad hoc."""
        self.log.debug('validation...')
        with self.socket() as s:
            r = s.send(b'1')
            return r == 1

    # debug
    def raw(self, idx, msg):
        if not getattr(self, 'read', False):
            self.log.debug('UnImplemented read function')
            return 'UnImplemented'
        with self.socket() as s:
            s.send(msg)
        return self.read()

    xmlrpc_raw = raw
    
