#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Generic Device object"""
import multiprocessing
import os
import subprocess
import tempfile
from collections import defaultdict
from copy import deepcopy
from functools import cmp_to_key
from traceback import format_exc

import setproctitle
from mdf_canon import milang, option, linux
from mdf_canon.csutil import unlockme, initializeme, sharedProcessResources, time
from twisted.internet import reactor
from twisted.internet.task import deferLater

from mdf_droid.device.control import add_kalman_definitions
# Possible relative import?
from mdf_droid.device.registry import get_registry
from . import control
from . import device_conf
from .inputoutput import InputOutput
from .node import Node
from .. import utils

STOPPED, RUNNING, STOPPING = 0, 1, 2


class AlreadyLocked(BaseException):
    pass


def conf_dict(conf_list):
    """Transform a conf_def configuration list into a configuration dictionary for fast indexing"""
    return {e.get('handle', e.keys().__iter__().__next__()): e for e in conf_list}


class Device(option.Aggregative, milang.Scriptable, Node):
    """The Device is the root object of any other MDF component. 
A Device can be any object exposing configurable and/or readable options and 
hierarchically nested in the main object tree.

The options defined here are available to every other class contributing to an 
MDF server existence.

The core functionalities of a device are:

- Managing a collection of :ref:`Options <option_model>`.
- Acting as a node in a hierarchy of Devices.
- Being able to manage a parallel process to perform I/O or computations.
"""
    allow_none = True
    idx = -1
    node = 'undefined'
    """Index on parent device list"""
    fixedNaturalName = False
    """Define if this device has a fixed, pre-defined natural name and should not be named after idx0,1,2"""
    naturalName = 'dev'
    """Automatic naming"""
    available = {}
    """Available device registry {devpath:dev file}"""
    _udev = {}
    """query_udev cache"""
    conf_def = deepcopy(Node.conf_def + device_conf.conf)
    """Configuration definition"""
    process = False
    """Parallel acquisition process"""
    _daemon_acquisition_process = False
    """Acquisition child processes should be daemonic? (testing)"""
    Node.setProperties('zerotime', 'initializing', 'locked')
    mp = multiprocessing
    whitelist = set([])
    """Exceptions to the DeviceServer-level blacklist"""
    blacklist_options = ['dev', 'devpath']

    def copy(self):
        # FIXME: Defined here just to allow hu_testing!
        return self

    def time(self):
        """Returns the server's time"""
        return time()

    xmlrpc_time = time

    @classmethod
    def served_by(cls, DeviceServerClass, original=False, current=True):
        """Performs registration operations when this class is served by a DeviceServer"""
        if original is not False:
            cls = original
        if cls in DeviceServerClass.ServedClasses:
            print('Attept to register an already registered class', cls)
            return False
        # Add class enabling option to the conf
        DeviceServerClass.conf_def.append({"handle": 'scan_' + cls.__name__,
                                           "name": 'Scan for ' + cls.__name__, "current": current,
                                           "type": 'Boolean', "writeLevel": 5, "readLevel": 4,
                                           # Causes a set_func call when a
                                           # preset is loaded
                                           'attr': ['Hardware']
                                           })
        DeviceServerClass.ServedClasses.append(cls)
        return True

    @property
    def conf_dict(self):
        return conf_dict(self.conf_def)

    def __init__(self, parent=None, node='', conf_def=False):
        """Role-aware"""
        # Assure that each instance gets its own, unique, configuration
        # definition
        self.roledev = {}
        """Role->Dev mapping dictionary"""
        if conf_def is not False:
            self.conf_def = conf_def
        self.conf_def = deepcopy(self.conf_def)
        confdev = self.conf_dict
        for e in self.conf_def:
            a = e.get('attr', [])
            if ('FilterGet' in a) or ('FilterSet' in a):
                add_kalman_definitions(e['handle'], confdev, self.conf_def)
        milang.Scriptable.__init__(self)
        Node.__init__(self, parent=parent, node=node)
        if self['name'] == 'device':
            self['name'] = self['devpath']
        self._lock = self.mp.Lock()
        self.desc.setKeep_names(['locked'])
        sharedProcessResources.register(self.restore_lock, self._lock)
        self.build_controls()
        self.node = node

    def defer(self, func, t=1):
        return deferLater(reactor, t, func)

    def end_status(self, msg, *args):
        """Send an end_status to the currently running instrument"""
        if not self.root_obj:
            return False
        return self.root_obj.end_status(msg, *args)

    def build_controls(self):
        for handle, opt in self.describe().items():
            a = opt.get('attr', [])
            if ('FilterGet' in a) or ('FilterSet' in a):
                control.Control(self, handle, 'ReadOnly' in a)

    def restore_lock(self, lock):
        self._lock = lock

    def __getstate__(self):
        state = self.__dict__.copy()
        state.pop('_lock')
        return state

    def __setstate__(self, state):
        list(map(lambda a: setattr(self, *a), state.items()))
        self._lock = self.mp.Lock()

    def xmlrpc___nonzero__(self):
        return True

    def xmlrpc___bool__(self):
        return True

    def xmlrpc_collect_aggregate(self, *a, **k):
        return self.collect_aggregate(*a, **k)

    def xmlrpc_update_aggregates(self, *a, **k):
        return self.update_aggregates(*a, **k)

    def dashboard_check(self, handle):
        return True

    xmlrpc_dashboard_check = dashboard_check

    @classmethod
    def list_available_devices(cls):
        return cls.available

    def pre_scan(self):
        """Placeholder called on each instance of this class,
        before rescanning for new available devices.
        Useful for closing operations."""
        return True

    @classmethod
    def set_available_devices(cls, avail):
        """If  `avail` is a dictionary, directly set as available class attribute.
        If is a list, compile {devpath:dev} dictionary by replicating items in list
        and validating devpath"""
        if isinstance(avail, list):
            avail = {utils.validate_filename(v): v for v in avail}
        cls.available = avail

    @classmethod
    def from_devpath(cls, devpath):
        """Try to translate `devpath` into a physical device identifier (like a /dev/ file or specific driver enumerator).
        Returns `dev`==`devpath` if not found.
        The inverse of from_dev."""
        return cls.available.get(devpath, devpath)

    @classmethod
    def from_dev(cls, dev):
        """Try to translate a device system identifier `dev` into a device unique path devpath.
        Returns `dev` if not found.
        The inverse of from_devpath.
        """
        devpath = dev
        for dp, d in cls.available.items():
            if d == dev:
                devpath = dp
        return utils.validate_filename(devpath)

    def sleep(self, t=0.1):
        utils.sleep(t)

    def connect(self):
        """Dummy method for client-server mixed testing."""
        pass

    def connection(self, blacklist=[]):
        """Connect (initialize) device for operation."""
        self['isConnected'] = False
        dp = self['devpath']
        av = self.__class__.available
        if (dp not in av) or (av[dp] != self['dev']):
            self.log.debug('devpath not found in available', dp, av, dp in av, self['dev'])
            return False
        items = set([self[k] or 'UNKNOWN---ENRY---' for k in self.blacklist_options])
        if len(items - set(blacklist)) < len(items):
            self.log.info('Device is blacklisted', items, blacklist)
            return False
        self['isConnected'] = True
        return True

    def load_hardware_defaults(self):
        for opt in self.keys():
            if 'Hardware' in self.getattr(opt, 'attr'):
                self.log.debug('Loading factory default into hardware', opt)
                self[opt] = self.getattr(opt, 'factory_default')

    def post_connection(self):
        """Executed immediately after a successful device initialization and connection."""
        # Update available presets
        v = self.desc.listPresets()
        self.log.debug('post_connection, available presets:', v)
        # Try loading default configuration, if present
        r = self.set_preset('default')
        if not r:
            self.load_hardware_defaults()

    def map_role_dev(self, handle, new=None, force=False):
        """Processes a role-dev association for option `handle`.
        Returns:
        False on failure - old value should be restored.
        None on unset - new value is empty role
        obj,preset,io - new object role was defined with preset and io
        """
        prop = self.desc.gete(handle)
        slot = prop['type']
        # Discard non-role objects
        if not slot.startswith('Role'):
            return False
        # Empty entry
        empty_entry = (False, False, False)
        oldentry = self.roledev.get(handle, empty_entry)
        self.roledev[handle] = empty_entry
        isIO = slot.endswith('IO')
        if not force and self.root_isRunning and not isIO:
            self.log.critical(
                'Tentative to change role-dev mapping while running. Option:', handle)
            return False
        if isIO:
            cur = prop.get('options', ['None'] * 3)
            iokid = cur[2]
        else:
            cur = prop['current']
            iokid = False
        if new is not None:
            cur = new
        path = cur[0]
        preset = cur[1]
        io = False
        self.log.debug('map_role_dev', self['fullpath'], handle, cur, isIO)
        # Identify invalid, incomplete, empty configurations
        if path in ['None', None]:
            self.log.info('Un-set Role configuration for:', handle, cur)
            cur[0] = 'None'  # avoid None
            cur[1] = 'default'
            if isIO:
                cur[2] = 'None'
            if isIO:
                self.desc.setattr(handle, 'options', cur)
                self.desc.set(handle, prop['factory_default'])
            else:
                self.desc.set(handle, cur)
            return None
        if path == '.':
            obj = self
        else:
            # Search for configured dev path
            hp = self.root_obj.searchPath(path)
            if not hp:
                self.log.debug('Devpath not found:', handle, path, hp, cur)
                return False
            # Translate the hierarchy path into the real object
            obj = self.root_obj.toPath(hp)
            self.log.debug('map_role_dev toPath', self['fullpath'], hp, cur, obj['fullpath'])
        if obj is None:
            self.log.debug('Object not found in hierarchy', handle, hp, obj)
            return False
        # Get the IO handler if needed
        if isIO:
            # Intercept IO redirects to another role!
            if iokid not in obj:
                self.log.debug('IO Not found', handle, repr(hp), cur)
                return False
            iotype = obj.gete(iokid)['type']
            if iotype.startswith('Role'):
                self.log.debug('Resolving role', obj['fullpath'], iokid)
                r = obj.map_role_dev(iokid)
                if r:
                    self.roledev[handle] = r
                    return r
            io = obj.io(iokid)
            if not io:
                self.log.debug('IO Not found', handle, repr(hp), cur)
                return False
        # Init a new sample, but only if it was updated
        isSample = handle.startswith('smp') and prop['parent'] == 'nSamples'
        if isSample and (oldentry[0] is not obj):
            self.log.debug('map_role_dev: re-initializing sample', handle)
            self.init_sample(obj, handle)
        # Remember the association
        self.roledev[handle] = (obj, preset, io)
        # Proxy the object
        if io == False:
            self.putSubHandler(handle, obj)
        return obj, preset, io

    def dev2role(self, fullpath):
        """Search a role for device fullpath"""
        for role, obj in self.roledev.items():
            obj = obj[0]
            if not obj:
                continue
            if obj['fullpath'] == fullpath:
                return role
        return False

    def xmlrpc_roledev(self, handle):
        """Debug print of assigned roledevs"""
        obj = self.roledev.get(handle, [False, False, False])
        if obj[0] is False:
            return 'NO OBJECT ' + handle
        return obj[0]['fullpath'], obj[1:]

    def wiring(self, definitions=False):
        """Render a dot file representing all RoleIO relations of this object and all its children."""
        """
        digraph structs {
            node [shape=record];
            "name0 fullpath0"[label="<out1> first out|<out2> second out|<out3> third out"];
            "name1 fullpath1"[label="<role1> first role|<role2> second role|<role3> third role"];
            "name0 fullpath0":out1 -> "name1 fullpath1":role2;
        }
        """
        body = ''
        main_call = definitions is False
        if main_call:
            definitions = defaultdict(list)
        title = '{}\\n{}'.format(self['name'], self['fullpath'])
        for (handle, (obj, preset, io)) in self.roledev.items():
            definitions[title].append(handle)
            if obj is False:
                continue
            title_dev = '{}\\n{}'.format(obj['name'], obj['fullpath'])
            if io is False:
                connection = '"{}":{} -> "{}";\n'.format(title, handle, title_dev)
            else:
                connection = '"{}":{} -> "{}":{};\n'.format(
                    title, handle, title_dev, io.handle)
                definitions[title_dev].append(io.handle)
            body += connection
        # Recursive call
        for obj in self.devices:
            obj_body, definitions = obj.wiring(definitions)
            body += obj_body

        if not main_call:
            return body, definitions

        if body == '':
            return ''
        # Add definitions fields
        header = ''
        for title, sockets in definitions.items():
            label = '<{}> {}|' * len(sockets)
            sockets2 = []
            for s in sockets: sockets2 += [s, s]
            label = label.format(*sockets2)[:-1]
            header += '"{}" [label="{} |{}"];\n'.format(title, title, label)
        return 'digraph {{\n rankdir=LR;\n node [shape=record];\n{}\n{}\n}}'.format(header, body)

    xmlrpc_wiring = wiring

    def render_wiring(self, definitions=False):
        """Render wiring dot graph to temporary svg file.
        Return rendered file path"""
        dot = self.wiring(definitions)
        self.log.debug('GRAPH:\n', dot)
        handle, filename = tempfile.mkstemp()
        self.log.debug('tmpfile', handle, filename)
        os.write(handle, dot.encode('utf8'))
        os.close(handle)
        out = subprocess.check_output('dot -O -T svg {}'.format(filename), shell=True)
        self.log.debug('dot call:', out)
        svg_filename = filename + '.svg'
        svg = open(filename + '.svg', 'r').read()
        os.remove(filename)
        os.remove(svg_filename)
        return svg

    xmlrpc_render_wiring = render_wiring

    def init_sample(self, obj, handle):
        return True

    def check(self):
        # Skip if initializing.
        if self['initializing']:
            self.log.debug('Cannot check() while initializing')
            return False
        # Join any finished child procs
        self.mp.active_children()
        # Refresh running status
        if self['running'] and self['anerr'] > self['maxErr']:
            self.log.error('Runtime errors exceed maxErr', self['anerr'])
            return False
        if self['loopErr'] > 3:
            self.log.error('Control loop failing', self['loopErr'])
            return False
        r = self.check_children()
        return r

    xmlrpc_check = check

    def check_children(self):
        """Propagate check() to all subdevices."""
        done = [self]
        ret = 1
        for obj in self.devices:
            if obj in done:
                continue
            try:
                r = obj.check()
            except:
                self.log.error('check error', format_exc())
                r = False
            count = r or -1
            # Increase/decrease error count
            anerr = obj['anerr'] - count
            # Avoid below-zero
            if anerr >= 0:
                obj['anerr'] = anerr
            done.append(obj)
            ret *= r
        return ret

    def do_self_test(self):
        """Returns a list of (status, message) validation items"""
        r = []
        if self['initializing']:
            r.append([0, 'Device is still initializing'])
            self.log.warning('Device is still initializing')
        for key in self.desc.keys():
            role = self.roledev.get(key, None)
            if role is None:
                # Not a Role/RoleIO
                continue
            a = self.desc.getAttributes(key)
            if 'Required' not in a:
                # Not a Required Role
                continue
            if not role[0]:
                self.log.warning('Required role is not assigned', key)
                r.append([0, 'Required role is not assigned: ' + key])
        return len(r) == 0, r

    def get_selfTest(self):
        """Returns local validation items"""
        return self.do_self_test()[1]

    def do_iter_test(self, done=False):
        """Collects validation items from across all subdevices"""
        p = self['fullpath']
        done = done or set([p])
        ok = self.check()
        status = []
        if not ok:
            status.append([0, 'Periodic check is failing', p])
        for item in self.do_self_test()[1]:
            status.append(item + [p])
        for obj in self.devices:
            if not getattr(obj, 'do_iter_test', False) or not getattr(obj, 'desc', False):
                self.log.error('Child object cannot be validated', obj)
                continue
            p = obj['fullpath']
            if p in done:
                continue
            ok1, status1 = obj.do_iter_test(done)
            status += status1
            ok *= ok1
            done.add(p)
        return ok, status

    xmlrpc_do_iter_test = do_iter_test

    def get_validate(self):
        """Pre-test status validation and error reporting"""
        return self.do_iter_test()[1]

    def lock(self, blocking=True, exc=False):
        """Blocks current operations on device.
        `blocking`=False immediately returns the lock status (default: True, wait until free).
        `exc`=True throws an exception if device is locked (default: False, ignore)."""
        a = self._lock.acquire(blocking)
        if not blocking:
            if not a and exc:
                self.log.debug('Already locked')
                raise AlreadyLocked()
        if not a:
            print('Impossible to acquire lock', a)
        return a

    def get_locked(self):
        # Returns True if locked
        r = self._lock.acquire(False)
        # If it was not locked, release it immediately
        if r:
            self._lock.release()
        return not r

    def set_locked(self, v):
        if v:
            self._lock.acquire(1)
        else:
            self.unlock()
        return v

    def unlock(self):
        """Unblock device for concurrent operations."""
        try:
            self._lock.release()
        except:
            pass
        return True

    @initializeme(repeatable=True)
    def applyDesc(self, desc=False):
        """Apply current settings"""
        if desc is not False:
            self.desc.update(desc)
        else:
            desc = self.desc.describe([])
        # Trigger get/set function for options involving Hardware
        kn = self.desc.getKeep_names()
        # Sort by priority
        items = list(desc.values())
        items = sorted(items, key=cmp_to_key(option.prop_sorter))
        if self['isConnected']:
            for ent in items:
                key = ent['handle']
                if key in kn:
                    continue
                if ent['type'] in ['ReadOnly', 'Button', 'Hidden', 'Progress']:
                    continue
                if ent['type'].startswith('Role'):
                    self.log.info('Loading role', key, ent['current'])
                    self.map_role_dev(key)
                ro = set(['ReadOnly', 'Runtime']) - set(ent['attr'])
                if len(ro) < 2:
                    continue
                if key in self.controls:
                    self.log.info(
                        'Loading option control', key, ent['current'])
                    self.controls[key].set(ent['current'])
                elif 'Hardware' in ent['attr']:
                    self.log.info('Loading option', key, ent['current'])
                    self.set(key, ent['current'])
                
        return desc

    def io(self, handle):
        """Return an InputOutput object for option `handle`."""
        for opt in self.desc.describe().values():
            if opt['handle'] == handle:
                return InputOutput(opt, self)
        return None

    def set_preset(self, preset):
        new_preset = self.validate_preset_name(preset)
        return super().set_preset(new_preset)

    def init_instrument(self, name=False, sub=True):
        """Initialize device for instrument `name` use"""
        preset = self.set_preset(name)
        # Initialize sub-devices
        if sub:
            for dev in self.devices:
                self.log.debug('Initializing subdevice with', name, dev['devpath'])
                dev.init_instrument(name)
        self.log.info('Initialized instrument:', name, self['name'],
                      self['devpath'], preset, self['preset'], 'sub', sub)
        return True

    def xmlrpc_init_instrument(self, *a, **k):
        return self.init_instrument(*a, **k)

    def init_acquisition(self, instrument, *args):
        """Here each device should prepare itself for acquisition, 
        """
        self.log.debug('init_acquisition', instrument, *args)
        self.reset_acquisition()
        self['analysis'] = True
        return True

    control_write_cache = {}

    def prepare_control_loop(self, zerotime=0, **kwa):
        """Preparation for the run_acquisition control loop. Runs in the same subprocess.
        Returns True if the device control is allowed to start, False if there was an initialization problem and
        run_acquisition should abort.
        Separed for testing purposes"""
        self['anerr'] = 0
        self.control_write_cache = {}
        self.control_freq = self['maxfreq']
        self.control_monitor = self['monitor']
        # dt = zerotime-self.time()
        # if dt>0:
        #    self.log.debug('Sleeping for exact zerotime', dt)
        #    self.sleep(dt)
        return True

    def set_monitor(self, k):
        """Append kid `k` to `monitor` option if missing.
        If `k` is a sequence, fully replace the option."""
        if isinstance(k, list) or isinstance(k, tuple):
            return k
        m = self['monitor']
        if k not in m:
            m.append(k)
        return m

    def oldest_refresh_time(self, monitored):
        """Find the option with oldest refresh time in `monitored` list of options"""
        t = utils.time()
        delta = 0
        delta_opt = False
        for k in monitored:
            obj, opt = self.read_kid(k)
            if not obj:
                self.log.debug('Monitored option not found', k)
                continue
            oldest = obj.h_time_at(opt, -1)
            d = t - oldest
            if d > delta:
                delta = d
                delta_opt = k
        return delta_opt, delta

    control_error_cache = set([])
    control_freq = None
    monitor = None

    def control_loop(self, t=None, zerotime=0, **kw):
        """Called each acquisition/control iteration, in a separate process.
        Reads each option referenced in `monitor` option.
        Reads all custom controls with in_acquisition_process flag set.
        """
        n = 0
        t = t or utils.time()
        wait = zerotime - t
        if wait > 0:
            self.log.debug('Waiting for zerotime', wait)
            self.sleep(wait)
        ns = 0
        # Manage special controls
        for handle, ctrl in self.controls.items():
            if ctrl.in_acquisition_process == control.InAcquisitionProcess.FORBIDDEN:
                continue
            if ctrl.in_acquisition_process > control.InAcquisitionProcess.READONLY:
                cached = self.control_write_cache.get(handle, None)
                if cached is not None:
                    continue
                new = self.desc.get(handle)
                if new != cached:
                    ns += 1
                    print('SET NEW CONTROL VALUE', handle, new)
                    r = ctrl._set(new)
                    self.desc.set(handle, r)
                    self.control_write_cache[handle] = r
                    print('DONE', handle, r)
                if ctrl.in_acquisition_process == control.InAcquisitionProcess.WRITEONLY:
                    continue
            if not ctrl.in_acquisition_process:
                continue
            v = ctrl._get()
            if v is None:
                if handle not in self.control_error_cache:
                    self.log.debug(
                        'Error reading `{}` in acquisition process'.format(handle))
                    self.control_error_cache.add(handle)
                continue
            if handle in self.control_error_cache:
                self.log.debug('Resumed reading `{}` in acquisition process after error occurred'.format(handle))
                self.control_error_cache.remove(handle)
            n += 1
            # Set new value in-memory
            self.desc.set(handle, v, t=t)
        # Manage monitored options
        # m = self['monitor']
        m = self.control_monitor
        if not n and len(m) == 0:
            self.log.error(
                'No control and no monitored options were defined for acquisition. Exiting.')
            return False
        m0 = m[:]

        for k in m0:
            obj, opt = self.read_kid(k)
            if not obj:
                self.log.debug('Monitored option does not exist:', k)
                m.remove(k)
                continue
            try:
                v = obj[opt]
                e = self['anerr']
                if e > 0:
                    self['anerr'] = e - 1
                n += 1
            except:
                self['anerr'] += 1
                self.log.error('Error getting monitored option {} in parallel process.'.format(
                    opt), format_exc())
        # Purge non-existent options from monitor list
        if len(m) != len(m0):
            self.desc.set('monitor', m)
        self.limit_freq(t, freq=self.control_freq)
        # print('control_loop',n,ns)
        return True

    def limit_freq(self, t, t1=-1, freq=0):
        """Wait if cycle is faster than device frequency"""
        if not freq:
            freq = self['maxfreq']
        if not freq:
            return 0
        s, f = utils.limit_freq(t, t1, freq, ret=True)
        self['throttling'] = 1000 * s
        return s

    def process_alive(self):
        self.mp.active_children()
        pt = self.process_name()
        proc = linux.get_process_stats(pt)
        pid = self.get('pid', 0)
        alive = False
        if pid:
            alive = utils.check_pid(pid)
        for tabled in proc['proc_pid']:
            if not alive:
                self.log.info('process_alive: replacing dead PID', pt, pid, tabled)
                pid = tabled
                self['pid'] = tabled
                alive = True
                break
            # elif tabled != pid:
                # self.log.error('process_alive: killing foreign PID', pid, tabled, pt)
                # utils.kill_pid(pid)
                # self['pid'] = tabled
                # pid = tabled
        if not pid or not alive:
            self['pid'] = 0
            return False
        return True
    
    def process_kill(self):
        pt = self.process_name()
        proc = linux.get_process_stats(pt)
        pid = self.get('pid', 0)
        if pid not in proc['proc_pid']:
            self.log.error('process_alive: memory PID differs from OS', pid, proc['proc_pid'])
        for pid in proc['proc_pid']:
            self.log.debug('process_kill', pid)
            utils.kill_pid(pid)

    def xmlrpc_process_alive(self):
        return self.process_alive()

    def set_running(self, nval, zt=-1):
        """Start/stop the acquisition process"""
        if nval and self['initializing']:
            self.log.error(
                'Cannot start parallel acquisition while still initializing')
            nval = 0
        nval = int(nval)
        current = self.desc.get('running')
        if current == nval:
            self.log.debug('Running state already set:', current)
            return current
        nval0 = nval
        if nval == STOPPING:
            nval = STOPPED
        # immediately communicate the new status
        self.desc.set('running', nval)
        self.log.debug('running set to', nval)
        if nval0 == STOPPING:
            return nval
        if nval == STOPPED:
            # Wait until the process stops
            if self.process_alive():
                # Called while closing...
                if self['initializing']:
                    self.log.debug('Trying to stop process while initializing')
                    return STOPPED
                pid = self['pid']
                self.log.debug('A parallel process was defined.', pid)
                self.desc.set('running', STOPPED)
                r = utils.join_pid(pid, 12, self['fullpath'])
                if not r:
                    self.log.critical('Terminating acquisition process!', pid,
                                      self.desc.get('running'), self['running'])
                    self.process_kill()
                self.desc.set('running', STOPPED)
                self.unlock()
                self.process = False
                self['pid'] = 0
        elif nval == RUNNING:
            if self.process_alive():
                self.log.error(
                    'Asked to start but already running as pid', self['pid'])
                return 1
            # Start a new subprocess
            if zt < 0:
                zt = self.root_obj['zerotime']
            self.log.info('Starting acquisition process')
            self.reset_acquisition()
            self.desc.set('running', RUNNING)
            self.process = self.mp.Process(target=self.run_acquisition,
                                           args=(zt,),
                                           kwargs={'spr': sharedProcessResources},
                                           name=self['fullpath'] + 'run_acquisition')
            self.process.daemon = self._daemon_acquisition_process
            self.process.start()
            self['pid'] = self.process.ident
            self.log.info(
                'Started acquisition process with pid', self.process.ident)
            nval = 1
        else:
            assert False, "set_running to invalid value {}".format(nval)
        self.mp.active_children()
        return nval

    def get_running(self):
        """Retrieve the parallel acquisition process status.
        It is a combination of the in-memory valued AND the real process status."""
        p = self.process_alive()
        r = self.desc.get('running')
        if not p:
            # underlying process is dead
            return STOPPED
        if r != RUNNING:
            # flag already unset, but process still alive
            return STOPPING
        return RUNNING

    def restart(self):
        self.log.debug('Restaring...')
        r = self.set_running(False)
        self.log.debug('Restart: stopped', r)
        self.set_running(True)
        self.log.debug('Restart: started', r)
        return self['running']

    def soft_get(self, key):
        """Avoid triggering actions on opt requests while the device is already running.
        Get a key from memory if running, from self if not running."""
        if self['running'] != STOPPED:
            return self.desc.get(key)
        else:
            return self.get(key)

    xmlrpc_soft_get = soft_get

    def process_name(self):
        return 'MdfServer.run:' + self['fullpath'] + '---'

    def run_acquisition(self, zerotime, *args, **kwargs):
        """Continously call control_loop.
        """
        spr = kwargs.get('spr', False)
        if spr: spr()
        if zerotime <= 0:
            zerotime = utils.time()
        if 'zerotime' in self.desc:
            self['zerotime'] = zerotime
        self.set('analysis', True)
        self.desc.set('running', RUNNING)
        self.log.debug('run_acquisition set running to', self.desc.get('running'))
        if not self.prepare_control_loop(zerotime=zerotime):
            self.log.error(
                "Control loop initialization problem. Aborting device control.")
            self.end_acquisition()
            self['loopErr'] += 1
            return False

        pt = self.process_name()
        setproctitle.setproctitle(pt)
        setproctitle.setthreadtitle(pt)
        i = 0
        while True:
            i += 1
            try:
                if not self.control_loop(zerotime=zerotime):
                    self.log.debug('Control loop returned False')
                    self['loopErr'] += 1
                    break
            except:
                self.log.error('control_loop exception', format_exc())
                self['loopErr'] += 1
                break
            if self.desc.get('running') != RUNNING:
                self.log.debug('Exited from running state')
                self['loopErr'] += 1
                break
            i = 0
            anerr = self['anerr']
            maxErr = self['maxErr']
            if anerr >= maxErr:
                self.log.debug('Exceeded maximum error count', anerr, maxErr)
                break
            self['loopErr'] = 0
            continue
        self.end_acquisition()

        for i in range(10):
            if not self.mp.active_children():
                break
            self.log.error('Waiting for active children', multiprocessing.active_children())
            self.sleep(1)
        [p.terminate() for p in self.mp.active_children()]
    
    def wait_next_loop(self, timeout=0.5):
        t0 = self.time()
        stamp = None
        # TODO: push on change with redis?
        while self.time() - t0 < timeout:
            stamp1, val = self.h_get('loopErr', -1)
            if stamp is None:
                stamp = stamp1
            elif stamp != stamp1:
                return True
            self.sleep(0.01)
        return False
    
    def end_acquisition(self):
        """Executed when acquisition loop ends"""
        self.desc.set('running', STOPPED)
        self.set('analysis', False)
        self.log.debug('Acquisition loop ended.', os.getpid(), self['pid'])
        return True

    def iter_samples(self):
        return []

    @unlockme()
    def reset_acquisition(self):
        """Clears out the history buffer and resets all History values to factory defaults.
        Run at acquisition initialization and right before starting the acquisition subprocess.
        Always run in the main server process."""
        self.log.debug('Resetting options for new acquisition.')
        self['anerr'] = 0
        for k in self.desc.keys():
            try:
                opt = self.desc.gete(k)
            except:
                from traceback import print_exc
                print_exc()
                continue
            a = opt['attr']
            if 'Reset' in a:
                self.desc.set(k, self.desc.getattr(k, 'factory_default'))
            if 'History' in a:
                # Clear history buffer
                self.desc.h_clear(k)
            if k in self.controls:
                # Clear kalman filter
                self.controls[k].reset()
            # Ensure reset of all Meta options
            if opt['type'] != 'Meta':
                continue
            self.desc.set(k, {'temp': 'None', 'time': 'None', 'value': 'None'})

    xmlrpc_reset_acquisition = reset_acquisition

    def close(self):
        option.Aggregative.close(self)
        milang.Scriptable.close(self)
        if self.desc is False:
            return False
        print('Device.close', self)
        self['initializing'] = True
        self.set_running(STOPPED)
        try:
            dp = self.desc.get('devpath')
            dev = self.desc.get('dev')
            if dev:
                get_registry().purge(dev)
            else:
                print('Could not find attribute', self.name, self.node, dev, dp)
            cls = self.__class__
            print('deleting available', dp, dev)
            if dp in cls.available:
                del self.__class__.available[dp]
        except:
            pass
        return Node.close(self)

    def xmlrpc_close(self, writeLevel=4):
        return self.close()

    def __del__(self):
        print('Device.__del__: garbage collection', self.name, self.node)
        try:
            import sys
            sys.excepthook = lambda *a: print('Exception during Device.__del__, ignored')
        except ImportError:
            return
        if hasattr(self, 'desc'):
            self.close()
        try:
            sys.excepthook = sys.__excepthook__
        except:
            pass
