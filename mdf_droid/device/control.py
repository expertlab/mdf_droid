#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Generic option Control object"""
import numpy as np
from mdf_canon.csutil import time
from mdf_canon.kalman import KalmanFilter, iter_kalman_definitions
from mdf_canon.option import ao
from mdf_droid.device.configuration import FunctionRoutingError


class InAcquisitionProcess():
    FORBIDDEN = -1
    DISABLED = 0
    READONLY = 1
    WRITEONLY = 2
    READWRITE = 3
    
    @classmethod
    def check_attributes(cls, attributes):
        for k, v in cls.__dict__.items():
            if 'Parallel' + k.capitalize() in attributes:
                return v
        return cls.DISABLED


def add_kalman_definitions(handle, dev, conf_def):
    """Add kalman definitions to `conf_def` options list in object definition"""
    
    added = set([])
    skipped = set([])
    if 'kalman' not in dev:
        ao(conf_def, 'kalman', 'Section', name='Filter')
        added.add('kalman')
    for newkey, kw in iter_kalman_definitions(handle).items():
        if newkey in dev:
            # Current redefinition
            if len(dev[newkey]) == 1:
                kw['current'] = dev[newkey][newkey]
            # Complete definition
            else:
                skipped.add(newkey)
                continue
        ao(conf_def, **kw)
        added.add(newkey)
    return added, skipped


class BaseControl(object):
    """Generic control object managing get/set and flags on behalf of an Option.
    Controls might live in separate processes."""
    in_acquisition_process = InAcquisitionProcess.DISABLED
    """Reading this value must always happen in a separate acquisition process."""
    limit_freq = 0
    """Max queries per second"""
    read_only = False
    write_only = False
    last_query = -1

    def __init__(self, parent, handle, read_only=False):
        self.parent = parent
        self.log = self.parent.log
        self.handle = handle
        self.read_only = read_only
        self.prop = self.parent.gete(handle)
        self.in_acquisition_process = InAcquisitionProcess.check_attributes(self.attr)
        # Create a reference in parent controls
        parent.controls[handle] = self
        
    def reset(self):
        return True
        
    @property
    def attr(self):
        return self.prop.get('attr', [])
    
    def _get(self):
        """Actually read the value in an autostarted Control."""
        return self.parent.desc.get(self.handle)
        
    def _set(self, val):
        """Set value to external device or process"""
        return val
    
    def get(self):
        return self.get()
    
    def set(self, val):
        return self._set(val)

    def getFlag(self, flag):
        return False

    def setFlag(self, flag, val):
        return False

    
class Control(BaseControl):
    _kalman = None

    def reset(self):
        self._kalman = None
        return True

    @property
    def kalman(self):
        pre = 'kalman_' + self.handle + '_'
        if not self._kalman:
            self._kalman = KalmanFilter(log=self.log)
            
        for opt in KalmanFilter.options:
            val = self.parent.get(pre + opt, 0)
            setattr(self._kalman, opt, val)
        self._kalman.update_outputs()
        return self._kalman
    
    @kalman.setter
    def kalman(self, new):
        self._kalman = new

    def get(self):
        """Manage the autostart of the Control in a separate process."""
        # Manage autostart
        if 'ReadOnChange' in self.attr:
            return self.parent.desc.get(self.handle)
        if self.in_acquisition_process in (InAcquisitionProcess.FORBIDDEN, InAcquisitionProcess.WRITEONLY):
            return self.parent.desc.get(self.handle)
        if self.in_acquisition_process in (InAcquisitionProcess.READONLY, InAcquisitionProcess.READWRITE):
            r = self.parent['running']
            # Start acquisition if not running
            if not r:
                self.log.debug('Restarting acquisition process for control', self.handle)
                self.parent['running'] = 1
            # Return in-memory value
            return self.parent.desc.get(self.handle)
        if self.limit_freq:
            t = time()
            f = 1. / (t - self.last_query)
            self.last_query = t
            if f > self.limit_freq:
                # self.parent.log.debug('limit_freq', self.handle, self.limit_freq, f)
                return self.parent.desc.get(self.handle)
                
        # Else, actually read the value and return
        r = self._get()
        if ('FilterGet' in self.attr) and self.kalman.active:
            r = self.kalman(time(), r)
            self.save_kalman_status()
        return r
    
    def set(self, val, t=None):
        """Set current value and return operation status."""
        if 'WriteOnChange' in self.attr:
            if val == self.parent.desc.get(self.handle):
                return val
        if self.in_acquisition_process in (InAcquisitionProcess.READONLY, InAcquisitionProcess.FORBIDDEN):
            return self.parent.desc.get(self.handle)
        if self.in_acquisition_process in (InAcquisitionProcess.WRITEONLY, InAcquisitionProcess.READWRITE):
            if not self.parent.desc.get('running'):
                self.log.debug('Restarting acquisition process for control', self.handle)
                self.parent['running'] = 1
            return val
        r = self._set(val)
        if r is None:
            return r
        if ('FilterSet' in self.attr) and self.kalman.active:
            t = t if t > 0 else time()
            r = self.kalman(t or time(), r)
            self.save_kalman_status()
        return r
    
    def save_kalman_status(self):
        """Set ancillary options"""
        for out in self.kalman.outputs:
            h = 'kalman_' + self.handle + '_' + out
            if h in self.parent:
                val = getattr(self.kalman, out, 0)
                if val is not None:
                    self.parent[h] = val
        return True

"""
# Example Calibrator option structure:

    {"handle": 'T',
     "name": 'Sample Temperature',
        "type": 'Float', 'unit': 'celsius',
        "attr": ['History', 'ReadOnly'],
     },
    {"handle": 'rawT',
     "name": 'Raw Temperature',
        "parent": 'T',
        "type": 'Float', 'unit': 'celsius',
        "attr": ['History', 'ReadOnly'],
     },

    {"handle": 'calibrationT',    "name": 'Sample Calibration',
     "current": [[('Measured', 'Float'),
                  ('Theoretical', 'Float'),
                  ],
                 [20,20]
                 ],
        "unit": ['celsius', 'celsius'],
        "type": 'Table',
        "writeLevel": 4,
     },
"""


class Calibrator(Control):
    """Generic calibration control.
    The host Device must define also rawOption (Float)
    and calibrationOption (2 col Floats table, Measured, Theoretical)"""
    _calibration_func = False
        
    def reset(self):
        self._calibration_func = False
        return True

    @property
    def calibration_func(self):
        """Cache a polynomial calibration function"""
        # Return if already defined
        if self._calibration_func:
            return self._calibration_func
        # Disabled
        if self._calibration_func is None:
            return False
        cal = self.parent['calibration' + self.handle]
        
        if 'calibrationDeg' + self.handle in self.parent:
            deg = self.parent['calibrationDeg' + self.handle]
        else:
            deg = min(3, len(cal) - 2)
        
        # Disable if not enough points
        if len(cal) - 2 < deg or deg <= 0:
            self._calibration_func = None
            return False
        # Create new function
        m, t = [], []
        for v0, v1 in cal:
            m.append(v0)
            t.append(v1)
        self.parent.log.debug('Recreating calibration_func', self.handle, deg, m, t)
        factors = np.polyfit(m, t, deg=deg)
        self._calibration_func = np.poly1d(factors)
        return self._calibration_func
    
    _inverse_func = False

    @property
    def inverse_func(self):
        """Cache the inverse of the calibration function"""
        # Return if already defined
        if self._inverse_func:
            return self._inverse_func
        # Disabled
        if self._inverse_func is None:
            return False
        cal = self.parent['calibration' + self.handle]
        
        if 'calibrationDeg' + self.handle in self.parent:
            deg = self.parent['calibrationDeg' + self.handle]
        else:
            deg = min(3, len(cal) - 2)
        
        # Disable if not enough points
        if len(cal) - 2 < deg or deg <= 0:
            self._inverse_func = None
            return False
        # Create new function
        m, t = [], []
        for v0, v1 in cal:
            m.append(v0)
            t.append(v1)
        self.parent.log.debug('Recreating inverse_func', self.handle, deg, m, t)
        factors = np.polyfit(t, m, deg=deg)
        self._inverse_func = np.poly1d(factors)
        return self._inverse_func
    
    def calibrated(self, nval):
        """Return theoretical value from measured value `nval`"""
        if not self.calibration_func:
            return nval
        # Store in rawT dataset
        self.parent['raw' + self.handle] = nval
        return self._calibration_func(nval)
    
    def inverse(self, val):
        """Return the required measured value in order 
        to obtain a calibrated value `val`"""
        if not self.inverse_func:
            return val
        return self._inverse_func(val)
    
