#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
import os
from copy import deepcopy

from mdf_canon.option import ao
from mdf_droid import device
from mdf_droid.tests import reset_test_memory
print('Importing', __name__)


def setUpModule():
    print('Starting', __name__)
    reset_test_memory()


class FakeDeviceA(device.Device):
    pass


class FakeDeviceB(device.Device):
    pass


class TestDevicePathRegistry(unittest.TestCase):

    def setUp(self):
        self.ds = device.DeviceServer()
        self.dev1 = FakeDeviceA(self.ds, 'dev1')
        self.dev1['dev'] = '/dev/1'
        self.dev2 = FakeDeviceB(self.ds, 'dev2')
        self.dev2['dev'] = '/dev/2'
        self.reg = device.DevicePathRegistry()
        
    def tearDown(self):
        # from time import sleep; sleep(60)
        self.reg.close()
        self.ds.close()
        self.dev1.close()
        self.dev2.close()

    def test_assign_free(self):
        lst = set(['/dev/1', '/dev/2'])
        av = lambda: self.reg.check_available(lst)
        self.assertSetEqual(av(), lst)
        r = self.reg.assign(self.dev1['dev'], FakeDeviceA)
        self.assertTrue(r)
        self.assertSetEqual(av(), set(['/dev/2']))
        # unassign from other server should fail
        self.assertFalse(self.reg.free(self.dev1, 'blabla'))
        # assign to other server should fail
        self.assertFalse(self.reg.assign(self.dev1, 'blabla'))
        # reserve to other server should fail
        self.assertFalse(self.reg.reserve(self.dev1, 'blabla'))
        # reserve dev2
        r = self.reg.reserve(self.dev2['dev'], FakeDeviceB)
        self.assertTrue(r)
        # reservation should also make the device unavailable
        self.assertEqual(av(), set())
        # freeing
        self.assertTrue(self.reg.free(self.dev1, FakeDeviceA))
        self.assertSetEqual(av(), set(['/dev/1']))
        self.assertFalse(self.reg.free(self.dev2, FakeDeviceA))
        self.assertTrue(self.reg.free(self.dev2, FakeDeviceB))
        self.assertSetEqual(av(), lst)
        # Removal will return True
        self.assertTrue(self.reg.free(self.dev2, FakeDeviceB))

    def test_free_all(self):
        self.reg.assign(self.dev1['dev'], FakeDeviceA)
        self.reg.assign(self.dev2, FakeDeviceB)
        r = self.reg.free_all('pippo')
        self.assertEqual(r, 0)
        self.assertEqual(len(self.reg['reg']), 2)
        r = self.reg.free_all(FakeDeviceA, FakeDeviceB)
        self.assertEqual(r, 2)


if __name__ == "__main__":
    unittest.main()
