#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest

from mdf_droid import device
from mdf_droid.device.serialdevice import SerialMessage

print('Importing', __name__)


def setUpModule():
    print('Starting', __name__)


class TestSerial(unittest.TestCase):
    
    def test_validate_received_message(self):
        v, r = SerialMessage.validate('', minlen=1)
        self.assertEqual(v, SerialMessage.SHORT)
        self.assertEqual(v.errors(), ['Too short to be a reply'])
        v, r = SerialMessage.validate('sdrtsth', maxlen=3)
        self.assertEqual(v, SerialMessage.LONG)

    def test__init__(self):
        print(device.Serial.list_available_devices())
        d = device.Serial(node='/dev/ttyS0')
        self.assertTrue(d['dev'], '/dev/ttyS0')
        
    def test_fake(self):
        d = device.Serial(node='fake',
                          serial_class=device.serialdevice.FakeSerial)
        d.available['fake'] = 'fake'
        self.assertTrue(d.connection())
        
        d.com._set_instance_attribute('output_buffer', 'ciao')
        self.assertEqual(d.com.inWaiting(), 4)
        self.assertEqual(d.com._get_instance_attribute('output_buffer'), 'ciao')
        self.assertEqual(d.raw('test'), b'empty')
        

if __name__ == "__main__":
    unittest.main(verbosity=2)
