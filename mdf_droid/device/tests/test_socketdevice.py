#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
import os
from mdf_droid import device
from random import randint
import threading
import socket

print('Importing ', __name__)
PORT = 38322 + randint(0, 1000)
server = False


def setUpModule():
    print('Starting ', __name__)
    os.environ['http_proxy'] = ''


def start():
    srv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Avoid "already in use" error
    srv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    srv.bind(('127.0.0.1', PORT))
    srv.listen(1)

    def accept():
        print('Dummy server waiting for connection...')
        srv.accept()

    server = threading.Thread(target=accept)
    server.start()
    return server


class TestSocket(unittest.TestCase):

    def setUp(self):
        self.d = device.Socket(node='127.0.0.1:%i' % PORT)

    def tearDown(self):
        self.d.close()

    def test_nodelay(self):
        d = self.d
        srv = start()
        d.connection()
        self.assertTrue(d['isConnected'])
        srv.join()
        srv = start()
        with d.socket() as s:
            delay = s.getsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY)
            self.assertEqual(delay, False)
        srv.join()
        d['nodelay'] = True
        srv = start()
        with d.socket() as s:
            delay = s.getsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY)
        self.assertEqual(delay, True)
        srv.join()

    def test_connect(self):
        d = self.d
        self.assertEqual(d['addr'], '127.0.0.1')
        self.assertEqual(d['port'], PORT)
        self.assertFalse(d['isConnected'])

        self.assertFalse(d.connection())
        self.assertFalse(d['isConnected'])

        server = start()
        d.connection()
        self.assertTrue(d['isConnected'])
        server.join()

# TODO: test enumeration (with generic DeviceServer)


if __name__ == "__main__":
    unittest.main(verbosity=2)
