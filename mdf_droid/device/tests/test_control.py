#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
from mdf_droid import device
from mdf_droid.device import control
from mdf_canon.option import mkheader
print('Importing', __name__)


def setUpModule():
    print('Starting', __name__)

    
cal = [[20, 120], [100, 200], [300, 400], [400, 500]]
cal1 = [[c[1], c[0]] for c in cal]
cal2 = [[20, 20], [538, 532], [1076, 1064], [1177, 1165]]


class TestControl(unittest.TestCase):
    
    def test_add_kalman_definitions(self):
        d = device.Device()
        a, s = control.add_kalman_definitions('d', d.conf_dict, d.conf_def)
        self.assertEqual(len(s), 0)
        self.assertEqual(len(a), 15)

    def test_kalman(self):
        d = device.Device()
        d.sete('v', {"type": 'Float', "attr": ['FilterSet']})
        sec = 'kalman_v_'
        d.sete(sec + 'sensorvar', {"type":'Float', "current":1})
        d.sete(sec + 'duration', {"type":'Float', "current":15})
        d.sete(sec + 'devfactor', {"type":'Float', "current":1})
        d.sete(sec + 'maxbuffer', {"type":'Integer', "current":5})
        d.sete(sec + 'minbuffer', {"type":'Integer', "current":3})
        d.sete(sec + 'unfiltered', {"type": 'Float'})
        d.sete(sec + 'correction', {"type": 'Float'})
        d.build_controls()
        d['v'] = 1
        self.assertEqual(d['v'], 1)
        self.assertEqual(d[sec + 'unfiltered'], 1)
        self.assertEqual(d[sec + 'correction'], 0)
        d['v'] = 2
        self.assertEqual(d['v'], 2)
        self.assertEqual(d[sec + 'unfiltered'], 2)
        self.assertEqual(d[sec + 'correction'], 0)
        self.assertFalse(d.controls['v'].kalman.full)
        for i in range(4):
            d['v'] = 2
        self.assertTrue(d.controls['v'].kalman.full)
        
        d['v'] = 100
        self.assertAlmostEqual(d['v'], 2, delta=1)
        self.assertEqual(d[sec + 'unfiltered'], 2)
        self.assertEqual(d[sec + 'correction'], -98)
    

class TestCalibrator(unittest.TestCase):
    
    def setUp(self):
        self.d = device.Device()
        self.d.sete('T', {"type": 'Float', "attr": ['History', 'ReadOnly']})
        self.d.sete('rawT', {"type": 'Float', 'unit': 'celsius'})
        self.d.sete('calibrationT', {"current": [], "type": 'Table',
                                     "header": mkheader(('Measured', 'Float'),
                                                       ('Theoretical', 'Float'))
                                     })
        device.Calibrator(self.d, 'T')
        
    def test_create(self):
        self.assertIn('T', self.d.controls)
        c = self.d.controls['T']
        self.assertTrue(c._calibration_func is False)
        self.assertTrue(c._inverse_func is False)
        self.assertEqual(c.calibrated(10), 10)
        self.assertTrue(c._calibration_func is None)
        self.assertEqual(c.inverse(10), 10)
        self.assertTrue(c._inverse_func is None)
        
    def test_calibrated(self):
        c = self.d.controls['T']
        self.d['calibrationT'] = cal
        n = c.calibrated(100)
        self.assertFalse(not c.calibration_func)
        self.assertAlmostEqual(n, 200)
        self.assertAlmostEqual(c.calibrated(50), 150)
        self.assertAlmostEqual(c.calibrated(150), 250)
        
    def test_inverse(self):
        c = self.d.controls['T']
        self.d['calibrationT'] = cal
        self.assertAlmostEqual(c.inverse(100), 0)
        self.assertAlmostEqual(c.inverse(150), 50)
        self.assertAlmostEqual(c.inverse(250), 150)
        self.d['calibrationT'] = cal1
        c._inverse_func = 0
        self.assertAlmostEqual(c.inverse(20), 120)
        self.assertAlmostEqual(c.inverse(50), 150)
        self.assertAlmostEqual(c.inverse(150), 250)
        
        self.d['calibrationT'] = cal2
        c._inverse_func = 0
        c._calibration_func = 0
        v = c.calibrated(200)
        self.assertAlmostEqual(c.inverse(v), 200, delta=2e-2)
        
    
if __name__ == "__main__":
    unittest.main()
