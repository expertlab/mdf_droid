#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
from mdf_droid import device
from mdf_droid.tests import reset_test_memory
print('Importing', __name__)


def setUpModule():
    print('Starting', __name__)
    reset_test_memory()
    
class FakeMeasurer(device.Node, device.Measurer):

    """Fake class, inheriting both Measurer and Node"""
    pass


class TestMeasurer(unittest.TestCase):

    def setUp(self):
        self.m = FakeMeasurer(node='measurer')
        self.m.sete('nSamples', {'type': 'Integer'})
        self.m.sete('initializing', {'type': 'Boolean'})
        self.m.sete('running', {'type': 'Boolean'})
        self.m.sete('preset', {'current':'factory_default', 'factory_default':'factory_default', 'type': 'Preset'})
        self.m.sete('smp0', {'type': 'Role'})
        
    def verify(self, n):
        # Check that defined samples has their handles
        for i in range(n):
            h = 'smp%i' % i
            self.assertTrue(h in self.m)
            self.assertTrue(h in self.m.roledev,"Missing {}".format(h))
            self.assertEqual(self.m.roledev[h], (False, False))
        
        s = 'smp%i' % n
        s1 = 'smp%i' % (n + 1)
        self.assertFalse(s in self.m)
        self.assertFalse(s in self.m.roledev)
        self.assertFalse(s1 in self.m)
        self.assertFalse(s1 in self.m.roledev)
        sl = [s for s in self.m.iter_samples()]
        self.assertEqual(sl, [False] * n)

    def test_nsamples(self):
        # After init, no sample is defined
        self.m['nSamples'] = 1
        self.verify(1)
        self.m['nSamples'] = 4
        self.verify(4)
        self.m['nSamples'] = 8
        self.verify(8)
        self.m['nSamples'] = 5
        self.verify(5)


if __name__ == "__main__":
    unittest.main()
