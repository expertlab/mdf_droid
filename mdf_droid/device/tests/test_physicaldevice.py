#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
from mdf_droid import device, tests

print('Importing', __name__)


def setUpModule():
    print('Starting', __name__)
    tests.reset_test_memory()


class TestPhysical(unittest.TestCase):

    def setUp(self):
        self.d = device.Physical(node='physical')

    def test_retry(self):
        self.assertEqual(self.d.retry, self.d['retry'])
        self.d['retry'] += 1
        self.assertEqual(self.d.retry, self.d['retry'])
        print('set prop')
        self.d.retry = 20
        print('done')
        self.assertEqual(self.d.retry, self.d['retry'])
        self.d['retry'] = 8
        self.assertEqual(8, self.d.retry)


class TestUDevice(unittest.TestCase):

    def setUp(self):
        self.d = device.UDevice()

    def test_list_available_devices(self):

        class SubPhysical(device.UDevice):
            dev_pattern = '/dev/tty*'

        v = SubPhysical.list_available_devices()
        self.assertIn('/dev/tty1', list(v.values()))
        self.assertGreater(len(v), 4)


if __name__ == "__main__":
    unittest.main(verbosity=2)
