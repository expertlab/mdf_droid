#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
import os
import threading

from mdf_droid import device
from http.server import SimpleHTTPRequestHandler
from random import randint
import socketserver

print('Importing ', __name__)

PORT = 28127 + randint(0, 1000)


def setUpModule():
    global httpd, server, PORT
    print('Starting ', __name__)
    Handler = SimpleHTTPRequestHandler
    httpd = socketserver.TCPServer(("", 0), Handler)
    ADDRESS, PORT = httpd.server_address
    print('Preparing server', ADDRESS, PORT)
    # A separate server object for handling one request at a time
    server = threading.Thread(target=httpd.handle_request)
    os.environ['http_proxy'] = ''


class TestHTTP(unittest.TestCase):

    def setUp(self):
        self.d = device.HTTP(node='http://127.0.0.1:{}'.format(PORT))

    def test_connect(self):
        global server
        os.environ['http_proxy'] = ''
        self.assertFalse(self.d['isConnected'])
        server.start()
        self.d.connection()
        print('JOINING')
        server.join()
        self.assertTrue(self.d['isConnected'])


if __name__ == "__main__":
    unittest.main()
