#!/usr/bin/python
# -*- coding: utf-8 -*-
"""A device hosting one or more samples and operating on them (typically, for measurement)."""
from mdf_canon.csutil import initializeme


class Measurer(object):

    """A device hosting one or more samples and operating on them (typically, for measurement).
    Lightweight class for defining additional methods."""
    conf_def = [{"handle": 'nSamples', "name": 'Number of samples', "max": 8, "current": 0, "step": 1, "min": 0, "type": 'Integer',
                 "writeLevel":2, "attr": ["Hardware"]},
                {"handle": 'smp0', "name": 'Sample 0 path',
                    "type": 'Role', 'parent': 'nSamples', "writeLevel": 3},
                ]

    @initializeme(repeatable=True)
    def set_nSamples(self, n):
        """Set the number of samples in the current measurement.
        Creates a Role option for each sample."""
        if self.root and self.root['initTest'] + self.root['closingTest']:
            n0 = self['nSamples']
            self.log.debug('Skip samples setup while init/closingTest', n0, n)
            return n0
        if self.root_isRunning:
            self.log.error(
                'Cannot change number of samples while running acquisition.')
            return None
        self.log.debug('Measurer.set_nSamples', n)
        if n > 16:
            n = 16
        preset = self['preset']
        for i in range(n):
            h = 'smp%i' % i
            # Don't overwrite already existing options
            if (h in self.desc):
                self.log.debug('Found sample role', h)
                if h not in self.roledev:
                    self.roledev[h] = (False, False)
                continue
            opt = self.get_from_preset(h, preset, True)
            if not opt:
                self.log.debug('Creating sample Role', h)
                opt = {'name': 'Output Sample %i' % i, 'handle': h,
                   'current': ['None', 'default'],
                   'type': 'Role', 'parent': 'nSamples'}
            self.desc.sete(h, opt)
            self.roledev[h] = (False, False)
        # Remove unnecessary samples
        self.delete_sample_roles_from(n)
#       # Re-init all samples
        self.init_samples()
        self.reset_regions()
        if self['running']:
            self.log.debug('Samples changed: restarting device...')
            self['running'] = False
            self['running'] = True
        return n
    
    def delete_sample_roles_from(self, start):
        self.log.debug('Measurer.delete_sample_roles_from', start)
        for i in range(start, 16):
            h = 'smp%i' % i
            if h in self.desc:
                self.log.debug('Removing sample option', h)
                self.desc.delete(h)
                if h in self.roledev:
                    self.log.debug('Removing sample role', h)
                    del self.roledev[h]
            else:
                break
        self.desc.set('nSamples', start)
    
    @property
    def samples(self):
        return [s for s in self.iter_samples()]

    def iter_samples(self, dsample=False):
        """Generator function returning the configured samples"""
        nSamples = 1
        if dsample is False:
            nSamples = self['nSamples']
        i = 0
        while i < nSamples:
            if dsample is not False:
                yield dsample
                break
            h = 'smp%i' % (i)
            # Retrieve sample object from roledev dictionary mapping
            smp = self.roledev.get(h, (False, False, False))[0]
            # Deleted sample
            if smp and not smp.desc:
                smp = False
            if not smp:
                r = self.map_role_dev(h, force=True)
                if r:
                    smp = r[0]
            
            #  If no sample was defined, return the error
            if smp is False:
                self.log.debug('No sample defined!', i)
#               break
            yield smp
            i += 1

    def iter_samples_get(self, opt):
        """Iterate over samples collecting their option `opt` in a list."""
        r = []
        for smp in self.iter_samples():
            if smp is False:
                continue
            r.append(smp.get(opt))
        return r

    def init_sample(self, sample, handle):
        return True

    def init_samples(self):
        self.log.debug('init_samples')
        n = 0
        for i, smp in enumerate(self.iter_samples()):
            if not smp:
                continue
            h = 'smp%i' % i
            n += self.init_sample(smp, h)
        self.update_superregion()
        return n
    
    def reset_region(self, sample, handle):
        """Initialize `sample` referred by `handle` (smp0, smp1...)"""
        if sample['preset'] != 'factory_default':
            r = sample.get_from_preset('roi', sample['preset'])
            self.log.debug('Restoring roi from preset', sample['roi'], r)
            sample['roi'] = r
            return True
        if 'size' not in self:
            return True
        n = self['nSamples']
        if n == 0:
            self.log.error('reset_region: no samples defined!')
            return False
        # smp0
        sample_number = int(handle[3:])
        # Rotated size
        w, h = self['size']
        pw = float(w) / n
        x = pw * sample_number
        roi = [int(x), 0, int(pw), h]
        sample['roi'] = roi
        self.log.debug('AutoROI', roi)
        return True
    
    def reset_regions(self):
        r = []
        for i, smp in enumerate(self.samples):
            if not smp:
                break
            e = self.reset_region(smp, f'smp{i}')
            r.append(e)
        self.update_superregion()
        return r
    
    def update_superregion(self):
        return True
