#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Main Interfaces"""
from time import sleep
from copy import deepcopy
from twisted.web import xmlrpc
from traceback import format_exc
import serial
import os
from enum import Flag, auto
from mdf_canon.csutil import retry, lockme, time, unicode_encode, unicode_decode
from mdf_canon.mem.process_proxy import ProcessProxy
from ..utils import crc
from .physicaldevice import UDevice

from .. import parameters as params

conf = [
    {"handle": 'baudrate', "name": 'Communication Baudrate',
     "current": 19200, "type": 'Chooser', "options":[19200], "readLevel": 3},
    {"handle": 'bytesize', "name": 'Byte size',
     "current": 8, "type": 'Integer', "readLevel": 3},
    {"handle": 'parity', "name": 'Parity',
     "current": 'N', "type": 'String', "readLevel": 3},
    {"handle": 'stopbits', "name": 'Stop bits',
     "current": 1, "type": 'Integer', "readLevel": 3},
    {"handle": 'rtscts', "name": 'RtsCts', "readLevel": 3,
        "current": 0, "type": 'Boolean', },
    {"handle": 'xonxoff', "name": 'xOnxOff', "readLevel": 3,
     "current": 0, "type": 'Boolean', },
    {"handle": 'exclusive', "name": 'Exclusive', "readLevel": 3,
     "current": 0, "type": 'Boolean', },
    {"handle": 'autoBaudrate', "name": 'Automatically find correct baudrate',
     "current": True, "type": 'Boolean', "readLevel": 3},
    {"handle": 'sent', "name": 'Sent commands', "type": 'Integer', "attr":['History', 'ReadOnly', 'Runtime']},
    {"handle": 'received', "name": 'Received commands', "type": 'Integer', "attr":['History', 'ReadOnly', 'Runtime']},
    {"handle": 'queue', "name": 'Command queue', "type": 'Hidden', "attr":['Runtime'], "current": [], "parent": 'packet'},
    {"handle": 'packet', 'type':'TextArea', 'attr':['ReadOnly', 'Runtime']},
    {"handle": 'packetSent', 'type':'TextArea', 'attr':['ReadOnly', 'Runtime'], "parent": 'packet'},
    {"handle": 'rateLimit', 'type':'Float', "unit": 'hertz'},
    {"handle": 'reconnect', 'type':'Button'},
]


class InvalidSerialMessage(xmlrpc.Fault):

    def __init__(self, msg='InvalidSerialMessage'):
        xmlrpc.Fault.__init__(self, 3883, msg)


class SerialError(xmlrpc.Fault, serial.SerialException):

    def __init__(self, msg='SerialError', code=3884):
        serial.SerialException.__init__(self, msg)
        xmlrpc.Fault.__init__(self, code, msg)


class SerialTimeout(SerialError):

    def __init__(self, msg='SerialTimeout', code=3885):
        SerialError.__init__(self, msg, code)


class SerialPortNotOpen(SerialError):

    def __init__(self, msg='SerialPortNotOpen', code=3886):
        SerialError.__init__(self, msg, code)


class FakeSerial(object):
    """Simulate a serial class"""
    delay = 0
    timeout = 0
    _opened = True
    _outWaiting = 0
    _inWaiting = 0
    
    def __init__(self, **kw):
        self.keywords = kw
        self.output_buffer = b""  # Reply
        self.input_buffer = b""  # Message
        
    @property
    def baudrate(self):
        return self.keywords.get('baudrate', 19200)
    
    def close(self):
        self._opened = False
        
    def open(self):
        self._opened = True
        
    def isOpen(self):
        return self._opened
    
    def write(self, msg):
        sleep(self.delay)
        r = self.command(msg)
        if r:
            self.output_buffer = r
        self.simulate()
        return len(msg)
    
    def command(self, msg):
        return b'empty'
        
    def simulate(self):
        return True
    
    def read(self, n=0):
        cut = True
        if n <= 0:
            n = len(self.output_buffer) - 1
            cut = False
        r = self.output_buffer[:n]
        if cut:
            self.output_buffer = self.output_buffer[n:]
        else:
            self.output_buffer = b""
        return r
    
    def outWaiting(self):
        """Length of the message being sent to FakeSerial"""
        # Instantaneous
        return 0
    
    def inWaiting(self):
        """Lenght of the reply being sent from FakeSerial"""
        return len(self.output_buffer)
    
    def flushInput(self):
        """Flush reply being sent from FakeSerial"""
        self.output_buffer = b""
    
    def flushOutput(self):
        """Flush message being sent to FakeSerial"""
        self.input_buffer = b""
    
    def flush(self):
        self.flushInput()
        self.flushOutput()
    

serial_config = {'bytesize': serial.EIGHTBITS, 'parity': serial.PARITY_NONE,
                'stopbits':1, 'xonxoff':0, 'rtscts':0, 'exclusive': True}


class SerialMessage(Flag):
    OK = 0
    SHORT = auto()  # Will keep trying to read until timeout
    NOCRC = auto()  # Message fails crc - give up
    NOSTART = auto()  # incorrectly started
    NOEND = auto()  # incorrectly ended
    NOLIM = auto()  # incorrectly delimited
    LONG = auto()  # Message is malformed (eg too long) - give up
    TIMEOUT = auto()  # Read retry attempts timed out (set externally)
    
    def errors(self):
        errs = []
        if self.NOCRC in self:
            errs.append('CRC Redundancy Check failed')
        if self.SHORT in self:
            errs.append('Too short to be a reply')
        if self.NOEND in self:
            errs.append('Message is not correctly terminated')
        if self.NOSTART in self:
            errs.append('Message is not correctly started')
        if self.NOLIM in self:
            errs.append('Message is not correctly delimited')
        return errs
    
    def exception(self, red=''):
        msg = ' - '.join(self.errors())
        if red:
            msg += ': ' + repr(red)
        return InvalidSerialMessage(msg)

    @classmethod
    def validate(cls, red, minlen=1, maxlen=1e5,
                 delimiter=False, startstring=False, endstring=False,
                 ncrc=0):
        if len(red) < max(minlen, ncrc):
            return cls.SHORT, red
        # Check startstring
        validation = cls.OK
        if startstring:
            if startstring in red:
                i = red.index(startstring)
                red = red[i:]
            else:
                validation |= cls.NOSTART | cls.SHORT
        # Check endstring
        if endstring:
            if endstring in red:
                i = red.index(endstring)
                red = red[:i + len(endstring)]
            else:
                validation |= cls.NOEND | cls.SHORT
        # Check delimiter
        if delimiter:
            if red.count(delimiter) < 2:
                validation |= cls.NOLIM | cls.SHORT
        # Still valid: check crc
        if validation:
            if ncrc and red[-ncrc:] != crc(red[:-ncrc]):
                validation |= cls.NOCRC
        if len(red) > maxlen:
            validation |= cls.LONG
            validation &= ~cls.SHORT
        return validation, red


class Serial(UDevice):

    """Generic interface for serial devices."""
    conf_def = deepcopy(UDevice.conf_def)
    conf_def += conf
    dev_pattern = '/dev/tty{{USB,ACM}}[0-{}]'.format(params.max_serial_scan)
    baudrates = []
    """List of baudrates available for auto scan"""
    available = {}
    minimum_reply_len = 1
    maximum_reply_length = 1000
    """Standard minimum length for reply messages"""
    cyclic_redundancy_check = 0
    """Perform crc on messages - set to last digits to use"""
    endstring = b''
    """String marking end of reply message"""
    startstring = b''
    endwrite = b''
    delimiter = b''
    UDevice.setProperties(
        'bytesize', 'parity', 'stopbits', 'xonxoff', 'rtscts')
    serial_class = serial.Serial

    def __init__(self, parent=None, node='?s',
                 serial_class=False, **serial_config):
        if serial_class:
            self.serial_class = serial_class
        UDevice.__init__(self, parent=parent, node=node)
        # Redefine here for updated max
        self.com = False
        self.setattr('baudrate', 'options', self.baudrates)
        self['isConnected'] = False
        for k, v in serial_config.items():
            self.setattr(k, 'factory_default', v)
            if self['preset'] == 'factory_default':
                self.setattr(k, 'current', v)

    def connection(self, blacklist=[]):
        """Connect to serial port and validate response"""
        if not super().connection(blacklist=blacklist):
            return False
        v = False
        self['isConnected'] = v
        b = self['baudrate']
        self.setattr('baudrate', 'options', self.baudrates)
        # Ricerca abilitata
        print('AutoSearch:', self['autoBaudrate'], self.baudrates)
        if self['autoBaudrate'] and len(self.baudrates) > 0:
            if b not in self.baudrates:
                b = self.baudrates + [b]
            else:
                b = self.baudrates[:]
            self.log.info('Scanning Baudrates', b, self.__class__.__name__)
            v = self.findBaudrate(b)
        else:
            print('connect_baudrate')
            v = self.connect_baudrate(b)
        print(b, self['autoBaudrate'], self.baudrates)
        # imposto l'opzione e attivo eventuali set_isConnected
        self['isConnected'] = v
        # Se sono connesso, carico le impostazioni di default, etc...
        return v

    xmlrpc_connection = connection

    def connect_baudrate(self, baudrate=False):
        """Connects serial port with a baudrate."""
        if not baudrate:
            baudrate = self['baudrate']
        print('Serial.connect_baudrate', baudrate)
        if self.com is not False:
            try:
                if self.com.isOpen():
                    self.com.close()
                    sleep(.1)
            except:
                self.log.error('Closing former port', self.file, baudrate, format_exc())
            self.com._stop()
        self.desc.set('baudrate', baudrate)
        
        if self.com is False:
            self.com = ProcessProxy(self.serial_class, maxfreq=self['rateLimit'])
            self.com._timeout = 1.
        
        try:
            self.com._start(port=self['dev'], baudrate=baudrate,
                                 bytesize=self['bytesize'],
                                 parity=self['parity'], stopbits=self['stopbits'],
                                 timeout=self.timeout, xonxoff=self['xonxoff'], rtscts=self['rtscts'],
                                 exclusive=self['exclusive'])
            
        except:
            self.com = False
            self.log.error('Error opening serial port:', self['dev'], format_exc())
            return False
                    
        if not self.com.isOpen():
            self.log.debug('Unable to open serial port')
            return False
        # Validate this connection
        if self.validate_connection():
            return True
        # Discard
        return False

    def findBaudrate(self, baudrates):
        """Iterate amongst available baudrates until a connection is successful."""
        for br in baudrates:
            self.log.debug('Try baudrate:', br)
            if self.connect_baudrate(baudrate=br):
                return True
        if self.com:
            self.com.close()
        return False

    def validate_connection(self):
        """Connection validation function. To be re-implemented ad hoc."""
        if not self.com:
            self.log.debug('validate_connection: No Com Port process established')
            return False
        if not self.com._is_alive():
            self.log.debug('validate_connection: Com Port process died')
            return False
        return True

    def get_baudrate(self):
        if not self.com:
            return self.desc.get('baudrate')
        return self.com._get_instance_attribute('baudrate')

    @lockme()
    def raw(self, msg):
        """Debug. Directly write to serial port and try to read reply"""
        if not getattr(self, 'read', False):
            self.log.debug('UnImplemented read function')
            return 'UnImplemented'
        msg = unicode_encode(msg, 'utf8')
        self.com.write(msg)
        self.sleep()
        return self.read()

    xmlrpc_raw = raw

    @UDevice.timeout.setter
    def timeout(self, nval):
        """Propagate timeout to serial port."""
        self.timeout = nval * 1000.
        if self.com:
            self.com._set_instance_attribute('timeout', self.timeout)

    def set_timeout(self, val):
        val = int(val)
        self.timeout = val
        self.log.debug('timeout set to', val, 'ms')
        return val

    def _flush(self):
        self.com.flushInput()
        self.sleep()
        self.com.flushOutput()
        self.sleep()
        self.com.flush()
        self.sleep()

    @lockme()
    def flush(self):
        self._flush()

    xmlrpc_flush = flush
    
    def check_open(self):
        if self.com.isOpen():
            try:
                self.com.flush()
            except:
                self.log.error('Port cannot flush! Assuming was closed in the meanwhile.', format_exc())
            return True
        if os.path.exists(self['dev']):
            self.log.error('Port was not open. Reopening...')
            return self.com.open()
        self.log.error('Port disconnected. Searching a substitute...')
        self.search_new_serial_port()
        
    def get_reconnect(self):
        if self.com:
            self.com._stop()
            del self.com
        self.connect_baudrate()
        
    def search_new_serial_port(self):
        if self.com:
            self.com._stop()
            del self.com
        avail = self.list_available_devices()
        self.log.debug('search_new_serial_port', avail)
        found = 0
        for dp, tty in avail.items():
            if dp == self['devpath']:
                self.log.error('Found device address:', tty, 'instead of:', self['dev'])
                self.file = tty
                found = 1
                break
            else:
                self.log.debug('Skip device address:', tty, 'for', self['dev'])
        if not found:
            self.log.critical('Serial disconnect! Cannot find a new port for serial number', self['devpath'], self['dev'], avail)
            return False
        return self.connect_baudrate()
    
    xmlrpc_search_new_serial_port = search_new_serial_port
    
    def write(self, msg):
        """Write msg to the serial port"""
        self.check_open()
        msg = unicode_encode(msg, 'utf8')
        self['packetSent'] = repr(unicode_decode(msg))
        if self.endwrite and not msg.endswith(self.endwrite):
            msg += self.endwrite
        n = 0
        try:
            n = self.com.write(msg)
        except serial.SerialException:
            if not self.search_new_serial_port():
                self.log.critical('Aborting write: cannot find serial port', self.file)
                self['anerr'] += 1
                return False
        # Written the whole message
        if n == len(msg):
            self['sent'] += 1
            return True
        i = 0
        while self.com.outWaiting() > 0 and i < 5:
            i += 1
            self.sleep()
        if self.com.outWaiting():
            self['anerr'] += 1
            self._flush()
            raise SerialTimeout('Output buffer not empty after write')
        self['sent'] += 1
        return True
        
    def read(self, minlen=-1, timeout=-1, endstring=False, startstring=False, delimiter=False, maxlen=-1):
        """Read from the serial port, at least minlen characters, until timeout seconds passed."""
        if not self.com.isOpen():
            self.com.open()
            self._flush()
            raise SerialPortNotOpen()
        # Get default minlength
        if minlen < 0:
            minlen = self.minimum_reply_len
        if maxlen < 0:
            maxlen = self.maximum_reply_length
        # Get default timeout
        if timeout < 0:
            timeout = self.timeout
        # Get default endstring
        endstring = bytes(endstring) if endstring else self.endstring
        startstring = bytes(startstring) if startstring else self.startstring
        delimiter = bytes(delimiter) if delimiter else self.delimiter
        ncrc = self.cyclic_redundancy_check
        red = b''
        t = time()
        cycles = 0
        wait = 0
        errors = SerialMessage.SHORT
        dt = 0
        while SerialMessage.SHORT in errors and dt < timeout:
            cycles += 1
            w = self.com.inWaiting()
            if w:
                wr = self.com.read(w)
                red += wr
                wait = 0
                errors, extracted = SerialMessage.validate(red, minlen, maxlen,
                                                            delimiter, startstring, endstring,
                                                            ncrc)
                if not errors:
                    red = extracted
                    break
                # print('RED PART', wait, cycles, dt, timeout, w, len(red), repr(red), repr(wr))
            else:
                wait += 1
                if len(red) >= minlen and wait > 3:
                    break
                self.sleep()
            dt = time() - t
        self['packet'] = repr(unicode_decode(red))
        anerr = self['anerr']
        if errors:
            self['anerr'] = anerr + 1
            if dt >= timeout:
                errors |= SerialMessage.TIMEOUT
            self._flush()
            raise errors.exception(red)
        elif anerr:
            self['anerr'] = 0
        self['received'] += 1
        return red

    def _writeread(self, msg=False, **kw):
        """Write `msg` and read the reply."""
        if msg and self.write(msg):
            self.sleep()
            return self.read(**kw)
        return None
        
    @lockme()
    @retry()
    def writeread(self, msg=False, **kw):
        """Locked write `msg` and read the reply."""
        return self._writeread(msg=msg, **kw)

    def close(self):
        if self.com:
            try:
                self.com.close()
            except:
                self.log.warning('While closeing', format_exc())
        return UDevice.close(self)

    def cmd(self, *data):
        return self.write(data[0])

    def write_queue(self, handle="queue"):
        queue = self[handle] or []
        done = set([])
        # Execute from last command
        for cmd in queue[::-1]:
            # Make it extendable
            if isinstance(cmd, (str, bytes)):
                cmd = [cmd]
            # Avoid re-executing commands in same batch
            if cmd[0] in done:
                continue
            self.cmd(*cmd)
            done.add(cmd[0])
        if queue:
            self[handle] = []
        return done

    def enqueue(self, data, handle='queue'):
        q = self[handle]
        q.append(data)
        self[handle] = q
        return len(q)
