# -*- coding: utf-8 -*-
"""Device registry"""
from multiprocessing import Lock
from mdf_canon.mem import DirShelf
from mdf_canon.csutil import lockme
from .. import share
from mdf_droid import parameters as params

conf_def = {
    # devpath:devsrvpath mappings
    'reg': {'handle': 'reg', 'current': {}, 'type': 'Object'},  # Assigned
    'tmp': {'handle': 'tmp', 'current': {}, 'type': 'Object'},  # Reserved
    'saved': {'handle': 'saved', 'current': {}, 'type': 'Object'},  # Reserved
}


def _cvt(path, devsrv):
    """If passed arguments are Devices, retrieve their appropriate `dev` and class name options."""
    if not isinstance(path, str):
        if hasattr(path, '__getitem__'):
            path = path['dev']
        else:
            path = None
    if not isinstance(devsrv, str):
        if hasattr(devsrv, '__name__'):
            devsrv = devsrv.__name__
        else:
            devsrv = None
    return path, devsrv


class DevicePathRegistry(object):

    """Registry object for device paths. Assures that a device will not be opened at the same time by two DeviceServer"""
    _lock = Lock()
    _lockme_error = False

    def __init__(self):
        self.shelf = DirShelf(basedir=params.confdir, viewdir='reg')
        # Set current description
        self.shelf.update(conf_def)
        
    def close(self):
        self.shelf.close()
        
    def __del__(self):
        self.close()

    def __getitem__(self, key):
        return self.shelf.get_current(key)

    def __setitem__(self, key, val):
        self.shelf.set_current(key, val)

    def _unreg(self, path, devsrv, key='reg'):
        """Remove `path` from the list of already assigned paths to registry `key`.
        Returns False only if path is present but cannot be removed because of devsrv mismatch."""
        path, devsrv = _cvt(path, devsrv)
        if None in (path, devsrv):
            return False
        r = self[key]
        if path not in r:
            return True
        if not r[path] == devsrv:
            return False
        del r[path]
        self[key] = r
        return True

    def _free(self, *a):
        """Remove `path` from all registries.
        Returns False if removal fails on any registry."""
        r = self._unreg(*a, key='reg')
        r = r and self._unreg(*a, key='tmp')
        return r

    @lockme()
    def free(self, *a):
        """Locked version of _free()."""
        return self._free(*a)

    def _free_classes(self, classes, key='reg'):
        r = self[key]
        pre = len(r)
        f = list(filter(lambda e: e[1] not in classes, self[key].items()))
        r = {e[0]: e[1] for e in f}
        self[key] = r
        return pre - len(r)

    @lockme()
    def free_all(self, *classes):
        """Locked version of _free_all(), from all registries."""
        classes = set(_cvt('', cls)[1] for cls in classes)
        r = self._free_classes(classes, key='reg')
        r += self._free_classes(classes, key='tmp')
        return r

    @lockme()
    def purge(self, path):
        """Remove device `path` from all registries. Called on Device.close()."""
        path, s = _cvt(path, '')
        if None in (path, s):
            return False
        r = self['reg']
        t = self['tmp']
        if path in r:
            del r[path]
            self['reg'] = r
        if path in t:
            del t[path]
            self['tmp'] = t
        return True

    @lockme()
    def assign(self, path, devsrv, key='reg'):
        """Assign device `path` to `devsrv` path on registry `key`.
        Returns true on success, False if already assigned to another DeviceServer"""
        path, devsrv = _cvt(path, devsrv)
        if None in (path, devsrv):
            return False
        # Try to free the registries
        if not self._free(path, devsrv):
            print('DevicePathRegistry.assign: error, already in use.')
            return False
        r = self[key]
        r[path] = devsrv
        self[key] = r
        return True

    def reserve(self, path, devsrv):
        """Assign `path` and ` devsrv` to temporary registry 'tmp'."""
        return self.assign(path, devsrv, key='tmp')

    @lockme()
    def check_available(self, lst=set(), cls=None):
        """Return unassigned paths in `lst`, removing `failed` paths"""
        assigned = set(self['reg'].keys())
        reserved = set(self['tmp'].keys())
        lst = set(lst)
        
        savedmap = self['saved']
        saved = set([])
        devcls = '' if cls is None else cls.__name__
        if devcls and savedmap and not hasattr(cls, 'enumerated_option'):
            print('SAVEDMAP', savedmap)
            saved = [e for e in savedmap.items()]
            # Take only items of devcls
            saved = list(filter(lambda e: e[1] == devcls, saved))
            # Take devpath
            saved = set(e[0] for e in saved)
            notfound = saved - lst
            new = lst - set(savedmap.keys())
            print('SAVED0', saved)
            saved = saved - assigned - reserved - notfound
            print('NOTFOUND', notfound)
            print('NEW', new)
            print('SAVED OK', saved)
            saved.update(new)
            return saved
        else:
            print('NO SAVED', saved, savedmap, devcls)
        return lst - assigned - reserved


registry = None


def get_registry():
    global registry
    if registry is None:
        registry = DevicePathRegistry()
    return registry


def delete_registry():
    global registry
    if registry is not None:
        registry.shelf.close()
    registry = None
