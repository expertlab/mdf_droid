# -*- coding: utf-8 -*-
"""Data management and formats"""
import types
import os
from copy import deepcopy
import inspect
import functools
from twisted.web import xmlrpc
from time import time

from mdf_canon import logger
from mdf_canon import csutil, option
from mdf_canon.csutil import sanitize, xmlrpcSanitize, func_args, dumps, DEFAULT_PROTOCOL
from ..utils import listDirExt
from .. import parameters as params
from mdf_canon.mem import invalid
from functools import cmp_to_key
####
# EXCEPTIONS


class ReadNotAuthorized(xmlrpc.Fault):

    def __init__(self, msg='ReadNotAuthorized'):
        xmlrpc.Fault.__init__(self, 3880, msg)


class WriteNotAuthorized(xmlrpc.Fault):

    def __init__(self, msg='WriteNotAuthorized'):
        xmlrpc.Fault.__init__(self, 3881, msg)


class NoProperty(xmlrpc.Fault):

    def __init__(self, msg='NoProperty'):
        xmlrpc.Fault.__init__(self, 3890, msg)


class FunctionRoutingError(xmlrpc.Fault):

    def __init__(self, msg='FunctionRoutingError'):
        xmlrpc.Fault.__init__(self, 3891, msg)

####
# UTILITIES


def fill_implicit_args(function, args, connection):
    """Fill connection-related optional function arguments `connection`,
    if target function `function` requires them as parameters and if they are not already listed as `args`.
    Returns False on validation failure.
    Returns function, args, and filled kwargs dict on success.
    connection keywords: readLevel,writeLevel,userName,sessionID,request."""
    # authLevel,userName parameters are set only if the calling function
    # accepts them
    pop = 0
    kwargs = {}  # filled arguments
    vn = func_args(function)
    # Internal call, nothing to do here
    if len(connection) == 0 or len(vn) == 0:
        return function, args, kwargs
    pop = 0
    
    for k, v in connection.items():
        if k in vn:
            kwargs[k] = v
            pop += 1
    # Asks all optional arguments as dict
    if 'connection' in vn:
        kwargs['connection'] = connection
    # Check for maximum number of arguments accepted by function
    maxlen = len(vn) - pop - 1
    if len(args) > maxlen and pop > 0 and maxlen > 0:
        print('fill_implicit_args', maxlen, args, pop)
        return False
    return function, args, kwargs


class SubDict(dict):

    """Subordinated dictionary for Dict-type properties. It allows syntax like:
    parent['pkey']['subkey']=foo, where parent is a ConfigurationInterface object."""

    def __init__(self, ini_dict, parent, parent_key):
        dict.__init__(self, ini_dict)
        self.parent = parent
        self.parent_key = parent_key

    def __setitem__(self, key, val):
        """This will cause each change in the subdict to reflect on the parent dict"""
        dict.__setitem__(self, key, val)
        self.parent.set(self.parent_key, self.copy())


class ConfigurationInterface(xmlrpc.XMLRPC, object):

    """Public interface to Conf objects (XMLRPC and get/set mechanics)"""
    conf_class = 'Conf'
    """Conf class towards which this class acts as a ConfigurationInterface."""
    main_confdir = params.confdir
    """Base server configuration directory"""
    conf_def = [{"handle": 'mro', "name": 'Class hierarchy', "type": 'List', "attr": ['ReadOnly', 'Hidden']},
                {"handle": 'log', "name": 'Log',
                    "type": 'Log', "attr": ['History', 'Runtime']},
                ]
    """Default configuration list"""

    server = None
    allow_none = True
    _Method__name = 'undefined'
    _lockme_error = False
    separator = '/'
    name = None
    
    def __init__(self, desc):
        """Create an interface for configuration object `desc`. """

        xmlrpc.XMLRPC.__init__(self, allowNone=True)
        object.__init__(self)
        self.separator = '/'
        self.devices = []
        self.controls = {}
        self.roledev = {}
        self.immutable = {k:None for k in ('fullpath', 'devpath')}
        self.desc = desc
        self.desc_cache = {}
        self.log = logger.SubLogger(self)
        if hasattr(desc, 'log'):
            self.desc.log = self.log
    
    @classmethod
    def skeleton(cls, obj=False):
        """Compatibility with BaseDocument"""
        m = {}
        entries = sorted(list(cls.conf_def), key=cmp_to_key(option.prop_sorter))
        changes = {}
        for o in entries:
            if 'handle' in o:
                option.ao(m, **o)
            else:
                changes.update(o)
        for k, v in changes.items():
            if k not in m:
                print('Skeleton cannot set default', k, v)
                continue
            m[k]['factory_default'] = v
        return m
        
    def reload_code(self):
        return True
    
    def xmlrpc_reload_code(self):
        return self.reload_code()

    def keys(self, *a, **k): return self.desc.keys(*a, **k)

    def get_preset(self, *a, **k): return self.desc.get_preset(*a, **k)

    def sete(self, *a, **k): return self.desc.sete(*a, **k)
    
    def hasattr(self, *a, **k): return self.desc.hasattr(*a, **k)

    def getattr(self, *a, **k): return self.desc.getattr(*a, **k)

    def setattr(self, *a, **k): return self.desc.setattr(*a, **k)
    
    def getAttributes(self, *a, **k): return self.desc.getAttributes(*a, **k)

    def getkid(self, *a, **k): return self.desc.getkid(*a, **k)

    def gettype(self, *a, **k): return self.desc.gettype(*a, **k)

    def has_key(self, item): return self.__contains__(item)

    def listPresets(self, *a, **k): return self.desc.listPresets(*a, **k)

    def updateCurrent(self, currentDict, t=-1):
        if t < 0:
            t = time()
        for k, v in currentDict.items():
            if k in self.controls:
                currentDict[k] = self.controls[k].set(v, t)
        return self.desc.updateCurrent(currentDict, t)

    def update(self, *a, **k): return self.desc.update(*a, **k)

    def fp(self, *a, **k): return self.desc.fp(*a, **k)

    def iolist(self, *a, **k): return self.desc.iolist(*a, **k)

    def get_from_preset(self, *a, **k): return self.desc.get_from_preset(*a, **k)

    def compare_presets(self, *a, **k): return self.desc.compare_presets(*a, **k)

    def set_to_preset(self, *a, **k): return self.desc.set_to_preset(*a, **k)
    
    def xmlrpc_read_current(self, *a, **k): return self.desc.read_current(*a, **k)

    def xmlrpc_has_key(self, item, **k): return self.__contains__(item)

    def xmlrpc_keys(self, *a, **k): return list(self.desc.keys(*a, **k))

    def xmlrpc_items(self, *a, **k): return list(self.desc.items(*a, **k))

    def xmlrpc_values(self, *a, **k): return list(self.desc.values(*a, **k))
    
    def xmlrpc_hasattr(self, *a, **k): return self.hasattr(*a, **k)

    def xmlrpc_getattr(self, *a, **k): return self.getattr(*a, **k)

    def xmlrpc_setattr(self, *a, **k): return self.setattr(*a, **k)

    def xmlrpc_getkid(self, *a, **k): return xmlrpcSanitize(self.desc.getkid)(*a, **k)

    def xmlrpc_applyDesc(self, *a, **k): return self.applyDesc(*a, **k)

    def xmlrpc_listPresets(self, *a, **k): return self.desc.listPresets(*a, **k)

    def save(self, *a, **k): return self.desc.save(*a, **k)

    def xmlrpc_save(self, *a, **k): return self.desc.save(*a, **k)

    def remove(self, *a, **k): return self.desc.remove(*a, **k)

    def xmlrpc_remove(self, *a, **k): return self.desc.remove(*a, **k)

    def rename(self, *a, **k): return self.desc.rename(*a, **k)

    def xmlrpc_rename(self, *a, **k): return self.desc.rename(*a, **k)

    def delete(self, *a, **k):
        self.desc_cache.pop(a[0], 0) 
        return self.desc.delete(*a, **k)

    def xmlrpc_delete(self, *a, **k): return self.desc.delete  # key(*a, **k)

    def xmlrpc_getAttributes(self, *a, **k): return xmlrpcSanitize(self.getAttributes)(*a, **k)

    def xmlrpc_iolist(self, *a, **k): return self.iolist(*a, **k)

    def xmlrpc_get_from_preset(self, *a, **k): return self.get_from_preset(*a, **k)

    def xmlrpc_compare_presets(self, *a, **k): return self.compare_presets(*a, **k)

    def xmlrpc_set_to_preset(self, *a, **k): return self.set_to_preset(*a, **k)

    def xmlrpc___getitem__(self, *a, **k): return xmlrpcSanitize(self.xmlrpc_get)(*a, **k)

    def xmlrpc___setitem__(self, *a, **k): return xmlrpcSanitize(self.xmlrpc_set)(*a, **k)

    def xmlrpc_setFlags(self, *a, **k): return xmlrpcSanitize(self.setFlags)(*a, **k)

    def xmlrpc_getFlags(self, *a, **k): return xmlrpcSanitize(self.getFlags)(*a, **k)

    def xmlrpc___contains__(self, item): return self.__contains__(item)

    def xmlrpc___hash__(self, *a, **k): return self.__hash__(*a, **k)

    def xmlrpc___eq__(self, *a, **k): return self.__eq__(*a, **k)

    def xmlrpc___repr__(self, *a, **k): return self.desc.__repr__(*a, **k)

    def xmlrpc___str__(self, *a, **k): return self.__str__(*a, **k)
    
    # History management
    
    def h_get(self, *a, **k): return self.desc.h_get(*a, **k)
    
    def h_get_time_value(self, *a, **k): return self.desc.h_get_time_value(*a, **k)

    def h_get_history(self, *a, **k): return self.desc.h_get_history(*a, **k)
    
    def h_get_time(self, *a, **k): return self.desc.h_get_time(*a, **k)
    
    def h_time_at(self, *a, **k): return self.desc.h_time_at(*a, **k)
    
    def h_clear(self, *a, **k): return self.desc.h_clear(*a, **k)
    
    def xmlrpc_h_get(self, *a, **k): return xmlrpcSanitize(self.h_get)(*a, **k)

    def xmlrpc_h_get_time(self, *a, **k): return xmlrpcSanitize(self.h_get_time)(*a, **k)
    
    def xmlrpc_h_get_time_value(self, *a, **k): return xmlrpcSanitize(self.h_get_time_value)(*a, **k)
    
    def xmlrpc_h_get_history(self, *a, **k): return xmlrpcSanitize(self.h_get_history)(*a, **k)

    def xmlrpc_h_clear(self, *a, **k): return xmlrpcSanitize(self.h_clear)(*a, **k)
    
    def xmlrpc_h_time_at(self, *a, **k): return self.h_time_at(*a, **k)

    # Must explicitly define these functions.
    # Cannot assign them to self during __init__
    def __getitem__(self, *args, **kwargs):
        return self.get(*args, **kwargs)

    def __setitem__(self, *args, **kwargs):
        return self.set(*args, **kwargs)

    def __contains__(self, item):
        return item in self.desc

    def add_option(self, handle, *a, **k):
        k['kid'] = self['fullpath'] + handle
        return self.desc.add_option(handle, *a, **k)

    def close(self):
        print('ConfigurationInterface.close', type(self), id(self))
        self.desc_cache = {}
        self.devices = []
        self.controls = {}
        self.roledev = {}
        if self.desc is False:
            return False
        self.desc.close()
        self.desc = False
        return True

    @property
    def class_name(self):
        return self.__class__.__name__

    def classname(self):
        return self.class_name

    xmlrpc_classname = classname

    def mro(self):
        mro = inspect.getmro(self.__class__)
        r = []
        for cl in mro:
            r.append(cl.__name__)
        return r[:-3]

    xmlrpc_mro = mro

    def describe(self, *a, **kw):
        if kw.pop('cached', 0):
            return deepcopy(self.desc_cache)
        self['mro'] = self.mro()
        ret = self.desc.describe(*a, **kw)
        self.desc_cache.update(ret)
        return ret

    def get_mro(self):
        return self.mro()

    def __iter__(self): pass

    def __hash__(self):
        """L'hash viene calcolato sulla base dell'oggetto self.log onde evitare ricorsioni infinite."""
        return hash(self.log)

    def __eq__(self, other):
        if not getattr(other, '_Method__name', False):
            return False
        if self._Method__name != other._Method__name:
            return False
        return True

    def __str__(self):
        if self.desc is False:
            return 'Closed {}: {}, {}, {}'.format(self.__class__.__name__, repr(self), type(self), id(self))
        r = self.__class__.__name__ + ' for ' + self.desc.__str__()
        r += '\nconf_dir: %s \nconf_obj: %s' % (self.conf_dir, self.conf_obj)
        return r

    def xmlrpc_describe(self, readLevel=0):
        """Sanitize description dictionary and filter depending on user's readLevel."""
        r = self.desc.describe()
        for key, val in list(r.items()):
            if val['readLevel'] > readLevel:
                del r[key]
                continue
            val['current'] = xmlrpcSanitize(
                val['current'], attr=val['attr'], otype=val['type'])
#           if 'Binary' in val['attr'] or val['type']=='Profile':
#               val['current']=xmlrpc.Binary(dumps(val['current']))
            r[key] = val
        l = [v.keys() for v in r.values()]
        return r

    _rmodel = False

    def rmodel(self):
        """Dictionary model recursively listing all subdevices' paths.
        {'self':name,
         'sub1':{'self':name,
                 'sub1sub1':{'self':name,...},
         'sub2':{'self':name}
          ...}
        """
        if self._rmodel is not False:
            return self._rmodel
        out = {'self': self['name']}
        for name, path in self.list():
            d = self.getSubHandler(path)
            if d is self:
                print('Skipping myself', name)
                continue
            if d is None:
                print('skipping NONE', name, path)
                continue
            out[path] = d.rmodel()
        return out

    xmlrpc_rmodel = rmodel

    @classmethod
    def _pget(cls, key, self):
        """Helper function for getting a class-defined property"""
        # print 'PGET', type(self), key # this works!
        return self.get(key)

    @classmethod
    def _pset(cls, key, self, val):
        """Helper function for setting a class-defined property"""
        # FIXME: pset does not work!
        # print 'PSET', type(self), key, val
        return self.set(key, val)

    @classmethod
    def setProperties(cls, *keys):
        """Contructs class properties corresponding to Conf options."""
        for key in keys:
            if hasattr(cls, key):
                v = getattr(cls, key)
                print('Property {} is overwriting previous attribute {}'.format(key, repr(v)))
                del v
            pget = functools.partial(cls._pget, key)
            pset = functools.partial(cls._pset, key)
            p = property(pget, pset)
            setattr(cls, key, p)

    def set_preset(self, *args, **kwargs):
        """Calls applyDesc after setting the preset"""
        r = self.desc.set_preset(*args, **kwargs)
        self.log.debug('set_preset', args, kwargs, r)
        if r:
            self.applyDesc()
        return r

    def applyDesc(self, *a, **k):
        """To be reimplemented."""
        return True

    def validate_preset_name(self, name):
        lst = self.listPresets()
        ret = select_preset_for_name(name, self.listPresets())
        self.log.debug('validate_preset_name', name, ret, lst)
        return ret

    def setAttributes(self, name, attrlist, writeLevel=5):
        return self.desc.setAttributes(name, attrlist)

    xmlrpc_setAttributes = setAttributes
    
    def add_attr(self, opt, attr_name, writeLevel=5):
        return self.desc.add_attr(opt, attr_name)

    xmlrpc_add_attr = add_attr
    
    def del_attr(self, opt, attr_name, writeLevel=5):
        return self.desc.del_attr(opt, attr_name)

    xmplrpc_del_attr = del_attr

    def getFlags(self, opt):
        """Returns option flags for `opt`"""
        if opt in self.controls:
            out = {}
            pre = self.desc.getFlags(opt)
            for key, val in pre.items():
                r = self.controls[opt].getFlag(key)
                print('control getflag', key, r)
                if r:
                    out[key] = val
                else:
                    out[key] = pre[key]
            self.desc.setFlags(opt, out)
        return self.desc.getFlags(opt)

    def file_list(self, opt, ext=''):
        """Update a FileList options attribute"""
        odir = os.path.join(self.desc.getConf_dir(), opt, '')
        r = listDirExt(odir, ext, create=True)
        self.setattr(opt, 'options', r)
        return r

    def get(self, name, *opt):
        """Get routing.
        First search for a get_`name` method to call,
        if not, search for a special control,
        if not, search for a RoleIO,
        if not, directly retrieve the value from memory"""
        #################################
        # Check immutability and existance
        val = self.immutable.get(name, None)
        if val is not None:
            return val
        if name not in self.desc:
            if len(opt) > 0:
                return opt[0]
            self.log.warning('No property: ', name)
            raise NoProperty('NoProperty: ' + name)
        val = 'no-function-nor-control-defined'
        #################################
        # Call getter, prio: getter function, Control, RoleIO
        func = getattr(self, 'get_' + name, False)
        if func:
            val = func()
        # Call special controls
        elif name in self.controls:
            val = self.controls[name].get()
            # if val != self.desc.get(name):
            #    self.desc.set(name, val)
        elif name in self.roledev:
            val = self.get_role_io(name, val)
        # Get a SubDict
        if isinstance(val, dict):
            return SubDict(val, self, name)
        # None will cause to retrieve former memory value
        if val is None:
            return self.desc.get(name)
        elif val != 'no-function-nor-control-defined':
            # If intercepted by any function/control, return here
            # They should take care about managing special types (Dict, FileList,
            # RoleIO)
            self.desc.set(name, val)
            return val
        #################################
        # Read from memory and manage special types
        prop = self.desc.gete(name)
        val = prop['current']
        typ = prop.get('type', '')
        # Manage Dict-type options
        if isinstance(prop, bool) or isinstance(prop, str):
            self.log.error(
                'Wrong type for handle', name, type(prop), repr(prop))
        if typ == 'Meta':
            val = SubDict(val, self, name)
        # Update file listings on get()
        elif typ == 'FileList':
            self.file_list(name)
        # Redefine and resolve RoleIO if previously missing
        elif typ == 'RoleIO':
            val = self.get_role_io(name, val)
        return val
    
    def get_role_io(self, name, val=None):
        """Retrieve `name` RoleIO option from the referred option value"""
        obj = self.roledev.get(name, False)
        # Try to remap
        if not obj:
            obj = self.map_role_dev(name)
        if obj:
            obj, pre, io = obj
            if io and obj:
                val = obj[io.handle]
                self.desc.set(name, val)
        else:
            val = self.desc.get(name) 
        return val 

    def gete(self, opt, *a, **k): 
        if k.pop('cached', 0) and opt in self.desc_cache:
            return self.desc_cache[opt]
        r = self.desc.gete(opt, *a, **k)
        # Refresh file listing
        if r.get('type', '') == 'FileList':
            self.file_list(opt)
            r = self.desc.gete(opt, *a, **k)
        self.desc_cache[opt] = r
        return r

    @property
    def root_obj(self):
        """Dummy root obj"""
        return self

    def xmlrpc_get(self, name, readLevel=0):
        """Client frontend for the `get()` method.
        Security check with `readLevel`.
        Only read values from memory if no acquisition isRunning.
        Pickle or otherwise xmlrpc-sanitize values for network transmission"""
        p = self.desc.gete(name)
        r = p.get('readLevel', -1)
        if readLevel < r:
            self.log.critical('Not authorized get', name)
            raise ReadNotAuthorized(
                'Option: %s Required: %i Level: %i' % (name, r, readLevel))
        r = 'need-to-call-get()'
        if 'ParallelForbidden' in p['attr'] or 'ParallelWriteonly' in p['attr']:
            r = self.desc.get(name)
        elif 'Hot' in p['attr']:
            if self.root_obj.get('isRunning'):
                r = self.desc.get(name)
        if r == 'need-to-call-get()':
            r = self.get(name)
        if p['type'] == 'Image':
            return xmlrpc.Binary(r)
        elif p['type'] in ['Binary', 'Profile']:
            return xmlrpc.Binary(dumps(r, DEFAULT_PROTOCOL))
        elif p['type'] == 'Meta':
            return r.copy()
        else:
            return csutil.xmlrpcSanitize(r)

    def multiget(self, opts, readLevel=0):
        """Performs get operation on a list of options, returning a {opt:val} mapping"""
        r = {}
        for opt in opts:
            r[opt] = self.xmlrpc_get(opt, readLevel=readLevel)
        return r

    def xmlrpc_multiget(self, *a, readLevel=0, **k):
        return xmlrpcSanitize(self.multiget(*a, readLevel=0, **k))

    @sanitize()
    def xmlrpc_geth(self, name, readLevel=0):
        dn = self.desc.gete(name)
        rl = dn['readLevel']
        if rl > readLevel:
            self.log.critical('Not authorized geth', name)
            raise ReadNotAuthorized(
                'Option: %s, Required: %i, Level: %i' % (name, rl, readLevel))
        attr = dn.get('attr', [])
        if 'History' in attr and getattr(self.desc, 'history', False):
            return self.desc.h_get_history(name)
        return 'No history for property: ' + name

    @sanitize()
    def xmlrpc_set(self, name, val, connection={}):
        writeLevel = connection.get('writeLevel', 0)
        required = self.desc.getattr(name, 'writeLevel')
        if writeLevel < required:
            self.log.critical('Not authorized set', name)
            raise WriteNotAuthorized(
                'Option: %s, Required: %i, Level: %i' % (name, required, writeLevel))
        return self.set(name, val, connection=connection)

    def setFlags(self, opt, flags):
        """Set flags for option `opt`"""
        if opt in self.controls:
            out = {}
            pre = self.desc.getFlags(opt)
            for key, val in flags.items():
                r = self.controls[opt].setFlag(key, val)
                if r:
                    out[key] = val
                else:
                    out[key] = pre[key]
            flags = out
        return self.desc.setFlags(opt, flags)

    def map_role_dev(self, new=None, force=False):
        return False, False, False
    
    def set(self, name, val, t=-1, connection={}):
        """Set routing.
        First searches for a set_`name` method to call and executes it, updating the value.
        Then searches for a control `name` and executes control.set(value), updating the value.
        Then sets the value to the referred RoleIO and executes io.set(value), updating the value.
        Finally directly set the resulting value on memory (self.desc).
        Returns the final value set.
        Any None interrupts the sequence.
        """
        if not self.desc:
            print('No desc interface object', self, self.desc)
            return None
        if name in self.immutable:
            return self.immutable[name]
        connection['t'] = t
        dn = self.gete(name, cached=True)
        oval = self.desc.get(name)  # why not dn['current']?
        if oval == invalid:
            oval = None
        a = dn['attr']
        if 'ParallelForbidden' in a or 'ParallelReadonly' in a:
            return oval
        typ = dn['type']
        role, io = None, None
        # Dict-like management
        if typ == 'Meta':
            # Obtain a pure-dict object which must be picklable
            val = val.copy()
        # Role management
        elif typ == 'Role':
            if isinstance(val, str):
                val = val.split(',')
                if len(val) == 1:
                    val.append('default')
            if not (isinstance(val, list) or isinstance(val, tuple)):
                # Convert object to role list: [fullpath,preset]
                val = [val['fullpath'], val['preset']]
        # Resolve RoleIO
        # FIXME: security breach: could write to a protected opt as access
        # levels are not checked here
        elif typ == 'RoleIO':
            role = self.roledev.get(name, False)
            # Try to remap
            if not role:
                role = self.map_role_dev(name)
            if role:
                role, pre, io = role
        ### SETTER HOOK ###
        # Setter hook
        # Search for a setter function and call it
        func = getattr(self, 'set_' + name, False)
        if func and type(func) != types.MethodType:
            func = False
        # If a valid function was found, fill implicit arguments if present
        if func:
            r = fill_implicit_args(func, (val,), connection)
            if not r:
                raise FunctionRoutingError('Implicit function consistency error ' + name)
            # Pass also filled kwargs to func
            val = func(val, **r[-1])
        if val is None:
            self.log.debug('Failed setting', name)
            return oval
        ### CONTROL HOOK ###
        # Search if a control object has been defined in the self.controls
        # dict.
        if name in self.controls:
            func = self.controls[name].set
            r = fill_implicit_args(func, (val,), connection)
            if not r:
                raise FunctionRoutingError('Implicit function consistency error ' + name)
            # Pass also filled kwargs to func
            val = func(val, **r[-1])
        if val is None:
            self.log.debug('Failed setting', name)
            return oval
        ############
        # Send the output of func/control to RoleIO
        if io and role:
            val = role.set(io.handle, val)
            if val is None:
                self.log.debug('Failed setting routed to role', name)
                return oval
        # Record Event only if there is an actual change
        if 'Event' in dn.get('attr', []) and val == oval:
            return val
        # SET FINAL VALUE IN MEMORY
        self.desc.set(name, val, t)
        # At the end, intercept Role mapping requests
        if typ == 'Role':
            r = self.map_role_dev(name, val)
            # Restoring old value
            if r is False:
                self.desc.set(name, oval)
        return val

    @sanitize()
    def xmlrpc_gete(self, name, readLevel=0):
        r = self.desc.gete(name).entry.copy()
        if readLevel < r['readLevel']:
            self.log.critical('Not authorized gete', name)
            raise ReadNotAuthorized(
                'Option: %s Required: %i Level: %i' % (name, r['readLevel'], readLevel))
        r['current'] = self.xmlrpc_get(name, readLevel=readLevel)
        return r

    def xmlrpc_sete(self, name, opt, writeLevel=0):
        if writeLevel < 4:
            self.log.critical('Not authorized sete', name)
            raise WriteNotAuthorized(
                'Option: %s Required: %i Level: %i' % (name, 4, writeLevel))
        return self.sete(name, opt)

    def setConf_dir(self, cd):
        """Set the folder where the configuration should be saved."""
        self.desc.setConf_dir(cd)

    def getConf_dir(self):
        """Return the folder where the configuration should be saved."""
        return self.desc.getConf_dir()

    conf_dir = property(getConf_dir, setConf_dir)

    def setConf_obj(self, obj):
        """Set the output full path for current configuration."""
        self.desc.setConf_obj(obj)

    def getConf_obj(self):
        """Return the output full path for current configuration."""
        return self.desc.getConf_obj()

    conf_obj = property(getConf_obj, setConf_obj)

    @sanitize()
    def echo(self, s='none', readLevel=0, writeLevel=0, userName=''):
        """Login demo function."""
        l = ['guest', 'analyst', 'user', 'tech', 'maint', 'admin']
        r = 'Welcome {}. \nYour role is: read={},{} / write={},{}.\nHere is your echo: {}'.format(
            userName, readLevel, l[readLevel], writeLevel, l[writeLevel], s)
        return r

    xmlrpc_echo = echo

    def check_read(self, opt, readLevel=0):
        """Check if option `opt` can be red by current user"""
        return self.getattr(opt, 'readLevel') <= readLevel

    xmlrpc_check_read = check_read

    def check_write(self, opt, writeLevel=0):
        """Check if option `opt` can be written by current user"""
        return self.getattr(opt, 'writeLevel') <= writeLevel

    xmlrpc_check_write = check_write


def select_preset_for_name(name, available_presets):
    presets = list(filter(lambda preset: preset in available_presets,
                     presets_from_name(name)))
    selected_preset = (presets or ['default'])[0]

    if selected_preset not in available_presets:
        selected_preset = 'factory_default'

    return selected_preset


def presets_from_name(name):
    if not name:
        return []
    underscore_indexes = [i for i, ch in enumerate(name) if ch == '_']
    presets = [name]

    for underscore_index in underscore_indexes:
        presets.append(name[underscore_index + 1:])

    return presets
