# -*- coding: utf-8 -*-
"""MDF Server configuration"""
from mdf_canon.option import ao
import os
isWindows = os.name == 'nt'

plugins = """
mdf_droid.users.Users
mdf_droid.storage.Storage
mdf_droid.support.Support
mdf_server.license.License
#GROUP
mdf_server.beholder.beholder.Beholder
mdf_server.motion.Motion
mdf_server.thermal.Thermal
mdf_server.board.Board
#GROUP
mdf_server.furnace.Furnace
mdf_server.microscope.Microscope
mdf_server.dilatometer.Dilatometer
mdf_server.dilatometer.Vertical
mdf_server.bending.Bending
mdf_server.bending.TripleBending
mdf_server.dta.DifferentialThermalAnalysis
"""

if isWindows:
    plugins = """
mdf_droid.users.Users
mdf_droid.storage.Storage
mdf_droid.support.Support
#GROUP
mdf_server.motion.Motion
mdf_server.thermal.Thermal
#GROUP
mdf_server.furnace.Furnace
"""

notify = []
ao(notify, 't', 'DateTime', name='Time')
ao(notify, 'notify', 'String', name='Event')
ao(notify, 'title', 'String', name='Title')
ao(notify, 'message', 'String', name='Message')
ao(notify, 'data', 'String', name='Data', attr=['json', 'Hidden'])

conf = [
    # Main (Status)
    {"handle": 'Main', "name": 'Status',
     "current": '', "type": 'Section',
     },
    {"handle": 'lastClientAccessTime', "name": 'Last access time of client',
        "current": 0, "type": 'Hidden', "attr": ['Runtime'],
    },
    {"handle": 'autoShutdownInterval', "name": 'Shutdown after inactivity',
        "current": 0, "type": 'Chooser',
        "options":['Never', '5 min', '1 hour', '4 hours', '12 hours', '1 day', '3 days'],
        "values": [ 0, 300, 3600, 4 * 3600, 12 * 3600, 24 * 3600, 3 * 24 * 3600],
    },
    {"handle": 'inactive', "name": 'Server is currently inactive',
        "current": False, "type": 'Boolean', "attr": ['Runtime', 'Hidden'],
    },
    {"handle": 'summarize', "name": 'Summarize realtime plot',
        "current": True, "type": 'Boolean'},
    # scanning (Scanning for devices)
    {"handle": 'scanning', "name": 'Scanning for devices',
        "current": False, "type": 'Boolean', "attr": ['ReadOnly', 'Runtime']	},
    # isRunning (Running test)
    {"handle": 'isRunning', "name": 'Running test', "color":-1,
        "current": False, "type": 'Boolean', "attr": ['ReadOnly', 'Runtime']},
    # runningInstrument (Running Instrument)
    {"handle": 'runningInstrument', "name": 'Running Instrument', "current": '',
        "attr": ['ReadOnly', 'Runtime'], "type": 'Chooser',
     },
    {"handle": 'lastInstrument', "name": 'Last initialized instrument', "current": '',
        "attr": ['ReadOnly', 'Runtime'], "type": 'Chooser',
     },
    {"handle": 'restartOnNextCheck', "name": 'Restart server at next periodic check', "current": False,
        "attr": ['ReadOnly', 'Runtime'], "type": 'Boolean',
     },
    {"handle": 'restartOnFinishedTest', "name": 'Restart server at each finished test',
        "current": False, "type": 'Boolean',
     },
    {"handle": 'delayStart', "name": 'Repeat delayed start', "writeLevel": 1,
        "type": 'Integer', "current": 0, "attr": ['Runtime']},
    {"handle": 'delayT', "name": 'Delayed temperature', "unit": 'celsius',
        'current':-1, "min":-1, "max": 1200,
        "writeLevel": 1, "type": 'Float', "attr": ['Runtime']},
    {"handle": 'sequenceIndex', "name": 'Sequence index',
        'current': 0, "type": 'Integer', "attr": ['ReadOnly']},
    {"handle": 'timeDelta', "name": 'Hardware clock time delta (UTC)', 'unit': 'second',
        "attr": ['Runtime'], "type": 'Float'},
    # error (Errors)
    {"handle": 'error', "name": 'Errors',
     "attr": ['ReadOnly'], "type": 'String'},
    # zeroTime (Acquisition start time)
    {"handle": 'zerotime', "name": 'Acquisition start time',
        "attr": ['ReadOnly'], "type": 'Float'},
    # TODO: endtime
    # {"handle": 'endtime',"name": 'Acquisition end time',
    # 	"attr": ['ReadOnly'], "readLevel": 5,"type": 'Float'},
    {"handle": 'initInstrument',
        "name": 'Initializing instrument',
        "current": 0,
        "type": 'Progress',
        "attr": ['Runtime'],
     },
    {"handle": 'initTest',
        "name": 'Initializing New Test',
        "current": 0,
        "type": 'Progress',
        "attr": ['Runtime'],
     },
    {"handle": 'closingTest',
        "name": 'Closing the current test',
        "current": 0,
        "type": 'Progress',
        "attr": ['Runtime'],
     },
    {"handle": 'progress',
        "attr": ['ReadOnly', 'Runtime'],
        "name": 'Operation in progress',
        "type": 'List',
     },
    {"handle": 'endStatus', "name": 'End status',
        "current": '', "writeLevel": 5, "type": 'TextArea'},
    # instruments (List of available instruments)
    {"handle": 'instruments',
        "attr": ['ReadOnly', 'Runtime'],
        "name": 'List of available instruments',
        "current": [], "attr":['Hidden'], "type": 'List',
     },
    # deviceservers (List of available device servers)
    {"handle": 'deviceservers',
        "attr": ['ReadOnly', 'Hidden', 'Runtime'],
        "name": 'List of available device servers',
        "current": [], "type": 'List',
     },
    {"handle": 'registry', "writeLevel": 5,
        "name": 'Device registry',
        "current": "", "type": 'TextArea',
     },
    # Server-side logging
    {"handle": 'log', "name": 'Logging', "type": 'Section'},
    {"handle": 'log_level', "name": 'Log level', "type": 'Integer', "min":0, "max":50, "section":'log'},
    {"handle": 'log_notify', "name": 'Notifications', "section":'log', "type": 'Table', "current":[],
     "header": notify, "attr":['ReadOnly', 'Runtime']},

    # eq (Equipment Identification)
    {"handle": 'eq', "name": 'Equipment Identification', "type": 'Section'},
    # eq_sn (Serial Number)
    {"handle": 'eq_sn', "name": 'Serial Number',
        "current": '12345', "writeLevel": 5, "type": 'String'},
    {"handle": 'eq_mac', "name": 'External MAC address',
        "current": '', "writeLevel": 6, "type": 'String'},
    # Plugin list
    {"handle": 'eq_plugin', "name": 'Load Plugins', 'writeLevel': 5,
     "current": plugins, "type": 'TextArea'},
    # eq_kiln (Enable KILN)
    {"handle": 'eq_kiln', "name": 'Enable KILN', 'writeLevel': 5,
        "current": True, "type": 'Hidden',
     },
    # eq_hsm (Enable HSM Heating Microscope)
    {"handle": 'eq_hsm', "name": 'Microscope',
     "current": 1, "type": 'Boolean', 'writeLevel': 5, },
    {"handle": 'eq_horizontal', "name": 'Optical Dilatometer',
     "current": True, "type": 'Boolean', 'writeLevel': 5, },
    {"handle": 'eq_vertical', "name": 'Vertical Dilatometer',
     "current": False, "type": 'Boolean', 'writeLevel': 5, },
    {"handle": 'eq_flex', "name": 'Optical Bending analyzer',
        "current": 1, "type": 'Boolean', 'writeLevel': 5, },
    {"handle": 'eq_flex3', "name": 'Triple Bending analyzer',
        "current": 1, "type": 'Boolean', 'writeLevel': 5, },
    {"handle": 'eq_dta', "name": 'DTA',
        "current": 0, "type": 'Boolean', 'writeLevel': 5, },
    {"handle": 'eq_motion', "name": 'Has motion control',
     "current": True, "type": 'Boolean', 'writeLevel': 5, },
    {"handle": 'eq_serialPorts', "name": 'Max COM serial ports to be scanned',
     "current": 0, "max": 10, "step": 1, "min": 0, "type": 'Integer', 'writeLevel': 5, },
    
    # cs (Customer Information)
    {"handle": 'cs', "name": 'Customer Information', "type": 'Section'},
    # cs_org (Organization)
    {"handle": 'cs_org', "name": 'Organization',
     "current": 'Expert Lab Service', "type": 'String'},
    # cs_dept (Department)
    {"handle": 'cs_dept', "name": 'Department',
        "current": '', "type": 'String'},
    # cs_lab (Laboratory)
    {"handle": 'cs_lab', "name": 'Laboratory',
        "current": '', "type": 'String'},
    # cs_addr (Complete Address)
    {"handle": 'cs_addr', "name": 'Complete Address',
        "current": 'viale Virgilio 58/L, 41123 Modena (MO), Italy', "type": 'String'},
    # cs_cname (Contact Name)
    {"handle": 'cs_cname', "name": 'Contact Name',
        "current": 'Expert Lab Service', "type": 'String'},
    # cs_qual (Contact Qualification)
    {"handle": 'cs_qual', "name": 'Contact Qualification',
        "current": '', "type": 'String'},
    # email (eMail List)
    {"handle": 'email', 	"name": 'eMail List',
        "current": 'info@expertlabservice.it', "type": 'String'},
    # cs_phone (Phone Number)
    {"handle": 'cs_phone', "name": 'Phone Number',
        "current": '+39 059 8860020', "type": 'String'},

]
