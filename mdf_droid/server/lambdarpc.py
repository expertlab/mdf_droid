# -*- coding: utf-8 -*-
"""Streaming resource for options"""
from time import sleep
import json
import os
from twisted.web import resource, static,  server
from twisted.internet import interfaces
from twisted.internet import reactor
from twisted.internet.task import deferLater, cooperate, NotPaused
from PIL import Image
from io import BytesIO, StringIO
from .stream import ImageProducer, OptionProducer

import logging as pylogging
pylogging.getLogger("PIL").setLevel(pylogging.CRITICAL)

try:
    from zope.interface import implements, implementer
except:
    from zope.interface import classImplements, implementer
    implements = classImplements


class LambdaRPC(resource.Resource):

    """Renders server object tree as a tabular folder of files. Allows lambda-style calls."""

    def __init__(self, obj, node='lambda'):
        resource.Resource.__init__(self)
        self.helper = static.DirectoryLister('')
        t = self.helper.template
        t = t.replace('Content encoding', 'Value').replace(
            'Filename', 'Name').replace('Size', 'Handle')
        self.helper.template = t
        self.obj = obj
        self.node = node

    def getChild(self, path, request):
        if path:
            # Remove /RPC/stream or /stream prefixes
            if path.startswith(b'/RPC'):
                path = path.lstrip('b/RPC')
            if path.startswith(b'/stream'):
                path = path.lstrip(b'/stream')
            if path.startswith(b'/lambda'):
                path = path.lstrip(b'/lambda')
            sub = self.obj.child(path.decode('utf-8'))
            if not sub:
                return resource.NoResource("No such child resource: "+str(sub))
            return LambdaRPC(sub)
        else:
            return self
    
    def check_POST(self, request):
        request.content.seek(0)
        body = request.content.read()
        print('check_POST', body)
        args = json.loads(body)
        cmd = 'xmlrpc_'+args.pop('cmd',False)
        if not cmd or not hasattr(self.obj, cmd):
            print('Function not recognized for object', cmd, self.obj['fullpath'])
        if not cmd:
            return False
        kwargs = args.pop('**', {})
        kwargs.update(args)
        args = kwargs.pop('*', [])
        return cmd, args, kwargs
                

    def render_POST(self, request):
        """Allows lambda-like calls"""
        request.setHeader(b"Content-Type", b"application/json; charset=utf-8")
        r = self.check_POST(request)
        if r is False:
            return 'Invalid POST request'.encode('utf8')
        cmd, args, kwargs = r
        functionPath = cmd.lstrip('/').split('/')
        reply = getattr(self.obj, cmd)(*args, **kwargs)
        return json.dumps(reply).encode('utf8')
    
    def _producer_failed_errback(self, producer):
        producer.stopProducing()

    def render_GET(self, request):
        opt = request.args.get(b'opt', [False])[0]
        if opt:
            opt = opt.decode('utf8')
            interval = float(request.args.get(b'interval', [b'0'])[0].decode('utf8'))
            typ = self.obj.gete(opt)['type']
            if typ == 'Image':
                scale = request.args.get('scale', [1])[0]
                producer = ImageProducer(request, self.obj, opt, interval)
                producer.scale = float(scale)
            else:
                producer = OptionProducer(request, self.obj, opt, interval)
            producer.start()
            request.notifyFinish().addErrback(self._producer_failed_errback, producer)
            return server.NOT_DONE_YET
        
        request.setHeader("content-type", "text/html; charset=utf-8")
        v = []
        dps = self.obj['fullpath'].encode('utf-8')
        if dps == u'/':
            dp0 = request.path
        else:
            dp0 = request.path.replace(dps, b'')
            dp0 = dp0.replace(b'//', b'/')
        dp0 = dp0.decode('utf-8')
        # List subdevices
        for d in self.obj.devices:
            dp = d['devpath'] 
            v.append({'text': d['name'], 'href': dp0 + u'/' + dp,
                      'size': dp, 'type': '{' + d.__class__.__name__ + '}',
                      'encoding': ''})
        # List options
        for handle, opt in self.obj.describe().items():
            url = dp0 + '?opt=' + handle
            rep = repr(opt['current'])
            if len(rep) > 50:
                rep = rep[:46] + ' ...'
            v.append({'text': opt['name'], 'href': url,
                      'size': handle,
                      'type': opt['type'],
                      'encoding': repr(opt['current'])[:50]})
        tableContent = "".join(self.helper._buildTableContent(v))
        header = "Directory listing for " + self.obj['fullpath']
        r = self.helper.template % {
            "header": header, "tableContent": tableContent}
        r = r.encode('ascii', 'ignore')
        return r
    

    def close(self):
        for k, f in self.children.items():
            print('MdfDirectory: closing', k)
            f.close()
