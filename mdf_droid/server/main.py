#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
# !/usr/bin/twistd -noy
"""Mdf super server to start and manage other modules."""
import importlib
import json
import os
from multiprocessing import Lock, Value
from multiprocessing.managers import BaseProxy
from time import time, sleep
from traceback import format_exc, print_exc
from mdf_canon.csutil import go as getstatusoutput, xmlrpclib, executor
from twisted.internet import defer, task, threads
from twisted.internet import reactor
from twisted.web import server, http, xmlrpc
from . import stream
from .base import BaseServer, server_conf
from .. import device
from .. import parameters as params
from .. import share
from .. import utils


def rename_plugins(text):
    text = text.replace('misura.droid', 'mdf_droid')
    text = text.replace('misura.kiln.Kiln', 'mdf_server.furnace.Furnace')
    text = text.replace('misura.flex.Flex', 'mdf_server.bending.Bending')
    text = text.replace('misura.smaug.Smaug', 'mdf_server.thermal.Thermal')
    text = text.replace('misura', 'mdf_server')
    text = text.replace('mdf_server.morla.Morla', 'mdf_server.motion.Motion')
    return text


def iterate_plugins(plug=server_conf.plugins):
    group = []
    for plug in plug.splitlines():
        if len(plug) < 2:
            continue
        if plug == '#GROUP':
            ret = group[:]
            group = []
            yield ret
        if plug.startswith('#'):
            continue
        func = False
        if '.' in plug:
            plug = plug.split('.')
            func = plug.pop(-1)
            plug = '.'.join(plug)
        try:
            m = importlib.import_module(plug)
            if func:
                m = getattr(m, func)
            group.append(m)
        except:
            print(format_exc())

    yield group


class MainServer(BaseServer):
    """Live MdfServer object"""

    reinit_instrument = False

    def __str__(self):
        return 'MAINSERVER ' + repr(self)

    def __init__(self, instanceName='',
                 port=3880,
                 confdir=params.confdir,
                 datadir=params.datadir,
                 plug=False, manager=False):
        if manager is False:
            manager = share.manager
        print('MainServer with manager', repr(manager))
        r = getstatusoutput('mkdir -p {0}MainServer && mv -v {0}*.csv {0}*.bak {0}MainServer'.format(confdir))
        print("Folder migration:", r)
        BaseServer.__init__(self, manager, confdir, datadir)
        reg = self.desc['registry']
        print('SETTING REGISTRY', reg)
        self['registry'] = reg
        self.time_delta = 0
        #        share.Log.setStatus(self.desc)
        self.glock = Lock()
        # Announce zeroconf service
        msg = 'SERIAL=%s; HSM=%i; ODLT=%i; FLEX=%i' % (self['eq_sn'],
                                                       self['eq_hsm'],
                                                       self['eq_horizontal'],
                                                       self['eq_flex'])
        if params.announceZeroconf:
            self.zeroconf = utils.ZeroconfService(
                name='mdf' + instanceName, port=str(port), text=msg)
            self.zeroconf.publish()
        # Redefine maximum serial scan
        max_serial = self['eq_serialPorts']
        if max_serial > 0:
            device.Serial.dev_pattern = '/dev/tty*[USB][0-{}]'.format(
                max_serial)
        else:
            device.Serial.dev_pattern = '/dev/ttyUSB*'
        params.max_serial_scan = max_serial
        # Load plugins
        initializing = Value('i', 0)
        if not plug:
            plug = self['eq_plugin']

        plug = rename_plugins(plug)

        def init(func):
            print('Starting', func)
            func(self)
            initializing.value -= 1

        jobs = []
        for group in iterate_plugins(plug):
            for func in group:
                if params.init_threads:
                    initializing.value += 1
                    job = executor().submit(init, func)
                    jobs.append(job)
                else:
                    func(self)
            [job.result() for job in jobs]
            while initializing.value > 0:
                print('WAITING THREADED INITIALIZATION',
                      initializing.value, group)
                jobs[0].result()
                print(jobs)
                sleep(1)
        # Refresh lists
        self.xmlrpc_list()
        # Update and reset last/runningInstrument chooser
        opts = []
        vals = []
        for ins in self.instruments:
            vals.append(ins['devpath'])
            opts.append(ins['name'])
        ri = self.gete('runningInstrument')
        ri['options'] = opts
        ri['values'] = vals
        ri['current'] = ''
        self.sete('runningInstrument', ri)
        ri = self.gete('lastInstrument')
        ri['options'] = opts
        ri['values'] = vals
        ri['current'] = ''
        self.sete('lastInstrument', ri)
        self.stream = stream.MdfDirectory(self)
        if self['eq_sn'] == 'VirtualMdfServer':
            self.enable_simulation_server()
            
    def connection(self, *a, **k):
        """Reactor ready!"""
        return self.start_threaded_check()
            
    def get_registry(self):
        """Only reset this varible if empty"""
        cur = self.desc.get('registry')
        if cur:
            return cur
        r = device.get_registry()['reg']
        m = [k + ':' + v for k, v in r.items()]
        return '\n'.join(m)
    
    def set_registry(self, nval):
        if not nval:
            self.desc.set('registry', "")
            return self.get_registry()
        r = {}
        for line in nval.splitlines():
            line = line.split(':')
            r[line[0]] = line[1]
        device.get_registry()['saved'] = r
        return nval
            
    def glock_release(self):
        try:
            self.glock.release()
            return True
        except:
            self.log.debug('Threaded check lock was already released')
            return False
            
    def start_threaded_check(self):
        self.glock_release()
        self.looping = task.LoopingCall(self.threaded_check)
        d = self.looping.start(5)
        d.addCallback(self.threaded_check_callback)
        d.addErrback(self.threaded_check_errback)
        self.log.info('Threaded check started')
        return True
    
    def threaded_check_callback(self, *a, **k):
        self.log.critical('Threaded check unexpectedly exited without errors!', *a, **k)
        self.start_threaded_check()
    
    def threaded_check_errback(self, failure):
        self.log.critical('Threaded check unexpectedly exited with an error! Trying to restore...\n', failure.getErrorMessage())
        failure.printDetailedTraceback()
        self.start_threaded_check()

    def enable_simulation_server(self):
        self.log.info('Enabling simulation server')
        from mdf_server.tests import preconf as pc
        pc.full_server(self)
        pc._full_hsm(1, self)
        pc._absolute_flex(self)
        pc._full_horizontal(self)
        self.log.info('Enabled simulation server')
        return 'done'

    xmlrpc_enable_simulation_server = enable_simulation_server

    def set_endStatus(self, val):
        """Concatenate general endStatus messages form all devices"""
        if not val:
            return ''
        msg = self['endStatus']
        if msg:
            if val not in msg:
                return msg + '\n' + val
            else:
                return msg
        else:
            return val

    def threaded_check(self):
        """Perform the global check() into a separate thread."""
        # If glock is locked, it means a previous check is still running: skip.
        if not os.path.exists(share.dbpath):
            print('#*#*#*#*#*#*#*#*#*#\n' * 5)
            print('SHARED MEMORY PANIC\n')
            print('#*#*#*#*#*#*#*#*#*#\n' * 5)
            getstatusoutput('pkill -9 -f MdfServer.py')
            getstatusoutput('pkill -9 -f server.bin')
        if not self.glock.acquire(False):
            self.log.debug('Skipping threaded check: locked')
            if not self.looping.running:
                self.log.error('threaded_check: LoopingCall was inactive, restarting....')
                self.start_threaded_check()
            return False
        if self.shutting_down:
            self.log.debug('Skipping threaded check: shutting down')
            self.glock_release()
            return False
        if self['initTest']:
            self.log.debug('Skipping threaded check: initializing new test')
            self.glock_release()
            return False

        if self['restartOnNextCheck'] and self['restartOnFinishedTest']:
            self.restart(init_instrument=True)
            self.glock_release()
            return False
        else:
            self['restartOnNextCheck'] = False

        def fcheck():
            """Do the iterative check and release glock"""
            result = False
            try:
                result = self.check()
            except:
                self.log.error('Threaded check error', format_exc())
            finally:
                self.glock_release()
            if not result and self.runningInstrument:
                self.runningInstrument.stop_acquisition(message='System check failed!')

        threads.deferToThread(fcheck)
        return True
    
    def end_status(self, msg, *args):
        if not self.runningInstrument:
            return False
        return self.runningInstrument.end_status(msg, *args)

    def pause_check(self):
        """Pause threaded check"""
        if not reactor.running:
            return
        BaseServer.pause_check(self)
        t0 = time()
        while time() - t0 < 5:
            r = self.glock.acquire(False)
            if r:
                break
            print('Waiting for looping call to stop')
            sleep(.1)

    def resume_check(self):
        """Resume threaded check"""
        self.shutting_down = False
        try:
            self.glock_release()
        except:
            pass

    def get_scanning(self):
        """Check if any subdevice is scanning for hardware to become ready"""
        self.set('scanning', False)
        for dev in self.devices:
            if 'waiting' not in dev:
                continue
            if dev['waiting']:
                self.set('scanning', True)
                break
        return False

    def describe(self, *a, **kw):
        self.get_scanning()
        return BaseServer.describe(self, *a, **kw)

    def search_log(self, fromt=False, tot=False, maxn=None, priority=0, owner=False):
        return share.database.get_log(fromt, priority, owner, tot=tot, maxn=maxn)

    xmlrpc_search_log = search_log

    def xmlrpc_send_log(self, msg, p=10):
        """Clients can send a message to the server"""
        # easter egg
        if msg.lower() == 'ground control to major tom':
            msg = 'Major Tom to Ground Control'
        msg = '(client) ' + msg
        self.log(msg, p=p)
        return True

    def set_timeDelta(self, val):
        self.time_delta = val
        return val

    def get_timeDelta(self):
        return self.time_delta

    def newSession(self):
        return 1

    xmlrpc_newSession = newSession

    def close(self):
        if self.desc is False:
            return False
        self.stream.close()
        self.shutting_down = True
        self.pause_check()
        self.glock_release()

        return BaseServer.close(self)

    def restart(self, after=1, init_instrument=False, writeLevel=5, userName='unknown'):
        if after < 1:
            after = 1
        self.shutdown(after=after, restart=True, init_instrument=init_instrument,
                      writeLevel=writeLevel, userName=userName)
        return 'Restarting in %i seconds' % after

    xmlrpc_restart = restart

    shutting_down = False

    def shutdown(self, after=1, restart=False, init_instrument=False, writeLevel=5, userName='system'):
        if self.shutting_down:
            return 'Already shutting down.'
        if writeLevel < 2:
            self.log.critical('Unauthorized shutdown request')
            return 'NotAuthorized'
        self.stop_acquisition(message='Server shutdown!')
        if after < 1:
            after = 0.1
        self.restart = restart
        if init_instrument:
            self.reinit_instrument = self['lastInstrument']
            self.log.info(
                'Reinitializing instrument after restart:', self.reinit_instrument)
        else:
            self.reinit_instrument = False
        self['isRunning'] = False
        self['endStatus'] = 'Forced via shutdown.'
        msg = 'SHUTDOWN'
        if restart:
            msg = 'RESTART'
        self.log.critical(
            '%s in %.1f seconds requested by user "%s"' % (msg, after, userName))

        utils.apply_time_delta(self.time_delta)

        def stop():
            from twisted.internet import reactor
            print('Stopping reactor')
            reactor.stop()
            print('Stopping share')
            try:
                share.stop()
                share.close_sparse_objects()
            except:
                print_exc()

        task.deferLater(reactor, after, stop)
        return 'Stopping scheduled in %.1f second.' % after

    xmlrpc_shutdown = shutdown

    def _syncRender(self, request, function, args, kwargs, cbRender=None, ebRender=None):
        """Standard rendering."""
        d = defer.maybeDeferred(function, *args, **kwargs).addErrback(
            ebRender or self._ebRender
        ).addCallback(
            cbRender or self._cbRender, request
        )
        return d

    def _asyncRender(self, request, function, args, kwargs, cbRender=None, ebRender=None):
        """Makes all rendering asynchronous."""
        d = threads.deferToThread(function, *args, **kwargs).addErrback(
            ebRender or self._ebRender
        ).addCallback(
            cbRender or self._cbRender, request
        )
        return d

    def _ebRender(self, failure):
        """Trap failures into Mdf logging system"""
        try:
            what = failure.getTraceback()
            self.log.debug('XMLRPC Failure:', what)
            # Try to delete the entire multiprocessing connection cache
            if "Broken pipe" in what:
                for address in BaseProxy._address_to_local.keys():
                    del BaseProxy._address_to_local[address][0].connection
        except:
            pass
        return xmlrpc.XMLRPC._ebRender(self, failure)

    def _responseFailed(self, err, d):
        print('responseFailed', err)
        d.cancel()

    def load_request_body(self, body):
        if body[0] == ord('<'):
            ct = (b"Content-Type", b"text/xml; charset=utf-8")
            args, functionPath = xmlrpclib.loads(body)
            return ct, functionPath, args, self._cbRender, self._ebRender
        ct = (b"Content-Type", b"application/json; charset=utf-8")
        args = json.loads(body)
        cmd = args.pop('method', False) or args.pop('cmd', False)
        args = args.pop('params', [])
        return ct, cmd, args, self.json_cbRender, self.json_ebRender

    def json_cbRender(self, result, request, responseFailed=None):
        if responseFailed:
            return

        if isinstance(result, xmlrpc.Handler):
            result = {"result": result.result}
        if isinstance(result, xmlrpc.Fault):
            result = {"error": {"code": result.faultCode, "message": result.faultString}}
        else:
            result = (result,)
        try:
            try:
                result = {'jsonrpc': "2.0", 'id': 0, "result": result}
                content = json.dumps(result)
            except Exception as e:
                error = {"error": {"code": self.FAILURE,
                                   "message": f"Can't serialize output: {e}"},
                         'jsonrpc': "2.0", "id": 0}
                content = json.dumps(error)

            if isinstance(content, str):
                content = content.encode("utf8")
            request.setHeader(b"content-length", b"%d" % (len(content),))
            request.write(content)
        except Exception:
            self._log.failure("")
        request.finish()

    def json_ebRender(self, failure):
        return self._ebRender(failure)

    def render_POST(self, request):
        """ Overridden 'render' method which takes care of
        specialized auth and procedure lookup. """
        request.content.seek(0, 0)

        #         if not request.args:
        #             #origin = request.getHeader('Origin')
        #             request.setHeader('Access-Control-Allow-Origin', "*")
        #             c = request.content.read()
        #             if not (b'"authSession":' in c and b'"user":' in c and b'"cmd":' in c):
        #                 self.log.debug('Invalid lambda call', c)
        #                 return 'INVALID CALL'
        #             request.setHeader(b"content-type", b"text/plain; charset=utf-8")
        #             self.log.debug('Lambda call:', c)
        #             return 'Lambda call OK'

        self.last_client_access_time = utils.time()
        rlev, wlev = 5, 5
        user = 'nouser'

        # MEMORY LEAK HERE!
        # session = request.getSession()
        session = ''

        # Redirect streaming POST requests:
        if wlev >= 5 and self.stream.check_POST(request):
            s = self.stream.getChild(request.path, request)
            return s.render_POST(request)

        if params.useAuth:
            user = request.getUser().decode('utf8')
            rlev, wlev = self.users.levels(user)

        # xmlrpc never passes parametric arguments, so I can add auth-related
        # ones here
        connection = {'readLevel': rlev, 'writeLevel': wlev,
                 'userName': user, 'sessionID': session, 'request': request}

        try:
            body = request.args.get(b'request', [request.content.read()])[0]
            ct, functionPath, args, cbRender, ebRender = self.load_request_body(body)
            request.setHeader(*ct)
            function = self.lookupProcedure(functionPath)
        except xmlrpclib.Fault as f:
            cbRender(f, request)
            return server.NOT_DONE_YET
        else:
            r = device.fill_implicit_args(function, args, connection)
            if r is False:
                request.setResponseCode(http.BAD_REQUEST)
                return 'Bad function call!'.encode('utf8')
            function, args, kwargs = r
            d = self._asyncRender(request, function, args, kwargs, cbRender, ebRender)
            request.notifyFinish().addErrback(self._responseFailed, d)
            return server.NOT_DONE_YET

    def render_GET(self, request):
        """Pass GET requests to the MdfDirectory object"""
        if request.path == b'/RPC/ssl':
            request.setHeader("content-type", "text/html; charset=utf-8")
            r = "<html><body>Ciao<script>history.back()</script></body></html>"
            r = r.encode('ascii', 'ignore')
            return r
        self.last_client_access_time = utils.time()
        s = self.stream.getChild(request.path, request)
        return s.render_GET(request)

    def lookupProcedure(self, procedurePath):
        """Route special procedure requests to the storage.test FileServer object."""
        procedurePath = procedurePath.lstrip('/').replace('//', '/')
        if procedurePath.startswith('storage'):
            f = self.separator.join(('storage', 'test', ''))
            if procedurePath.startswith(f):
                return self.storage.test.lookupProcedure(procedurePath[len(f):])
        return xmlrpc.XMLRPC.lookupProcedure(self, procedurePath)
