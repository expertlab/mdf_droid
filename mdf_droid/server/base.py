#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
# !/usr/bin/twistd -noy
"""Mdf super server to start and manage other modules."""
# TODO: implement errors
import json
import string
from collections import defaultdict
from time import time
from copy import deepcopy
from traceback import print_exc
from mdf_canon.csutil import go

from mdf_canon import csutil, dashboard, option
from mdf_canon.csutil import utime

from .. import parameters as params
from .. import device
from .. import share

from . import server_conf

notification_interval = 10


class BaseServer(device.Device):

    """Basic server object functions. Useful for testing purposes"""
    allowNone = True
    naturalName = 'server'
    restart = False
    scanningPorts = 0
    _Method__name = 'MAINSERVER'
    name = 'server'
    conf_def = deepcopy(device.Device.conf_def)
    conf_def += server_conf.conf
    conf_def += list(dashboard.conf.values())
    summary_cache = {}
    summary_time = 0

    def __str__(self):
        return 'BASESERVER ' + repr(self)

    def __init__(self, manager=share.manager, confdir=params.confdir, datadir=params.datadir):
        self.manager = manager
        self._server = self
        self._root = self
        device.Device.__init__(self, parent=None, node='MAINSERVER')
        self.separator = '/'

        self.storage, self.beholder, self.balance, self.smaug, self.morla = [
            False] * 5
        self.hsm, self.horizontal, self.vertical, self.flex, self.post, self.drop, self.kiln = [
            False] * 7
        self.instruments = []
        self.deviceservers = []
        self.main_confdir = confdir
        self.main_datadir = datadir

    def pause_check(self):
        self.shutting_down = True

    def summarized(self, dataset_name):
        zt = self['zerotime']
        if zt != self.summary_time and time() - zt < 30:
            self.summary_cache = {}
            self.summary_time = zt
        path = option.common_proxy.from_column(dataset_name)
        summary = '/'.join(['summary'] + path)
        handle = path.pop(-1)
        obj = False
        summarized = None
        role = False
        if summary in self.summary_cache:
            obj, unit, summarized, role = self.summary_cache[summary]
            if role:
                return self.summarized(role)
        else:
            obj = self.toPath(path)
            opt = obj.gete(handle)
            unit = opt.get('unit', 'None')
            if opt['type'] == 'RoleIO':
                role = opt['options'][0] + opt['options'][-1]
                self.summary_cache[summary] = obj, unit, summarized, role
                return self.summarized(role)
        if self['summarize'] and (summarized in [True, None]):
            try:
                t, val = self.desc.desc.read_current(summary)
                summarized = True
            except:
                summarized = False
                print_exc()
        elif summarized:
            summarized = False
        if not summarized:
            t, val = obj.h_get(handle, -1)
        self.summary_cache[summary] = obj, unit, summarized, role
        return t, val, unit

    @csutil.sanitize()
    def xmlrpc_summarized(self, *a, **k):
        return self.summarized(*a, **k)
        
    def get_instruments(self):
        """Returns the list of available Instrument names"""
        lst = [(obj['comment'] or obj['name'], obj.naturalName) for obj in self.instruments]
        self.desc.set('instruments', lst)
        return lst

    def get_deviceservers(self):
        """Returns the list of available DeviceServer names"""
        lst = [obj['name'] for obj in self.deviceservers]
        self.desc.set('deviceservers', lst)
        return lst

    @property
    def runningInstrument(self):
        ins = self['runningInstrument']
        if ins in ['', 'None', None, False]:
            return False
        obj = self.child(ins)
        if obj is None:
            return False
        return obj

    @property
    def lastInstrument(self):
        """Configured instrument, ready for test start"""
        ins = self['lastInstrument']
        if ins in ['', 'None', None, False]:
            return False
        obj = self.child(ins)
        if obj is None:
            return False
        return obj

    def get_progress(self):
        """Remove finished Progress before returning the list of active tasks."""
        p = list(set(self.desc.get('progress')))
        # Clean finished progresses
        for e0 in p[:]:
            e = e0.split('/')
            # Option name
            opt = e.pop(-1)
            # Retrive the pointed object
            obj = self.child(e)
            if not obj:
                self.log.error('Operation object no longer exists!', e0)
                p.remove(e0)
            # Operation ended
            if not obj[opt]:
                self.log.debug('Operation ended', e0)
                p.remove(e0)
        # List contains still running options
        return p

    def set_progress(self, kid):
        """Append kid to the list instead of substituting"""
        p = self.desc.get('progress')
        p.append(kid)
        return p
    
    last_client_access_time = 1e32

    def check(self):
        """Check for delayed start"""
        ins = self.lastInstrument  # there must be a defined running instrument
        test_in_progress = ins and (ins['running'] + ins['initTest'] + ins['closingTest'] + ins['initializing'])
        self['lastClientAccessTime'] = self.last_client_access_time
        delta = self.time() - self.last_client_access_time
        
        inactive = delta > 10 \
            and not test_in_progress \
            and not self['delayStart'] \
            and not self.time_delta
            
        self['inactive'] = inactive
            
        auto_shutdown_interval = self['autoShutdownInterval']

        client_inactive = inactive and (delta > auto_shutdown_interval) \
                        and auto_shutdown_interval >= 300 \
                        and self.last_client_access_time > 0 

        if client_inactive:
            self.log.debug('Client inactive: halting server.')
            self.support.get_halt()
        delayT = self['delayT']
        T = 0
        if self.kiln:
            T = self.kiln['T']
        sequence = self['delayStart']
        seqIndex = self['sequenceIndex']
        if sequence and not ins:
            self.log.debug('No instrument: disabling delayStart')
            self['delayStart'] = 0
        elif sequence == seqIndex and sequence > 0:
            self.log.debug('Finished delayStart sequence, resetting')
            self['delayStart'] = 0
        elif sequence > seqIndex and test_in_progress == 0:
            if T < delayT:
                self.log.info('delayStart:', self['lastInstrument'], self['delayT'], seqIndex, '/', sequence)
                if sequence > 1:
                    name = ins.measure['name'].split(' seq')
                    if len(name) > 1:
                        name.pop(-1)
                    name.append(str(seqIndex))
                    # never trigger the autoname of samples: use desc.set
                    ins.measure.desc.set('name', ' seq'.join(name))
                ins.start_acquisition(userName=ins.measure['operator'])
                self.sleep(2)
                self['sequenceIndex'] += 1
            else:
                self.log.debug('Waiting for delayed start {}/{} of {}. Target T: {}, current: {}'
                               .format(seqIndex, sequence, self['lastInstrument'], delayT, T))
        return device.Device.check(self)

    def set_delayStart(self, val, userName='unknown'):
        """Forbid if no instrument is configured or set operator name."""
        ins = self.lastInstrument
        if not val:
            pass
        elif not ins:
            self.log.error(
                'Cannot set delayed start if no instrument is configured.')
            val = 0
        elif ins is False:
            self.log.error('Unknown instrument for delayed start: {}'.format(repr(ins)))
            val = 0
        elif self['isRunning'] + ins['running'] + ins['initTest'] + ins['closingTest'] + ins['initializing'] != 0:
            self.log.error(
                'Cannot set delayed start. Instrument is already running/closing.')
            val = 0
        elif self['delayT'] <= 0:
            self.log.error(
                'Delayed start require `delayT` option to be greater than 0')
            val = 0
        self.log.debug('set_delayStart', val, userName)
        if val:
            ins.measure['operator'] = userName
        else:
            self['sequenceIndex'] = 0
        return val

    def stop_acquisition(self, save=True, message='System requested the test to stop.'):
        obj = self.runningInstrument
        if obj is False:
            self.log.error('No instrument running: cannot stop acquisition')
            return False
        if not obj['analysis']:
            self.log.warning('Acquisition is not running for instrument', obj.name)
            return False
        return obj.stop_acquisition(save=True, message=message)

    def xmlrpc_stop_acquisition(self, save=True, writeLevel=1, userName='unknown'):
        if writeLevel < 1:
            self.log.critical('Not authorized request: stop_acquisition')
            return 'NotAuthorized'
        msg = 'Operator [{}] requested the test to stop.'.format(userName)
        return self.stop_acquisition(save=save, message=msg)

    def mapdate(self, kid_times, readLevel=0):
        """Receives a list of KID names and times to check for updates.
        Replies with two lists. The first (idx) is the list of updated positional indexes referring to
        the relative position of the option in kid_times.
        The second (rep) contains new current values.
        Is time is positive, the value is red from memory (.desc.get()).
        If negative, reading is forced through a standard get() call."""
        idx = []
        rep = []

        for i, (k, t0) in enumerate(kid_times):
            obj, n = self.read_kid(k)
            if not obj:
                continue
            rl = obj.getattr(n, 'readLevel')
            if rl > readLevel:
                self.log.error(
                    'Permission denied for option:', n, rl, readLevel)
                continue
            # Detect forced updates
            if not t0 or t0 < 0:
                rep.append(obj.get(n))
                idx.append(i)
                continue
            nt = obj.h_time_at(n)
            if nt > t0:
                # Retrieve the memory value,
                # in order not to trigger on-get updates
                rep.append(obj.desc.get(n))
                idx.append(i)
        return [idx, rep]

    @csutil.sanitize()
    def xmlrpc_mapdate(self, *a, readLevel=0, **k):
        k['readLevel'] = readLevel
        return self.mapdate(*a, **k)
    
    def shutdown(self, *a, **k):
        try:
            self.close()
            share.stop()
        except:
            print_exc()
        from twisted.internet import reactor
        reactor.stop()

    def get_eq_mac(self):
        HW = go("/sbin/ifconfig")[1]
        # New ifconfig format
        if ' ether ' in HW and ' inet ' in HW and ' inet6 ' in HW:
            all_mac = go("/sbin/ifconfig | grep 'ether ' | awk '{ print $2}'")[1]
        else:
            # Old ifconfig format
            all_mac = go("/sbin/ifconfig | grep 'HW' | awk '{ print $5}'")[1]
        return all_mac
    
    def set_log_notify(self, nval):
        if not nval:
            self._already_notified = {}
        return nval
    
    # TODO: Here there should be locking
    _already_notified = {}

    def notify(self, notify, title, msg_format, interval=notification_interval, **data):
        autocloud = 'cloud' not in data
        level = data.get('level', 'warning')
        template = string.Template(msg_format)
        if autocloud and (notify not in self._already_notified):
            self._already_notified[notify] = 0
            data['cloud'] = True
        elif autocloud:
            data['cloud'] = False
        if (time() - self._already_notified[notify] > interval) or data['cloud']:
            ins = self.runningInstrument or self.kiln or None
            data['serial'] = self['eq_sn']
            if ins:
                data['name'] = ins.measure['name']
            if self.kiln:
                data['T'] = round(self.kiln.get('T'), 1)
                data['heatingRate'] = round(self.kiln['heatingRate'], 2)
            else:
                data['T'] = -9999
                data['heatingRate'] = -9999
            msg = template.safe_substitute(data)
            getattr(self.log, level)(title + '\n' + msg)
            title += ' (on {})'.format(self['eq_sn'])
            tab = self['log_notify']
            tab.append([time(), notify, title, msg, json.dumps(data)])
            self['log_notify'] = tab
            self._already_notified[notify] = utime()
            return True
        return False
    
    _client_notified = set([])

    def xmlrpc_client_notified(self, t):
        self.log.debug('Client has notified', t)
        tab0 = self['log_notify']
        tab = []
        mod = 0
        for ev in tab0:
            if abs(ev[0] - t) > 1e-3:
                tab.append(ev)
                continue
            mod += 1
        if mod:
            self['log_notify'] = tab
        return mod
        
    def xmlrpc_do_notify(self, notify='Notify:Mdf:ManCool', title='ManualCooling', msg_format='nothing $T', **data):
        return self.notify(notify, title, msg_format, **data)
    
    ##################################
    # Dashboard interface
    
    _rule_opt_hide = False

    @property
    def rule_opt_hide(self):
        if not self._rule_opt_hide:
            tab = [[('t',), ('retn',)],
                   [self['opt_hide'], 1],
                   ]
            self._rule_opt_hide = dashboard.RulesTable(tab)
        return self._rule_opt_hide
    
    _rule_opt_status = False

    @property
    def rule_opt_status(self):
        if not self._rule_opt_status:
            self._rule_opt_status = dashboard.RulesTable(self['opt_status'])
        return self._rule_opt_status
    
    def dashboard_options(self):
        # Collect available ordered options
        ins = self.runningInstrument or self.lastInstrument
        remObj = ins or self
        opts = dashboard.create_dashboard(self, remObj, self)
        return opts
    
    xmlrpc_dashboard_options = dashboard_options
    
    def list_options(self):
        return dashboard.list_options(self)
            
    def xmlrpc_list_options(self):
        return list(self.list_options())
    
    def recursive_list_options(self, check=True):
        return dashboard.recursive_list_options(self, check=check)
                
    def xmlrpc_recursive_list_options(self):
        return list(self.recursive_list_options())
        
