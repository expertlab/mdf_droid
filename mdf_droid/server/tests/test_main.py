#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest

from mdf_droid import server, tests
from mdf_droid import share, device

print('Importing ' + __name__)


def setUpModule():
    print('Starting ' + __name__)


def tearDownModule():
    print('Ending ' + __name__)


plug = """
mdf_droid.users.Users
mdf_droid.storage.Storage
mdf_droid.support.Support
"""


class MainServer(unittest.TestCase):

    def setUp(self):
        tests.reset_test_memory()
        self.srv = server.MainServer(plug=plug, manager=share.DummyManager())

    def test_plugins(self):
        # Load default plugins
        self.assertTrue(hasattr(self.srv, 'users'))
        self.assertTrue(hasattr(self.srv, 'storage'))
        self.assertTrue(hasattr(self.srv, 'support'))
        self.assertEqual(len(self.srv.deviceservers), 0)
        self.assertEqual(len(self.srv.instruments), 0)


if __name__ == "__main__":
    unittest.main(verbosity=2)
