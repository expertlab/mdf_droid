# -*- coding: utf-8 -*-
"""Streaming resource for options"""
from time import sleep
import os
from traceback import print_exc
from twisted.web import resource, static,  server
from twisted.internet import interfaces
from twisted.internet import reactor
from twisted.internet.task import deferLater, cooperate, NotPaused
from PIL import Image
from io import BytesIO, StringIO
import json
import logging as pylogging

try:
    from zope.interface import implements, implementer
except:
    from zope.interface import classImplements, implementer
    implements = classImplements

pylogging.getLogger("PIL").setLevel(pylogging.CRITICAL)

@implementer(interfaces.IPushProducer)
class GenericProducer(object):
    format = "text/plain; charset=utf-8"
    do = True
    task = False
    
    def __init__(self, request, obj, opt, interval=0):
        self.request = request
        self.obj = obj
        self.opt = opt
        self.do = True
        self.interval = interval
        self.last = -1
    
    def write_value(self, value):
        if isinstance(value,str):
            value=value.encode('utf8')
        if not isinstance(value, bytes):
            print('write_value received non-string', value, self.obj['fullpath'], self.opt)
            value = str(value).encode('utf8')
        self.request.write(value)
        
    def produce_value(self):
        """Get producible time,value. Returns None,None if impossible to produce."""
        t = self.obj.h_time_at(self.opt, -1)
        dt = (t - self.last) - self.interval
        if self.do and self.last > 0 and dt <= 0:
            # Avoid 100% CPU
            sleep(min(-dt, self.interval))
            return None, None
        try:
            val = self.obj[self.opt]
        except:
            print_exc()
            sleep(0.1)
            return None, None
        self.last = t
        return t, val
        
    def workload(self):
        while self.do:
            t, val = self.produce_value()
            if t is not None:
                self.write_value(val)
            if not self.interval:
                self.stopProducing()
                break
            print('sleep interval')
            sleep(self.interval)
            #yield deferLater(reactor, self.interval, lambda: None)
        print('done workload')
            
    def header(self):
        self.request.setHeader('pragma', 'no-cache')
        self.request.setHeader('connection', 'close')
        self.request.setHeader("content-type", self.format)
        
    def start(self):
        """Register the producer into the reactor."""
        self.header()
        self.request.registerProducer(self, False)
        self.task = cooperate(self.workload())
        
    def pauseProducing(self):
        self.do = False
        if self.task:
            self.task.stop()
        
    def stopProducing(self):
        self.do = False
        if self.task:
            self.task.stop()
        self.request.unregisterProducer()
        self.request.finish()
        
    def resumeProducing(self):
        """Called each time the reactor has time to send new value"""
        self.do = True
        try:
            self.task.resume()
        except NotPaused:
            pass
        

class OptionProducer(GenericProducer):
    """A dummy producer which continously sends the current option value."""
    
    def write_value(self,val):
        self.request.write(json.dumps(val).encode('utf8') + b'\n')

 
        
class AutoRefreshProducer(GenericProducer):
    def header(self):
        # Warning: causes flickering!
        if self.interval:
            self.request.setHeader("refresh", str(self.interval))
        super(AutoRefreshProducer, self).header()
    
    def start(self):
        """Register the producer into the reactor."""
        self.header()
        self.request.registerProducer(self, False)
        
    def resumeProducing(self):
        t,val = self.produce_value()
        if t is not None:
            self.write_value(val)
        self.do = False
        self.request.unregisterProducer()
        self.request.finish()
        

class ImageProducer(AutoRefreshProducer):
    format = 'image/png'
    scale = 1.0
    
    def write_value(self, val):
        if self.scale<1.0 and self.scale>0:
            im = Image.open(BytesIO(val))
            w = int(im.width*self.scale)
            h = int(im.height*self.scale)
            im.thumbnail((w,h))
            out = BytesIO()
            im.save(out, 'PNG')
            out.seek(0)
            val = out.read()
        super(ImageProducer, self).write_value(val)
    



class MdfDirectory(resource.Resource):

    """Renders server object tree as a tabular folder of files.
    Adds updates streaming and large file uploads via POST requests."""

    def __init__(self, obj, node='stream'):
        resource.Resource.__init__(self)
        self.helper = static.DirectoryLister('')
        t = self.helper.template
        t = t.replace('Content encoding', 'Value').replace(
            'Filename', 'Name').replace('Size', 'Handle')
        self.helper.template = t
        self.obj = obj
        self.node = node

    def getChild(self, path, request):
        if path:
            # Remove /RPC/stream or /stream prefixes
            if path.startswith(b'/RPC'):
                path = path.lstrip('b/RPC')
            if path.startswith(b'/stream'):
                path = path.lstrip(b'/stream')
            sub = self.obj.child(path.decode('utf-8'))
            if not sub:
                return resource.NoResource("No such child resource: "+str(sub))
            return MdfDirectory(sub)
        else:
            return self

    @classmethod
    def check_POST(self, request):
        opt = request.args.get(b'opt', [False])[0]
        filename = request.args.get(b'filename', [False])[0]
        data = request.args.get(b'data', [False])[0]
        overwrite = request.args.get(b'overwrite', [False])[0]
        if False in (opt, filename, data):
            return False
        return opt.decode('utf8'), filename.decode('utf8'), data, overwrite

    def render_POST(self, request):
        """Allows uploads via chunked post requests.
        Returns current file length."""
        request.setHeader("content-type", "text/plain; charset=utf-8")
        r = self.check_POST(request)
        if r is False:
            return 'Invalid POST request'.encode('utf8')
        opt, filename, data, overwrite = r
        if len(data) == 0:
            return 'No data for POST request'.encode('utf8')
        while '..' in filename:
            filename = filename.replace('..', '.')
        filename = filename.replace('/', '_')
        filename = os.path.join(self.obj.desc.getConf_dir(), opt, filename)
        
        if overwrite:
            print('Overwriting file:', filename)
            f = open(filename, 'wb') 
        else:
            print('Appending file:', filename)
            f = open(filename, 'ab+')
            # TODO: should lock files to avoid conflicts
            f.seek(0, 2)
        r0 = f.tell()
        f.write(data)
        f.seek(0, 2)
        r1 = f.tell()
        f.close()
        print('Updated file:', filename, r0, r1)
        return '{}->{}'.format(r0, r1).encode('utf8')
    
    def _producer_failed_errback(self, producer):
        producer.stopProducing()

    def render_GET(self, request):
        opt = request.args.get(b'opt', [False])[0]
        if opt:
            opt = opt.decode('utf8')
            interval = float(request.args.get(b'interval', [b'0'])[0].decode('utf8'))
            typ = self.obj.gete(opt)['type']
            if typ == 'Image':
                scale = request.args.get(b'scale', [1])[0]
                producer = ImageProducer(request, self.obj, opt, interval)
                producer.scale = float(scale)
            else:
                producer = OptionProducer(request, self.obj, opt, interval)
            producer.start()
            request.notifyFinish().addErrback(self._producer_failed_errback, producer)
            return server.NOT_DONE_YET
        
        request.setHeader("content-type", "text/html; charset=utf-8")
        v = []
        dps = self.obj['fullpath'].encode('utf-8')
        if dps == u'/':
            dp0 = request.path
        else:
            dp0 = request.path.replace(dps, b'')
            dp0 = dp0.replace(b'//', b'/')
        dp0 = dp0.decode('utf-8')
        # List subdevices
        for d in self.obj.devices:
            dp = d['devpath'] 
            v.append({'text': d['name'], 'href': dp0 + u'/' + dp,
                      'size': dp, 'type': '{' + d.__class__.__name__ + '}',
                      'encoding': ''})
        # List options
        for handle, opt in self.obj.describe().items():
            url = dp0 + '?opt=' + handle
            rep = repr(opt['current'])
            if len(rep) > 50:
                rep = rep[:46] + ' ...'
            v.append({'text': opt['name'], 'href': url,
                      'size': handle,
                      'type': opt['type'],
                      'encoding': repr(opt['current'])[:50]})
        tableContent = "".join(self.helper._buildTableContent(v))
        header = "Directory listing for " + self.obj['fullpath']
        r = self.helper.template % {
            "header": header, "tableContent": tableContent}
        r = r.encode('ascii', 'ignore')
        return r

    def close(self):
        for k, f in self.children.items():
            print('MdfDirectory: closing', k)
            f.close()
